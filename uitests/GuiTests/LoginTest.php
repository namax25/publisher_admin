<?php

namespace GuiTests;

/**
 * Description of TestLogin
 *
 * @author namax
 */
class LoginTest extends \PHPUnit_Framework_TestCase
{

    protected $webDriver;

    public function setUp()
    {
        $capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
        $this->webDriver = \RemoteWebDriver::create(SELENIUM_SERVER, $capabilities);
    }

    public function testLoginFormExists()
    {
        $this->webDriver->get(BASE_URL);

        $this->assertContains('Monitoring Tools', $this->webDriver->getTitle());
        $login = $this->webDriver->findElement(\WebDriverBy::name('login'));
        $password = $this->webDriver->findElement(\WebDriverBy::name('password'));

        $this->assertEquals('', $login->getText());
        $this->assertEquals('', $password->getText());
        $login->sendKeys('79255086415');
        $password->sendKeys('62408329');

        $submit = $this->webDriver->findElement(\WebDriverBy::name('submit'));
        $submit->submit();
    }


    public function tearDown()
    {
        $this->webDriver->close();
    }

}
