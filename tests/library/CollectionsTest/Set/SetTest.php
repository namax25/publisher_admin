<?php

namespace CollectionsTest\Set;

/**
 * Description of AbstractSet
 *
 * @author namax
 */
class SetTest extends \PHPUnit_Framework_TestCase
{

    protected $set = null;

    protected function setUp()
    {
        $this->set = new \Collections\Set\Set();
    }

    public function testAdd()
    {
        $this->assertEquals(true, $this->set->add(1));
        $this->assertEquals(true, $this->set->add(2));
        $this->assertEquals(false, $this->set->add(2));
    }

    public function testClear()
    {
        $this->addVals($this->set);

        $this->assertEquals(4, $this->set->size());
        $this->set->clear();
        $this->assertEquals([], $this->set->toArray());
        $this->assertEquals(0, $this->set->size());
    }

    private function addVals($set)
    {
        $items = [1, 2, 3, 1, 4];
        foreach ($items as $item) {
            $set->add($item);
        }
    }

    public function testContains()
    {
        $this->addVals($this->set);
        $this->assertEquals(true, $this->set->contains(2));
        $this->assertEquals(true, $this->set->contains(3));
        $this->assertEquals(false, $this->set->contains(5));
    }

    public function testHashCode()
    {
        $this->assertEquals(spl_object_hash($this->set), $this->set->hashCode());
    }

    public function testIsEmpty()
    {
        $this->assertEquals(true, $this->set->isEmpty());
        $this->assertEquals(true, $this->set->add(1));
        $this->assertEquals(false, $this->set->isEmpty());
    }

    public function testRemove()
    {
        $this->addVals($this->set);
        $this->assertEquals(true, $this->set->remove(2));
        $this->assertEquals(false, $this->set->remove(21));
        $this->assertEquals(3, $this->set->size());
    }

    public function testGetIterator()
    {
        $this->assertEquals(true, $this->set->getIterator() instanceof \Iterator);
        $this->set->add(1);
        $this->assertEquals(true, $this->set->getIterator() instanceof \Iterator);
    }

}
