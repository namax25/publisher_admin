<?php

namespace CollectionsTest\Set;

use \CollectionsTest\ObjectMockTest;

/**
 * Description of HashSet
 *
 * @author namax
 */
class HashSetTest extends \PHPUnit_Framework_TestCase
{

    protected $set = null;

    protected function setUp()
    {
        $this->set = new \Collections\Set\HashSet();
    }

    private function addVals($set)
    {
        $o1 = new ObjectMockTest();
        $o2 = new ObjectMockTest();
        $o2->setVal1(2);
        $o3 = new ObjectMockTest();

        $set->add($o1);
        $set->add($o2);
        $set->add($o3);
    }

    /**
     * @test
     */
    public function add()
    {
        $o1 = new ObjectMockTest();
        $this->assertEquals(true, $this->set->add($o1));

        $o2 = new ObjectMockTest();
        $o2->setVal1(2);
        $this->assertEquals(true, $this->set->add($o2));

        $o3 = new ObjectMockTest();
        $this->assertEquals(false, $this->set->add($o3));
        $this->assertEquals(2, $this->set->size());
    }

    public function testContains()
    {
        $this->addVals($this->set);
        $o2 = new ObjectMockTest();
        $o2->setVal1(2);
        $this->assertEquals(true, $this->set->contains(new ObjectMockTest()));
        $this->assertEquals(true, $this->set->contains($o2));
        $o3 = new ObjectMockTest();
        $o3->setVal1(33);
        $this->assertEquals(false, $this->set->contains($o3));
    }

    /**
     * @test
     */
    public function remove()
    {
        $o1 = new ObjectMockTest();
        $o1->setVal1(2);
        $o2 = new ObjectMockTest();
        $o2->setVal1(3);
        $this->set->add($o1);
        $this->set->add($o2);
        $this->assertEquals(2, $this->set->size());
        $this->assertEquals(true, $this->set->remove($o1));
        $this->assertEquals(false, $this->set->remove($o1));
        $this->assertEquals(true, $this->set->contains($o2));
        $this->assertEquals(1, $this->set->size());
    }

}
