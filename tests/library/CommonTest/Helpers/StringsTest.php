<?php

namespace CommonTest\Helpers;

use  \Helpers\Strings;

/**
 * Description of Ads
 * Помощник для работы с объявлениями
 * @author namax
 */
class StringsTest extends \PHPUnit_Framework_TestCase
{

    public function testCreate()
    {
        $this->assertInstanceOf('\Helpers\Strings', Strings::create());
    }

    public function providerStrings()
    {
        return array(
            array("Проверка содержит ли строка заданную подстроку", "ПроВерка", true, true, true),
            array("Проверка содержит ли строка заданную подстроку", "проверка", true, true, true),
            array("Проверка содержит ли строка заданную подстроку", "Проверка", true, false, true),
            array("Проверка содержит ли строка заданную подстроку", "проверка", true, false, false),
            array("class StringsTests", "StringsTests", false, false, true),
            array("class StringsTests", "stringsTests", false, false, false),
            array("class StringsTests", "stringstests", false, true, true),
            array("class StringsTests", "stringsTests", false, true, true),
        );
    }

    /**
     * @dataProvider providerStrings
     */
    public function testIsStrContain($string, $substring, $utf, $caseInsensitive, $expected)
    {
        $this->assertEquals($expected, Strings::create()->isStrContain($substring, $string, $utf, $caseInsensitive));
    }

    public function providerUcfirst()
    {
        return array(
            array("Проверка", true, "Проверка"),
            array("stringsTests", false, "StringsTests"),
            array("StringsTests", false, "StringsTests"),
            array("проверка", true, "Проверка"),
        );
    }

    /**
     * @dataProvider providerUcfirst
     */
    public function testUcfirst($str, $utf, $expected)
    {
        $this->assertEquals($expected, Strings::create()->ucfirst($str, $utf));
    }

    public function providerBadArrStrings()
    {
        return array(
            array("<sd\'xcv!", "sdxcv!"),
            array(["<sd\'xcv!", ">sd\'xcv#!"], ['sdxcv!', 'sdxcv#!']),
        );
    }

    /**
     * @dataProvider providerBadArrStrings
     */
    public function testClear($str, $expected)
    {
        $this->assertEquals($expected, Strings::create()->clear($str));
    }

    public function providerBadArrDigitsStrings()
    {
        return array(
            array("<s1d\'xc4v!", "14"),
            array(["<s3d\'xc4v!", ">5sd\'x1cv#!"], ['34', '51']),
        );
    }

    /**
     * @dataProvider providerBadArrDigitsStrings
     */
    public function testClearDigits($str, $expected)
    {
        $this->assertEquals($expected, Strings::create()->clearDigits($str));
    }

    public function providerAliasStrings()
    {
        return array(
            array("<s1d\'xc4v!_fd", "s1dxc4v_fd"),
            array(["<s3d\'xc4v!_fd23", ">5sd\'x1cv#!_dsf3"], ['s3dxc4v_fd23', '5sdx1cv_dsf3']),
        );
    }

    /**
     * @dataProvider providerAliasStrings
     */
    public function testClearAlias($str, $expected)
    {

        $this->assertEquals($expected, Strings::create()->clearAlias($str));
    }

}
