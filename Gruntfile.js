module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            build: {
                src: 'www/js/app.js',
                dest: 'www/build/app.min.js'
            }
        },
        cssmin: {
            combine: {
                files: {
                    'www/build/main.min.css': [
                        'www/css/style.css',
                    ]
                }
            }

        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'www/images_origin',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'www/images'
                }]
            }
        },
        watch: {
            cssmin: {
                files: ['www/css/*.css'],
                tasks: 'cssmin'
            },
            js: {
                files: ['www/js/*.js'],
                tasks: ['requirejs:rel']
            },
            images: {
                files: ['www/images_origin/*'],
                tasks: ['imagemin']
            }
        },
        bower: {
            target: {
                rjsConfig: "www/js/requirejs.config.js"
            }
        },
        requirejs: {
            rel: {
                options: {
                    baseUrl: "www/js",
                    mainConfigFile: "www/js/requirejs.config.js",
                    out: "www/build/app.min.js",
                    name: "app"
                }
            },
            dev: {
                options: {
                    baseUrl: "www/js",
                    mainConfigFile: "www/js/requirejs.config.js",
                    out: "www/build/app.min.js",
                    optimize: 'none',
                    name: "app"
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-bower-requirejs');
    // Default task(s). 'uglify', 'cssmin', 'imagemin',


    grunt.registerTask('dev', [  'requirejs:dev']);
    grunt.registerTask('rel', ['bower', 'cssmin', 'imagemin', 'requirejs:rel']);

    grunt.registerTask('build', 'run build', function () {
        var target = grunt.option('target') || 'dev';
        grunt.task.run(target);
    });
    // grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
};