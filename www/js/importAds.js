/* ========================================================================
 * Module: importAds.js
 * ========================================================================
 */


define(['jquery', 'alerts', 'utils'], function ($, alerts, utils) {

    'use strict';
    var module = {};
    var importLinks = $('.importLinks');

    importLinks.on("click", function () {
        var link = $(this);
        var type = link.data('type');


        if (type == 'sended') {
            alerts.info("Запрос уже отправлен");
            return false;
        }

        link.data('type', 'sended');

        if (sendImportRequest(type, link)) {
            link.text("Не удалось отправить запрос. Ошибочные параметры");
        }

        return false;
    })


    function sendImportRequest(typeName, link) {
        if (!typeName) {
            return false;
        }

        $.post("/app/import-ads/request", {type: typeName}, function (data) {

            alerts.printAllAlerts(data.msg);

            if (data.success ) {
                link.text("Запрос на импорт принят. Объявления скоро появятся...");
            }

        }, "json");

        link.text("Запрос на импорт отправлен...");

    }

    return module;
});