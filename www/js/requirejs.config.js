/* ========================================================================
 * Module: requirejs.config.js
 * ========================================================================
 */

'use strict';

requirejs.config({
    baseUrl: "/js",
    paths: {
        jquery: "../vendor/jquery/dist/jquery",
        bootstrap: "../vendor/bootstrap/dist/js/bootstrap",
        bootstrap_filestyle: "../vendor/bootstrap-filestyle/src/bootstrap-filestyle",
        jquery_cookie: "../vendor/jquery-cookie/jquery.cookie",
        requirejs: "../vendor/requirejs/require",
        jqueryform: "../vendor/jquery.form",
        "bootstrap-filestyle": "../vendor/bootstrap-filestyle/src/bootstrap-filestyle",
        "jquery-cookie": "../vendor/jquery-cookie/jquery.cookie"
    },
    shim: {
        jquery: {
            exports: "$"
        },
        bootstrap: {
            exports: "bootstrap",
            deps: [
                "jquery"
            ]
        },
        bootstrap_filestyle: {
            exports: "bootstrap_filestyle",
            deps: [
                "bootstrap"
            ]
        }
    },
    packages: [

    ]
});

require(["app"], function () {

});