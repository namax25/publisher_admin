/* ========================================================================
 * Module: alert.js
 * ========================================================================
*/

 define(['jquery', 'utils'], function ($, utils) {

    'use strict';
    var alerts = {};
    var successAlertTemplate = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Выполненно успешно!</strong><br />__MSG__</div>';
    var infoAlertTemplate = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Информация.</strong><br />__MSG__</div>';
    var warnAlertTemplate = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Внимание!</strong><br />__MSG__</div>';
    var errAlertTemplate = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Ошибка!</strong><br />__MSG__</div>';
    var statusMessages = $("#statusMessages");
    var isClearSatusTimerOn = false;

    function printAlert(messages, template) {
        if (messages) {

            if (utils.isStr(messages)) {
                var alertHtml = template.replace(/__MSG__/g, messages);
                statusMessages.append(alertHtml);

            } else if (messages.length > 1) {
                var div = $('<div></div>');
                $.each(messages, function (key, val) {
                    div.append("<p>" + val + "<\p>");
                });
                var alertHtml = template.replace(/__MSG__/g, div.html());
                statusMessages.append(alertHtml);
            } else {
                var alertHtml = template.replace(/__MSG__/g, messages[0]);
                statusMessages.append(alertHtml);
            }
            clearMessagesByTimer();
        }
    }

    function clearMessagesByTimer() {
        if (isClearSatusTimerOn === false) {
            if (statusMessages.html()) {
                isClearSatusTimerOn = true;
                statusMessages.delay(10000).fadeOut(600, function () {
                    isClearSatusTimerOn = false;
                    statusMessages.empty();
                });
            }
        }
    }


    alerts = {


        err: function (msg) {

            if (utils.isStr(msg)) {
                printAlert(msg, errAlertTemplate);
            } else if ('errors' in msg) {
                printAlert(msg.errors, errAlertTemplate);
            }

        },
        success: function (msg) {

            if (utils.isStr(msg)) {
                printAlert(msg, successAlertTemplate);
            } else if ('success' in msg) {
                printAlert(msg.success, successAlertTemplate);
            }

        },
        warn: function (msg) {

            if (utils.isStr(msg)) {
                printAlert(msg, warnAlertTemplate);
            } else if ('warn' in msg) {
                printAlert(msg.warn, warnAlertTemplate);
            }

        },
        info: function (msg) {
            if (utils.isStr(msg)) {
                printAlert(msg, infoAlertTemplate);
            } else if ('info' in msg) {
                printAlert(msg.info, infoAlertTemplate);
            }

        },
        printAllAlerts: function (messages) {
            statusMessages.show();
            this.success(messages);
            this.info(messages);
            this.warn(messages);
            this.err(messages);
        }

    };
    return alerts;
});




