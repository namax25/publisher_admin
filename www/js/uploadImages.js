/* ========================================================================
 * Module: upload_images.js
 * ========================================================================
 */

define(['jquery', 'alerts', 'utils', 'jqueryform'], function ($, alerts, utils, jqueryform) {

    'use strict';

    var module = {};
    var uploadForm = $('#upload-form');
    var adForm = $('#formArendaAds');
    var progress = $('#progress');
    var progressBar = $('#progress-bar');
    var statustxt = $('#statustxt');
    var filenameTxt = $('#filename');
    var images_container = $('#images_container');
    var maxFileSize = 2097152;
    var imageType = "";
    var hashCash = [];

    var sendBtn = '<button class="btn btn-default send-image" type="submit">Отправить</button>';
    var deleteBtn = '<button class="btn btn-danger delete-image" type="submit">Удалить</button>';

    progress.hide();

    $('#image-file').on('change', function (e) {
        progress.hide();
        progressBar.width('0%')
        statustxt.html('0%');


        var file = this.files[0];

        if (file['size'] > maxFileSize) {
            alerts.err("Изображение слишком большое. Максимум 2 MB");
            return false;
        }
        createPreview(file);

        var fullPath = this.value;
        if (fullPath) {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            filenameTxt.html(filename);
        }
    });

    $('#images_container').on('click', ".btn", function (e) {
        var btn = $(this);
        var tr = $(this).parent();
        var row = btn.parent().parent();

        if (btn.attr('class').indexOf("send-image") >= 0) {

            var imagesrc = row.find("img").attr('src');
            var imageFileBase64 = imagesrc.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
            var hash = utils.md5((imageFileBase64));

            if ($.inArray(hash, hashCash) == -1) {

                tr.html('<span class="label label-info">идет загрузка</span>');

                $.post("/app/ads-arenda/upload-images-ajax", {img: imagesrc})
                    .done(function (data) {
                        alerts.printAllAlerts(data.msg);
                        tr.empty();
                        // tr.html('<span class="label label-success">загружено</span> ' + deleteBtn);

                        var newDeleteBtn = $(deleteBtn);
                        newDeleteBtn.attr("data-hash", hash);
                        tr.html('<span class="label label-success">загружено</span> ');
                        newDeleteBtn.appendTo(tr);

                        adForm.append(
                            $('<input>')
                                .attr("type", "hidden")
                                .attr("name", "photos[]")
                                .attr("value", hash)
                        )

                    })
                    .fail(function () {
                        alerts.err("Произошла ошибка. Не удалось загрузить изображение");
                        tr.html('<span class="label label-danger">ошибка</span> ' + deleteBtn);
                    })

            } else {
                alerts.success("Файл успешно сохранен");
                tr.empty();
                // tr.html('<span class="label label-success">загружено</span> ' + deleteBtn);

                var newDeleteBtn = $(deleteBtn);
                newDeleteBtn.attr("data-hash", hash);
                tr.html('<span class="label label-success">загружено</span> ');
                newDeleteBtn.appendTo(tr);

                adForm.append(
                    $('<input>')
                        .attr("type", "hidden")
                        .attr("name", "photos[]")
                        .attr("value", hash)
                )
            }
        }


        if (btn.attr('class').indexOf("delete-image") >= 0) {
            var hashForDelete = btn.data("hash");
            $("input[name^=photos]").each(function (index) {
                if ($(this).val() == hashForDelete) {
                    $(this).remove();
                }
            });

            row.remove();
        }
    });


    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
    };


    function createPreview(file) {

        imageType = "";
        var preview = document.querySelector('img');

        var reader = new FileReader();

        reader.onloadend = function () {

            $('#images_container').find("tbody > tr:last img").attr('src', reader.result);

            var imageFileBase64 = reader.result.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
            var hash = utils.md5((imageFileBase64));


            if ($.inArray(hash, hashCash) == -1) {
                $.post("/app/ads-arenda/image-exists", {
                    hash: hash,
                    type: imageType.split("/")[1]
                })
                    .done(function (data) {

                        if (data.success) {
                            hashCash.push(hash);
                        }
                    })
                    .fail(function () {

                    })


            } else {
                console.log("hashCash");
            }


        }


        if (file) {

            imageType = file['type'];


            images_container.find('tbody').append($('<tr>')
                    .append($('<td>')
                        .append($('<img>')
                            .attr('src', "")
                            .attr('height', "100")
                            .text(file['name'])
                    )
                )
                    .append($('<td>')
                        .text(bytesToSize(file['size']))
                )
                    .append($('<td>')
                        .text((file['name']))
                )
                    .append($('<td>')
                        .html(sendBtn + ' ' + '<button class="btn btn-danger delete-image" type="submit">Удалить</button>')
                )
            );

            reader.readAsDataURL(file);
        }

    }


    uploadForm.on('submit', function (e) {
        e.preventDefault();

        if ($('#image-file').val() == '') {
            return;
        }

        $(this).ajaxSubmit({
            success: function (response, statusText, xhr, $form) {

                progressBar.width('100%')
                statustxt.html('100%');
                filenameTxt.empty();

                alerts.printAllAlerts(response.msg);

                if (!response.success) {
                    var fileInput = $('#image-file');
                    fileInput.replaceWith(fileInput.val('').clone(true));
                }

                progress.hide();

            },
            error: function (a, b, c) {
                console.log(a, b, c);
            },
            uploadProgress: function (event, position, total, percentComplete) {
                //Progress bar
                progress.show();
                progressBar.width(percentComplete + '%')
                statustxt.html(percentComplete + '%');
                //if (percentComplete > 50) {
                //    $('#statustxt').css('color', '#fff'); //change status text to white after 50%
                //}
            }

        });

    });


    return module;
});




