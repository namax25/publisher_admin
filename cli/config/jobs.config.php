<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'Dao\Users' => function ($sm) {
                return new \Dao\Users($sm->get('main_db'));
            },
            'Settings' => function ($sm) {
                $settings = new \Dao\Settings($sm->get('main_db'));

                return $settings->getSettings();
            },
            'log_db' => function ($sm) {
                $log_db = new \Log\Logger();
                $processor = new \Log\Processor\Backtrace();
                $log_db->addProcessor($processor);
                $mapColumn = [
                    'trace' => 'trace',
                    'timestamp' => 'date',
                    'message' => 'message',
                    'priority' => 'priority'
                ];
                $writer = new \Log\Writer\Db($sm->get('main_db'), "log", $mapColumn);
                $log_db->addWriter($writer);
                return $log_db;
            }
        ),
    ),
    'cookie_path' => array(
        "multi" => function () {
            return realpath(
                ROOT_PATH .
                DIRECTORY_SEPARATOR . 'tmp' .
                DIRECTORY_SEPARATOR . 'cookies'
            );
        }
    ),
    'rabbitmq_config' => array(
        "host" => 'localhost',
        "port" => 5672,
        "user" => 'guest',
        "pass" => 'guest',
        "vhost" => '/',
        "ampq_debug" => true,
    ),
    'tesseract_path' => "/usr/local/bin/tesseract",
);
