<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'amazon_ses' => array(
        'aws_access_key_id' => 'test',
        'aws_secret_access_key' => 'test+eLNR79BDrSH+wJcn',
        'region' => 'us-east-1',
    ),
    'sms_twilio' => array(
        'account_id' => 'test',
        'auth_tokem' => 'test',
    ),
    'sms_plivo' => array(
        'account_id' => 'test',
        'auth_tokem' => 'test',
    ),
    'db_smartagent' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=smartagent;host=5.9.7.203',
        'username' => 'root',
        'password' => '1',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
);
