<?php

/**
 *
 * @author namax
 */
class GeneratePass
{


    private $logCli = null;

    /**
     *
     * @var  \Dao\Users
     */
    private $daoUsers = null;
    private $autDbAdapter = null;


    /**
     *
     * @var \Sms\iSms
     */
    protected $smsService = null;

    public function __construct(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $this->setServiceManager($serviceManager);
        $this->daoUsers = new \Dao\Users($serviceManager->get('main_db'));
        $this->autDbAdapter = new \App\Auth\DbAdapter($this->daoUsers);
    }

    public function run($id)
    {

        $this->getLogCli()->info($id);
        $users = $this->daoUsers->getById($id);
        var_dump($users);
        $pass = mt_rand(10000000, 99999999);
        $this->getLogCli()->info($pass);
        $this->getLogCli()->info($this->autDbAdapter->generateNewHash($users, $pass));

    }

    /**
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->serviceManager;
    }

    protected function setServiceManager(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     *
     * @param type $serviceManager
     * @return \Zend\ServiceManager\Config
     */
    protected function getGlobalConfigs()
    {
        return $this->getServiceManager()->get('globalConfig');
    }

    /**
     *
     * @return \Zend\Log\LoggerInterface
     */
    protected function getLogCli()
    {
        if ($this->logCli === null) {
            return $this->getServiceManager()->get('log_cli');
        }
        return $this->logCli;
    }
}

$sendInvitations = new \GeneratePass($serviceManager);

$id = $argv[3];
$sendInvitations->run($id);



