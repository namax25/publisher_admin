<?php

use Zend\Json\Json;

system("clear");
mb_internal_encoding('UTF-8');
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '256M');
set_time_limit(0);

defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__ . '/../'));

//Подключение автозагрузки классов
require_once ROOT_PATH . '/vendor/autoload.php';
require_once ROOT_PATH . '/cli/utils/simple_html_dom.php';


//Конфигурация базы
$dbConfigs = new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/global.php');

//Подключение локального конфига базы
$localConfig = ROOT_PATH . '/cli/config/db/local.php';
if (file_exists($localConfig)) {
    $dbConfigs->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/local.php'));
}

$db = new Zend\Db\Adapter\Adapter($dbConfigs->main_db->toArray());


$log_cli = prepareLog();
//------------------------------------------------------------------------------
$log_cli->warn("Memory used: " . mem());
//------------------------------------------------------------------------------
main();
//------------------------------------------------------------------------------
$log_cli->warn("Memory used: " . mem());

//------------------------------------------------------------------------------

function main()
{
    global $log_cli, $db;


    $j = '["http:\/\/44.img.avito.st\/640x480\/876080244.jpg","http:\/\/09.img.avito.st\/640x480\/876080409.jpg","http:\/\/05.img.avito.st\/640x480\/876080505.jpg"]';
    $j = '[{"url":"http:\/\/91.img.avito.st\/640x480\/837398391.jpg","date":"2014-05-28 15:50:36","attempts":0,"status":1},{"url":"http:\/\/39.img.avito.st\/640x480\/837399039.jpg","date":"2014-05-28 15:50:36","attempts":0,"status":1},{"url":"http:\/\/47.img.avito.st\/640x480\/837401347.jpg","date":"2014-05-28 15:50:36","attempts":0,"status":1},{"url":"http:\/\/38.img.avito.st\/640x480\/837401938.jpg","date":"2014-05-28 15:50:36","attempts":0,"status":1},{"url":"http:\/\/99.img.avito.st\/640x480\/837402699.jpg","date":"2014-05-28 15:50:36","attempts":0,"status":1},{"url":"http:\/\/64.img.avito.st\/640x480\/837403464.jpg","date":"2014-05-28 15:50:37","attempts":0,"status":1},{"url":"http:\/\/33.img.avito.st\/640x480\/837404233.jpg","date":"2014-05-28 15:50:37","attempts":0,"status":1},{"url":"http:\/\/74.img.avito.st\/640x480\/837405274.jpg","date":"2014-05-28 15:50:37","attempts":0,"status":1},{"url":"http:\/\/45.img.avito.st\/640x480\/837405845.jpg","date":"2014-05-28 15:50:37","attempts":0,"status":1},{"url":"http:\/\/78.img.avito.st\/640x480\/837408178.jpg","date":"2014-05-28 15:50:37","attempts":0,"status":1},{"url":"http:\/\/68.img.avito.st\/640x480\/837408868.jpg","date":"2014-05-28 15:50:37","attempts":0,"status":1},{"url":"http:\/\/96.img.avito.st\/640x480\/837410696.jpg","date":"2014-05-28 15:50:38","attempts":0,"status":1}]';

    //var_dump(Json::decode($j));


    $msgArr = [
        "id" => "3224",
        "user_id" => 1,
        "type" => "Сдам",
        "category" => "Квартиры",
        "city" => "Москва",
        "address" => "ул. Ленина 14, д5",
        "metro" => "Авиамоторная",
        "rooms" => 4,
        "price" => "50000",
        "floor" => "1",
        "floors" => "5",
        "period" => "На длительный срок",
        "hometype" => "Монолитный",
        "area" => 50,
        "created_at" => "79056545832",
        "images" => [
            "C:\\Users\\namax_w7\\Pictures\\girl.jpg",
            "C:\\Users\\namax_w7\\Pictures\\girl1.jpg"
        ],
        "description" => "новый дом, балкон, диван-кровать, 2 кресла, кресло, журнальный стол, стол, ковер, диван, Площадь 43 м2   дом 81 кв 10  можно и раньше батареи там надо сделать если клиенты согласны  раньше....",
        "name" => "Иван",
        "email" => "ivan@gmail.com",
        "phone" => "79056545832",
    ];

    //echo json_encode($msgArr);

//    $img = new \Dao\Images($db);
//    var_dump($img->update(['date_last_update' => "2015-02-19 21:23:21"], ['id'=> 1]));
//echo md5(base64_encode(file_get_contents("www/uploads/e5/c9/e5c98e8f783aa5f8b15b9422a0a109ab.jpg"))).PHP_EOL;
//echo md5( (file_get_contents("www/uploads/e5/c9/e5c98e8f783aa5f8b15b9422a0a109ab.jpg"))).PHP_EOL;
// echo md5_file("www/uploads/e5/c9/e5c98e8f783aa5f8b15b9422a0a109ab.jpg");

    $path = ROOT_PATH . "/www/uploads/4f/b2/4fb29ce28b21a2bdb250c500a2ea299d.png";
    \Stopwatch::start();
    $log_cli->warn(__FUNCTION__ . __LINE__ . " Memory used: " . mem());

    $file = file_get_contents($path);
    $log_cli->warn(__FUNCTION__ . __LINE__ . " Memory used: " . mem());

    foo($file);
    $log_cli->warn(__FUNCTION__ . __LINE__ . " Memory used: " . mem());

    file_exists($path);
    $log_cli->info("Time used: " . \Stopwatch::end());


    $dao = new \Dao\Images($db);
    $j = $dao->getByHash('d84f7eb25d5ae9346149ad8d669c9cc9');
    $dao->updateImageDate('d84f7eb25d5ae9346149ad8d669c9cc9');
    var_dump($j);


//    $log_cli->warn("Memory used: " . mem());
//
//    \Stopwatch::start();1
//    $i = 0;
//    resize("www/uploads/4b/7f", "4b7facc5b2409d7b84265b68df9c065b.jpg", "test" . $i . ".jpg");
//    resize("www/uploads/4b/7f", "4b7facc5b2409d7b84265b68df9c065b.jpg", "test2" . $i . ".jpg");
//    $log_cli->info("Time used: " . \Stopwatch::end());

    $log_cli->warn("Memory used: " . mem());
}

function foo($file)
{
    $file .= $file;
    return 3;
}

function resize($path, $imagename, $newname)
{
    $maxImageWidth = 800;
    $maxImageHeight = 600;

    $image = new \Imagick($path . DIRECTORY_SEPARATOR . $imagename);

    if ($image->getimagewidth() > $maxImageWidth || $image->getimageheight() > $maxImageHeight) {
        $image->resizeImage($maxImageWidth, $maxImageHeight, \Imagick::FILTER_LANCZOS, 1, true);
    }

    $image->setImageBackgroundColor(new \ImagickPixel('white'));
    $image->flattenImages();
    $image->setImageFormat('jpg');

    $image->writeimage($path . DIRECTORY_SEPARATOR . $newname);
    $image->clear();
    $image->destroy();

}

function updateHashInGrabbersAdsFull()
{
    global $log_cli, $db;

    $daoGrabbersAdsFull = new \Dao\GrabbersAdsFull($db);

    while ($ads = $daoGrabbersAdsFull->getImportedAdsWithEmptyHash()) {

        foreach ($ads as $ad) {
            $hash = \Helpers\Hash::create()->fromArr($ad, [
                'phone',
                'price',
                'street',
                'building',
                'corpus',
                'metro',
                'period',
                'distance',
                'transport',
                'rooms',
                'floor',
                'floors',
                'client_commission'
            ]);
            $log_cli->log(\Log\Logger::OK, "Update hash for " . $ad['id']);
            $daoGrabbersAdsFull->update(['hash' => $hash], ['id' => $ad['id']]);
        }
    }
}

function responseHandlerCallbackForList($info, $output, $error)
{
    var_dump($error);
    echo $output;
}

function testSplContainers()
{
    global $log_cli;
    $log_cli->warn("Memory used: " . mem());

    $s = new SplQueue();
//    $s = new SplDoublyLinkedList();
//    $s = new SplObjectStorage();
//    $s = new intPq();
//    $s = [];
    for ($i = 1; $i < 3; $i++) {
        $s->enqueue(new Proxy("1.1.1." . $i, $i));
//        $s->push(new Proxy("1.1.1.1", $i));
//        $s->attach(new Proxy("1.1.1.1", $i));
//            $s->insert(new Proxy("1.1.1.1", $i), $i);
//        $s[] = ['url' => "1.1.1.1", 'port' => $i];
    }
    var_dump($s->offsetGet(1));


    $p = $s->dequeue();
    var_dump($p);

    $s->enqueue($p);
    var_dump($s->offsetGet(1));

//    $s->rewind();
//    do {
//        $p = $s->current();
//        var_dump($s->key(), $s->current(), $s->isEmpty());
//        $s->next();
//        $p->incErr();
//    } while ($s->valid());
//    $s->rewind();
//    do {
//        var_dump($s->key(), $s->current(), $s->isEmpty());
//        $s->next();
//    } while ($s->valid());
    $log_cli->warn("Memory used: " . mem());
}


class intPq extends SplPriorityQueue
{

    public function compare($priority1, $priority2)
    {
        if ($priority1 === $priority2) {
            return 0;
        }
        return $priority1 < $priority2 ? 1 : -1;
    }
}

class Proxy
{

    private $url = null;
    private $port = null;
    private $error = 0;

    public function __construct($url, $port)
    {
        $this->url = $url;
        $this->port = $port;
    }

    public function incErr()
    {
        $this->error++;
    }
}

//------------------------------------------------------------------------------

function prepareLog()
{
    $processor = new \Log\Processor\Backtrace();
    $formatter_cli = new \Log\Formatter\ColorCli();
    $log_cli = new \Log\Logger();
    $writerStream = new \Zend\Log\Writer\Stream('php://output');
    $writerStream->setFormatter($formatter_cli);
    $log_cli->addProcessor($processor);
    $log_cli->addWriter($writerStream);
    return $log_cli;
}

function mem()
{
    $size = memory_get_usage(true);
    $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}
