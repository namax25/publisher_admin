<?php
/**
 * Date: 12/25/14
 * Time: 4:28 PM
 * @author namax
 */


use PhpAmqpLib\Connection\AMQPConnection;
use Zend\Json\Json;

$config = $serviceManager->get('globalConfig')->rabbitmq_config;

$connection = new AMQPConnection($config->host, $config->port, $config->user, $config->pass);
$channel = $connection->channel();
$channel->queue_declare('importads_smartagent', false, false, false, false);

$smartagentDb = new Zend\Db\Adapter\Adapter($serviceManager->get('globalConfig')->db_smartagent->toArray());

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

/*$result = $serviceManager->get('main_db')->query('select * from arenda WHERE `id` = ?', array(5));
foreach ($result as $row) {
    var_dump($row);

}*/

$daoFactory = (new \Dao\DaoFactory())->setDb($serviceManager->get('main_db'));


$callback = function ($msg) {
    global $daoFactory, $log_cli;

    echo " [x] Received ", $msg->body, "\n";
    $data = json_decode($msg->body);
    var_dump($data);

    $amount = 10;
    $offset = 0;
    $total = 0;
    $id = $data->user_id;
    $old_id = $data->old_id;
    $userData = getUserData($old_id);
    do {
        $ads = getAds($old_id, $amount, $offset);

        foreach ($ads as $ad) {
            echo " [x] Importing ", $ad->id, "\n";
            $data = convertSmartagentAd($ad, $id, $userData);
            if ($data) {
                $daoFactory->getDaoArenda()->insert($data);
                $total++;
                $log_cli->info(" [x] Imported Successfully");
            }


        }
        $offset += $amount;

    } while ($ads->count() >= $amount);

    echo " [x] Imported ", $total, "\n";


};

$channel->basic_consume('importads_smartagent', '', false, true, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}


register_shutdown_function('shutdown', $channel, $connection);

function shutdown($ch, $conn)
{
    $ch->close();
    $conn->close();
}


/**
 * @param $id
 * @return \Zend\Db\Adapter\Driver\StatementInterface|\Zend\Db\ResultSet\ResultSet
 */
function getAds($id, $amount, $offset)
{
    global $smartagentDb;

    return $smartagentDb->query('SELECT
	id,
	region_id,
	street_cache,
	metro_ids,
	rooms,
	price,
	floor,
	floors,
	period,
	building,
	corpus,
	hometype,
	total_area,
	remont,
	created_at,
	images,
	user_id_who_created,
	name,
	note
 FROM arenda WHERE `user_id_who_created` = ? limit ' . $offset . ',' . $amount, [$id]);
}


function getUserData($id)
{
    global $smartagentDb;
    $result = $smartagentDb->query('SELECT id, name FROM users WHERE `id` = ?', [$id]);
    return $result->current();
}


function convertSmartagentAd($smartagentAd, $userId, $userDataOnOriginalSite)
{
    global $log_cli;

    $allowedNullKeys = ['images',];

    $item['id_old'] = $smartagentAd['id'];
    $item['user_id'] = $userId;
    $item['type'] = \Consts\Table\Arenda\AdType::SDAM;

    $item['category'] = $smartagentAd['rooms'] == \Consts\Table\Arenda\Rooms::ROOMS_21 ?
        \Consts\Table\Arenda\Category::KOMNATY :
        \Consts\Table\Arenda\Category::KVARTIRY;

    $item['city'] = $smartagentAd['region_id'];
    $item['address'] = $smartagentAd['street_cache'];
    $item['metro'] = \Helpers\Strings::create()->clearDigits($smartagentAd['metro_ids']);
    $item['rooms'] = $smartagentAd['rooms'];
    $item['price'] = $smartagentAd['price'];
    $item['floor'] = $smartagentAd['floor'];
    $item['floors'] = $smartagentAd['floors'];
    $item['period'] = $smartagentAd['period'];
    $item['hometype'] = $smartagentAd['hometype'];
    $item['area'] = $smartagentAd['total_area'];
    $item['created_at'] = $smartagentAd['created_at'];
    //$item['images'] = $smartagentAd['images'];
    $item['description'] = $smartagentAd['note'];
    $item['name'] = $smartagentAd['name'];

    if (is_null($item['name'])) {
        $item['name'] = $userDataOnOriginalSite->name;
    }

    // $item['email'] = $smartagentAd[''];

    foreach ($item as $key => $val) {

        If (is_null($val) && $key !== 'images') {

            $log_cli->info(" [x] Key is null " . $key);
            return false;
        }
    }

    return $item;

}


function parserImages($images)
{
    if (!$images) {

        return Json::encode([]);
    }


    $imagesArr = Json::decode($images);


}