<?php

/**
 * Скрипт запуска jobs.
 *
 */
//Очистка и начальные переменные
system("clear");
mb_internal_encoding('UTF-8');
error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(0);
date_default_timezone_set('UTC');

//Глобальная константа коренной папки
defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__ . '/../'));

//Подключение автозагрузки классов
require_once ROOT_PATH . '/vendor/autoload.php';
require_once ROOT_PATH . '/cli/utils/simple_html_dom.php';


//Конфигурация лога
$processor = new \Log\Processor\Backtrace();
$formatter_cli = new \Log\Formatter\ColorCli();
$log_cli = new \Log\Logger();
$writerStream = new \Zend\Log\Writer\Stream('php://output');
$writerStream->setFormatter($formatter_cli);
$log_cli->addProcessor($processor);
$log_cli->addWriter($writerStream);


//Конфигурация базы
$dbConfigs = new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/global.php');

//Подключение локального конфига базы
$localConfig = ROOT_PATH . '/cli/config/db/local.php';
if (file_exists($localConfig)) {
    $dbConfigs->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/local.php'));
}

$main_db = new Zend\Db\Adapter\Adapter($dbConfigs->main_db->toArray());


//Распарсевание параметров командной строки
$options = getopt("n:h");

if (isset($options['h'])) {
    printHelp();
}


if (!isset($options['n'])) {
    $log_cli->err("Script name is not specified");
    printHelp();
    die();
}


$name = $options['n'];

//Подключение пускового файла
$file = ROOT_PATH . '/cli/goscripts/' . $name . '.php';

if (!file_exists($file)) {
    $log_cli->err("File does not exists " . $file);
    die();
}


//Глобальный конфиг
$jobsConfigArr = require ROOT_PATH . '/cli/config/jobs.config.php';


$grabberFullLaunchCommand = implode(" ", $argv);

if (!Process::check($grabberFullLaunchCommand)) {
    $log_cli->err('Script has been already running');
    exit(1);
}


//Конфиг сервис менеджера
$smGlobalConfig = [];
if (isset($jobsConfigArr['service_manager'])) {

    $smGlobalConfig = $jobsConfigArr['service_manager'];
    unset($jobsConfigArr['service_manager']);
}

$globalConfig = new \Zend\Config\Config($jobsConfigArr);

//Подключение локального конфига  
$localJobConfig = ROOT_PATH . '/cli/config/jobs.config.local.php';
if (file_exists($localJobConfig)) {
    $globalConfig->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/jobs.config.local.php'));
}

//Сервис менеджер
$serviceManager = new \Zend\ServiceManager\ServiceManager();
//регистрируем как сервис уже готовые объекты
$serviceManager->setService('main_db', $main_db);
$serviceManager->setService('globalConfig', $globalConfig);
$serviceManager->setService('log_cli', $log_cli);
$serviceManagerConf = new \Zend\Mvc\Service\ServiceManagerConfig($smGlobalConfig);
$serviceManagerConf->configureServiceManager($serviceManager);

function printHelp()
{
    echo "\n";
    echo <<<EOF
----- Go Scripts Help -----
  -n    Script name (same name as file in cli/goscripts)
  -h    help
    
Examples:    
    1. Go Script event_notifier.php
    php go.php -n event_notifier
 
-------------------------
EOF;
    echo "\n";
    echo "\n";
    die();
}

require_once $file;


