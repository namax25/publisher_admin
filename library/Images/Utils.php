<?php
/**
 * Date: 2/23/15
 * Time: 12:23 PM
 * @author namax
 */

namespace Images;


class Utils
{
    const MAX_FILE_SIZE = 2097152;

    public function isImageToLarge($fileContent)
    {
        return strlen($fileContent) > self::MAX_FILE_SIZE;
    }

    public function getExtensionByType($ext)
    {
        $ext = strtolower($ext);

        switch ($ext) {
            case "png":
                return "png";
            case "jpeg":
            case "jpg":
                return "jpg";
        }

        return false;
    }


}