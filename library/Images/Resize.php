<?php
/**
 * Date: 2/23/15
 * Time: 12:08 PM
 * @author namax
 */

namespace Images;


class Resize
{
    private $compressionQuality = 60;
    const MAX_IMAGE_WIDTH = 800;
    const MAX_IMAGE_HEIGHT = 600;
    const MAX_THUMB_WIDTH = 150;
    const MAX_THUMB_HEIGHT = 100;

    const NORMAL_IMAGE_EXTENSION = "_n.jpg";
    const SMALL_IMAGE_EXTENSION = "_s.jpg";

    public function resize($path, $imagename, $newname, $maxImageWidth, $maxImageHeight)
    {
        $image = new \Imagick($path . DIRECTORY_SEPARATOR . $imagename);

        $image->setImageCompression(\Imagick::COMPRESSION_JPEG);
        $image->setImageCompressionQuality($this->getCompressionQuality());

        if ($image->getimagewidth() > $maxImageWidth || $image->getimageheight() > $maxImageHeight) {
            $image->resizeImage($maxImageWidth, $maxImageHeight, \Imagick::FILTER_LANCZOS, 1, true);
        }

        $image->setImageBackgroundColor(new \ImagickPixel('white'));
        $image->flattenImages();
        $image->setImageFormat('jpg');


        $image->writeimage($path . DIRECTORY_SEPARATOR . $newname);
        $image->clear();
        $image->destroy();


    }

    public function createStandartSizePhotos($path, $imagename, $hash)
    {
        $this->resizeNormal($path, $imagename, $hash);
        $this->resizeThumbs($path, $imagename, $hash);
    }

    public function resizeNormal($path, $imagename, $hash)
    {
        if (!file_exists($path . DIRECTORY_SEPARATOR . $hash . self::NORMAL_IMAGE_EXTENSION)) {
            $this->resize(
                $path,
                $imagename,
                $hash . self::NORMAL_IMAGE_EXTENSION,
                self::MAX_IMAGE_WIDTH,
                self::MAX_IMAGE_HEIGHT
            );
        }
    }

    public function resizeThumbs($path, $imagename, $hash)
    {

        if (!file_exists($path . DIRECTORY_SEPARATOR . $hash . self::SMALL_IMAGE_EXTENSION)) {
            $this->resize(
                $path,
                $imagename,
                $hash . self::SMALL_IMAGE_EXTENSION,
                self::MAX_THUMB_WIDTH,
                self::MAX_THUMB_HEIGHT
            );

        }

    }

    /**
     * @return int
     */
    public function getCompressionQuality()
    {
        return $this->compressionQuality;
    }

    /**
     * @param int $compressionQuality
     */
    public function setCompressionQuality($compressionQuality)
    {
        $this->compressionQuality = (int)(string)$compressionQuality;
        return $this;
    }


}