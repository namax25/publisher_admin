<?php
/**
 * Date: 12/13/14
 * Time: 4:40 PM
 * @author namax
 */

namespace Converters;


interface Convertable
{
    public function convert($data);
} 