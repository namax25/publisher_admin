<?php
/**
 * Date: 12/16/14
 * Time: 6:39 PM
 * @author namax
 */

namespace Converters\Ads;

use Converters\Convertable;

class AdToAvito implements Convertable
{


    protected $categoryNames = [
        \Consts\Table\Arenda\Category::KVARTIRY => 'Квартиры',
        \Consts\Table\Arenda\Category::KOMNATY => 'Комнаты',
    ];

    protected $metroNames = [];

    protected $hometypeNames = [
        \Consts\Table\Arenda\Hometype::BRICK => 'Кирпичный',
        \Consts\Table\Arenda\Hometype::PANEL => 'Панельный',
        \Consts\Table\Arenda\Hometype::BLOCK => 'Блочный',
        \Consts\Table\Arenda\Hometype::MONOLITH => 'Монолитный',
        \Consts\Table\Arenda\Hometype::WOOD => 'Деревянный',
    ];


    protected $rooms = [
        \Consts\Table\Arenda\Rooms::ROOMS_21 => 'Студия',
        \Consts\Table\Arenda\Rooms::ROOMS_20 => 'Студия',
        \Consts\Table\Arenda\Rooms::ROOMS_1 => '1',
        \Consts\Table\Arenda\Rooms::ROOMS_2 => '2',
        \Consts\Table\Arenda\Rooms::ROOMS_3 => '3',
        \Consts\Table\Arenda\Rooms::ROOMS_4 => '4',
        \Consts\Table\Arenda\Rooms::ROOMS_5 => '5',
        \Consts\Table\Arenda\Rooms::ROOMS_6 => '6',
        \Consts\Table\Arenda\Rooms::ROOMS_7 => '7',
        \Consts\Table\Arenda\Rooms::ROOMS_8 => '8',
        \Consts\Table\Arenda\Rooms::ROOMS_9 => '9',
    ];



    protected $periodNames = [
        \Consts\Table\Arenda\Period::LONG => 'На длительный срок',
        \Consts\Table\Arenda\Period::PER_DAY => 'Посуточно',
    ];

    protected $daoFactory;

    public function __construct(\Dao\DaoFactory $dao)
    {
        $this->daoFactory = $dao;
    }


    public function convert($data)
    {

        $result = [];
        $this->convertField($result, $data, 'type', \Consts\Table\Arenda\AdType::$names);
        $this->convertField($result, $data, 'category', $this->categoryNames);
        $this->convertField($result, $data, 'city', \Consts\Regions::$names);
        $this->convertField($result, $data, 'metro', $this->getMetroNames());
        $this->convertField($result, $data, 'period', $this->periodNames);
        $this->convertField($result, $data, 'hometype', $this->hometypeNames);

        if (!empty($data['images'])) {
            $data['images'] = \Zend\Json\Json::decode($data['images']);
        } else {
            $data['images'] = [];
        }
        $this->unsetFields($data);

        return array_merge($data, $result);
    }


    protected function getMetroNames()
    {
        if (!$this->metroNames) {
            $names = $this->daoFactory->getDaoMetro()->getAll()->toArray();
            $this->metroNames = \Helpers\RearrangeArray::create()->byKeys($names, 'id', 'name');
        }

        return $this->metroNames;
    }

    protected function unsetFields(&$data)
    {

        unset($data['id_old']);
        unset($data['publication_date']);
    }


    protected function convertField(&$result, $data, $field, $names)
    {

        if (!isset($data[$field])) {
            throw new \Exception(__METHOD__ . " Not found field in data '" . $field . "'");
        }

        if (!array_key_exists($data[$field], $names)) {
            throw new \Exception(__METHOD__ . " Name not found for field '" . $field . "'");
        }


        $result[$field] = $names[$data[$field]];

    }

}