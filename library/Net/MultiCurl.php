<?php

namespace Net;

use Zend\Config\Config;

/**
 * Description of MultiCurl
 *
 * @author namax
 */
class MultiCurl
{

    private $threads = 3;
    private $cookiePath = null;
    private $curlOptions = [];
    private $proxyManger = null;
    private $stopFlag = false;
    private $handlersToProxyMap = [];
    private $handlersToOriginUrlsMap = [];
    private $handlers = [];

    const ATTEMPS_BEFORE_STOP = 100;
    const MIN_ACTIVE_THREADS = 3;

    public function run(\Iterator $urls, $callback)
    {
        $this->cleanCookiesStorage();
        $mh = curl_multi_init();
        for ($i = 0; $i < $this->threads; $i++) {
            $ch = $this->prepareHandler($urls);
            if (!$ch) {
                break;
            }
            curl_multi_add_handle($mh, $ch);
        }

        while (curl_multi_exec($mh, $running) == CURLM_CALL_MULTI_PERFORM) {
            ;
        }


        usleep(100000);
        $execrun = curl_multi_exec($mh, $running);

        $stopper = 0;

        while ($running) {
            curl_multi_select($mh);
            usleep(500000);


            while (($execrun = curl_multi_exec($mh, $running)) == CURLM_CALL_MULTI_PERFORM) {
                ;
            }

            if ($execrun != CURLM_OK || $stopper > self::ATTEMPS_BEFORE_STOP) {
                echo "Sopper: " . ($stopper) . PHP_EOL;
                echo "Execrun: " . $execrun . PHP_EOL;

                break;
            }

            while ($done = curl_multi_info_read($mh)) {

                $info = curl_getinfo($done['handle']);
                $output = curl_multi_getcontent($done['handle']);
                $info['handle_str'] = (string)$done['handle'];
                $info['orig_url'] = $this->getHandlersToOriginUrlsMap($done['handle']);
                $this->unsetHandlersToOriginUrlsMap($done['handle']);

                $error = curl_error($done['handle']);

                $code = $this->handleResponse($callback, $done['handle'], $info, $output, $error);

                $this->codeHandler($code, $mh, $urls, $info);

                curl_multi_remove_handle($mh, $done['handle']);
                curl_close($done['handle']);
            }
            if (count($running) < self::ATTEMPS_BEFORE_STOP) {
                $stopper++;
            }
//            var_dump($running);
//            curl_multi_select($mh);
        }

        if ($this->handlers) {
            echo "Handlers: " . count($this->handlers) . PHP_EOL;

            foreach ($this->handlers as $handler) {
                echo "Remove handler: " . (string)$handler . PHP_EOL;
                curl_multi_remove_handle($mh, $handler);
                curl_close($handler);
            }
        }

        curl_multi_close($mh);
        $this->handlers = [];
        $this->handlersToOriginUrlsMap = [];
        return true;
    }

    protected function handleResponse($callback, $ch, $info, $output, $error)
    {
        if ($this->getProxyManger()) {
            $this->getProxyManger()->setLastError($this->getHandlersToProxyMap($ch), $info['http_code'], $error);
            $this->unsetHandlersToProxyMap($ch);
        }

        $code = Consts\CurlControlStatus::CONTINUE_WORK;
        if (is_callable($callback)) {
            $code = $callback($info, $output, $error);
        }

        return $code;
    }

    /**
     * 0 - success , can take next url
     * 1 - return url to queue
     * 255 - stop code
     * @param type $code
     */
    protected function codeHandler($code, &$mh, $urls, $info)
    {
        switch ($code) {
            case Consts\CurlControlStatus::CONTINUE_WORK:
                if (!$this->getStopFlag()) {
                    $ch = $this->prepareHandler($urls);
                    if ($ch) {
                        curl_multi_add_handle($mh, $ch);
                    }
                }
                break;
            case Consts\CurlControlStatus::PUT_URL_BACK_TO_QUEUE:
//                if (!$this->getStopFlag()) {
//                    Log::display(new Log_Status('PUT_URL_BACK_TO_QUEUE'));
//                    curl_multi_add_handle($mh, $this->prepareHandler($info['url']));
//                }
                break;
            case Consts\CurlControlStatus::SET_STOP_FLAG:
                $this->setStopFlag(true);
                break;
        }
    }

    protected function prepareHandler(\Iterator $urls)
    {

        $options = [];

        if (!$this->setUrlOption($urls, $options)) {
            return false;
        }

        $ch = curl_init();

        $this->setHandlersToOriginUrlsMap($ch, $options[CURLOPT_URL]);
        $this->setCommonOptions($options);
        $this->setCookieOptions($options);
        $this->setProxyOptions($options, $ch);

        if (is_array($this->getOptions())) {
            foreach ($this->getOptions() as $key => $option) {
                $options[$key] = $option;
            }
        }

        curl_setopt_array($ch, $options);
        return $ch;
    }

//<editor-fold defaultstate="collapsed" desc="Set Oprions For Curl Handler">
    protected function setUrlOption(\Iterator $urls, &$options)
    {
        if ($urls->valid()) {
            $options[CURLOPT_URL] = trim($urls->current());
            $urls->next();
            return true;
        }
        return false;
    }

    protected function setCommonOptions(&$options)
    {
        $options[CURLOPT_USERAGENT] = $this->getRandomUserAgent();
        $options[CURLOPT_RETURNTRANSFER] = true;
        $options[CURLOPT_HEADER] = false;
        $options[CURLOPT_TIMEOUT] = 20;
        $options[CURLOPT_CONNECTTIMEOUT] = 10;
    }

    protected function setProxyOptions(&$options, $ch)
    {
        if ($this->getProxyManger()) {
            $proxy = $this->getProxyManger()->getProxy();
            if ($proxy == null) {
                throw new \Exception("Not enough proxy");
            }
            $this->setHandlersToProxyMap($ch, $proxy);
            $options[CURLOPT_PROXY] = $proxy->getUrl() . ":" . $proxy->getPort();
            if ($proxy->getLogin()) {
                $options[CURLOPT_PROXYUSERPWD] = $proxy->getLogin() . ":" . $proxy->getPassword();
                $options[CURLOPT_PROXYAUTH] = 1;
            }
        }
    }

    protected function setCookieOptions(&$options)
    {
        if ($this->getCookiePath()) {
            $cookieFileName = "cookie_" . time() . mt_rand();
            $options[CURLOPT_COOKIEFILE] = $this->getCookiePath() . DIRECTORY_SEPARATOR . $cookieFileName . '.txt';
            $options[CURLOPT_COOKIEJAR] = $this->getCookiePath() . DIRECTORY_SEPARATOR . $cookieFileName . '.txt';
        }
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Getters\Setters">

    /**
     *
     * @param type curlHandler
     * @return Grabbers_Proxy_ProxyEntity
     */
    public function getHandlersToProxyMap($ch)
    {
        $key = (string)$ch;
        if (isset($this->handlersToProxyMap[$key])) {
            return $this->handlersToProxyMap[$key];
        }
        return false;
    }

    public function setHandlersToProxyMap($ch, \Entity\Proxy $proxy)
    {
        $key = (string)$ch;
        $this->handlersToProxyMap[$key] = $proxy;
    }

    public function unsetHandlersToProxyMap($ch)
    {
        $key = (string)$ch;
        unset($this->handlersToProxyMap[$key]);
    }

    public function getHandlersToOriginUrlsMap($ch)
    {
        $key = (string)$ch;
        if (isset($this->handlersToOriginUrlsMap[$key])) {
            return $this->handlersToOriginUrlsMap[$key];
        }
        return false;
    }

    protected function unsetHandlersToOriginUrlsMap($ch)
    {
        $key = (string)$ch;

        unset($this->handlers[$key]);
        unset($this->handlersToOriginUrlsMap[$key]);
    }

    protected function setHandlersToOriginUrlsMap($ch, $url)
    {
        $key = (string)$ch;
        $this->handlers[$key] = $ch;

        $this->handlersToOriginUrlsMap[$key] = $url;
    }

    public function setOptions($options)
    {
        $this->curlOptions = $options;
    }

    public function addOptions($options)
    {
        if (is_array($options)) {
            $this->curlOptions = array_merge($this->curlOptions, $options);
        }
    }

    public function getOptions()
    {
        return $this->curlOptions;
    }

    public function getThreads()
    {
        return $this->threads;
    }

    public function setThreads($threads)
    {
        $this->threads = (int)$threads;
    }

    public function getStopFlag()
    {
        return $this->stopFlag;
    }

    public function setStopFlag($stopFlag)
    {
        $this->stopFlag = $stopFlag;
    }

    public function getCookiePath()
    {
        return $this->cookiePath;
    }

    public function setCookiePath($cookiePath)
    {
        $this->cookiePath = $cookiePath;
    }

    /**
     *
     * @return \Proxy\Manager\ProxyManagerInterface
     */
    public function getProxyManger()
    {
        return $this->proxyManger;
    }

    public function setProxyManger(\Proxy\Manager\ProxyManagerInterface $proxyManger)
    {
        $this->proxyManger = $proxyManger;
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Utils">

    protected function getRandomUserAgent()
    {
        return \Net\Helpers\UserAgents::create()->getRandom();
    }

    /**
     * Clean cookies storage
     */
    protected function cleanCookiesStorage()
    {
        if ($this->getCookiePath()) {
            $itr = new \DirectoryIterator($this->getCookiePath());
            foreach ($itr as $item) {

                if (!$item->isFile() || $item->getExtension() != 'txt' || !preg_match("/^cookie_\d+\.txt/",
                        $item->getFilename())
                ) {
                    continue;
                }
                unlink($item->getPathname());
            }
        }
    }

// </editor-fold>
}
