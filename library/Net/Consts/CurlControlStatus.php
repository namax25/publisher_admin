<?php

namespace Net\Consts;

/**
 * Description of CurlControlStatus
 *
 * @author namax
 */
class CurlControlStatus
{

    const CONTINUE_WORK = 0;
    const PUT_URL_BACK_TO_QUEUE = 1;
    const SET_STOP_FLAG = 255;

}
