<?php

/**
 * Description of Lock
 * создает лок на самом файле не давай запустить его дважды
 * Актуально для консоли
 *
 */
class Lock
{

    private $fp;

    function __construct()
    {
        $this->fp = fopen(__FILE__, 'r');
        if (!flock($this->fp, LOCK_EX | LOCK_NB)) {
            die('Already running !' . PHP_EOL);
        }
    }

    function __destruct()
    {
        flock($this->fp, LOCK_UN);
        fclose($this->fp);
    }

}
