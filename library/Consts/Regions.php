<?php

namespace Consts;

/**
 * Description of Regions
 *
 * @author namax
 */
class Regions
{

    const MSK = 46;
    const MO = 47;
    const LEN_OB = 41;
    const SPB = 63;

    static public $allParams = [
        self::MSK,
        self::MO,
        self::LEN_OB,
        self::SPB,
    ];
    static public $names = [
        self::MSK => "Москва",
        self::MO => "Московская область",
        self::LEN_OB => "Ленинградская область",
        self::SPB => "Санкт-Петербург"
    ];

}
