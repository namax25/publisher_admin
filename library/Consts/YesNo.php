<?php

namespace Consts;

/**
 * Description of Common
 *
 * @author namax
 */
class YesNo
{

    const NO = 0;
    const YES = 1;

    static $allParams = [
        self::NO,
        self::YES,
    ];

}
