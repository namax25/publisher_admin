<?php

namespace Consts\Table\Arenda;

/**
 * Description of AdType
 *
 * @author namax
 */
class AdType
{


    const SDAM = 1;
    const SNIMU = 2;
    const PRODAM = 3;
    const KUPLYU = 4;

    static public $allParams = [
        self::SDAM,
        self::SNIMU,
        self::PRODAM,
        self::KUPLYU,
    ];

    static public $names =
        [
            self::SDAM => 'Сдам',
            self::SNIMU => 'Сниму',
            self::PRODAM => 'Продам',
            self::KUPLYU => 'Куплю'
        ];


}
