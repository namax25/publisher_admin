<?php

namespace Consts\Table\Arenda;

class DealType
{

    const UNKNOWN = 0;
    const JUST_SELL = 1;
    const ALTERNATIVE = 2;

    static public $allParams = array(
        self::UNKNOWN,
        self::JUST_SELL,
        self::ALTERNATIVE,
    );

}