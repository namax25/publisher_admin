<?php

namespace Consts\Table\Arenda;

/**
 * Description of Period
 *
 * @author namax
 */
class Period
{

    const UNKNOWN = 0;
    const PER_DAY = 1;
    const SHORT = 2;
    const LONG = 3;

    static public $allParams = [self::UNKNOWN, self::PER_DAY, self::SHORT, self::LONG];

    static public $names = [
        \Consts\Table\Arenda\Period::LONG => 'На длительный срок',
        \Consts\Table\Arenda\Period::PER_DAY => 'Посуточно',
    ];

}
