<?php

namespace Consts\Table\Arenda;

/**
 * Description of Ads_Datatypes_Hometype_WhiteList
 *
 * @author namax
 */
class Hometype
{

    const UNKNOWN = 0;
    const BLOCK = 1;
    const PANEL = 2;
    const MONOLITH = 3;
    const BRICK = 4;
    const WOOD = 5;

    static public $allParams = array(
        self::UNKNOWN,
        self::BLOCK,
        self::PANEL,
        self::MONOLITH,
        self::BRICK,
        self::WOOD
    );


    static public $names = [
        \Consts\Table\Arenda\Hometype::BRICK => 'Кирпичный',
        \Consts\Table\Arenda\Hometype::PANEL => 'Панельный',
        \Consts\Table\Arenda\Hometype::BLOCK => 'Блочный',
        \Consts\Table\Arenda\Hometype::MONOLITH => 'Монолитный',
        \Consts\Table\Arenda\Hometype::WOOD => 'Деревянный',
    ];

}
