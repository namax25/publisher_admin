<?php

namespace Consts\Table\Arenda;

/**
 * Description of Rooms
 *
 * @author namax
 */
class Rooms
{

    const UNKNOWN = 0;
    const ROOMS_1 = 1;
    const ROOMS_2 = 2;
    const ROOMS_3 = 3;
    const ROOMS_4 = 4;
    const ROOMS_5 = 5;
    const ROOMS_6 = 6;
    const ROOMS_7 = 7;
    const ROOMS_8 = 8;
    const ROOMS_9 = 9;

    /**
     * Таунхаус
     */
    const TOWNHOUSE = 9;

    /**
     * Дом
     */
    const HOME = 10;

    /**
     * Койко-место
     */
    const ROOMS_20 = 20;

    /**
     * Комната
     */
    const ROOMS_21 = 21;


    /**
     * Доля квартиры
     */
    const ROOMS_DOLYA = 22;

    static public $allParams = array(
        self::UNKNOWN,
        self::ROOMS_1,
        self::ROOMS_2,
        self::ROOMS_3,
        self::ROOMS_4,
        self::ROOMS_5,
        self::ROOMS_6,
        self::ROOMS_7,
        self::ROOMS_8,
        self::TOWNHOUSE,
        self::HOME,
        self::ROOMS_20,
        self::ROOMS_21,
        self::ROOMS_DOLYA,
    );

}
