<?php
/**
 * Date: 12/16/14
 * Time: 6:40 PM
 * @author namax
 */

namespace Consts\Table\Arenda;


class Category
{


    const KVARTIRY = 1;
    const KOMNATY = 2;

    static public $allParams = [
        self::KVARTIRY,
        self::KOMNATY,
    ];


    static public $names = [
        self::KVARTIRY => 'Квартиры',
        self::KOMNATY => 'Комнаты',
    ];
}