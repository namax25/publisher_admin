<?php

namespace Consts\Table\Users;

/**
 * Description of Status
 *
 * @author namax
 */
class Status
{

    const NEWONE = 0;
    const ACTIVE = 1;
    const NOT_ACTIVE = 2;
    const BLOCKED = 254;
    const DELETED = 255;

    static $allParams = array(
        self::DO_NOT_REMIND,
        self::ACTIVE,
        self::NOT_ACTIVE,
        self::BLOCKED,
        self::DELETED,
    );

}
