<?php

namespace Consts\Table\UserPhones;

/**
 * Description of Status
 *
 * @author namax
 */
class Status
{

    const NEWONE = 0;
    const SUCCESS = 1;
    const ACTIVE = 2;
    const WAIT_CONFIRMATION_CODE = 3;
    const USED = 4;
    const ERROR = 253;
    const BLOCKED = 254;
    const DELETED = 255;

    static $allParams = array(
        self::NEWONE,
        self::SUCCESS,
        self::ACTIVE,
        self::USED,
        self::ERROR,
        self::WAIT_CONFIRMATION_CODE,
        self::BLOCKED,
        self::DELETED,
    );

    static $paramsNames = [
        self::NEWONE => "новый",
        self::SUCCESS => "опубликовано",
        self::ACTIVE => "активный",
        self::USED => "использован",
        self::ERROR => "ошибка",
        self::WAIT_CONFIRMATION_CODE => "ожидает смс код",
        self::BLOCKED => "заблокирован",
        self::DELETED => "удален",
    ];

    static $classLabel = [
        self::NEWONE => "label-info",
        self::SUCCESS => "label-success",
        self::ACTIVE => "label-active",
        self::USED => "label-default",
        self::ERROR => "label-danger",
        self::WAIT_CONFIRMATION_CODE => "label-warning",
        self::BLOCKED => "label-default",
        self::DELETED => "label-default",
    ];

}
