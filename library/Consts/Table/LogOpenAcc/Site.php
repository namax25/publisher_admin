<?php
/**
 * Date: 2/12/15
 * Time: 4:27 PM
 * @author namax
 */

namespace Consts\Table\LogOpenAcc;


class Site
{
    const AVITO = 1;
    const YANDEX = 2;
    const IRR = 3;

    static public $allParams = array(
        self::AVITO,
        self::YANDEX,
        self::IRR,
    );
}


