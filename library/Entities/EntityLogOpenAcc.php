<?php
/**
 * Date: 2/12/15
 * Time: 4:30 PM
 * @author namax
 */

namespace Entities;


class EntityLogOpenAcc extends \Entities\AbstractEntity
{

    protected $id = null;
    protected $userId = null;
    protected $site = null;
    protected $login = null;
    protected $password = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['user_id'])) {
            $this->setUserId($data['user_id']);
        }
        if (isset($data['site'])) {
            $this->setSite($data['site']);
        }
        if (isset($data['login'])) {
            $this->setLogin($data['login']);
        }
        if (isset($data['password'])) {
            $this->setPassword($data['password']);
        }
    }

    public function clear()
    {
        $this->id = null;
        $this->userId = null;
        $this->site = null;
        $this->login = null;
        $this->password = null;
        return $this;
    }

    public function toArray()
    {
        $data = [];
        $data['id'] = $this->getId();
        $data['user_id'] = $this->getUserId();
        $data['site'] = $this->getSite();
        $data['login'] = $this->getLogin();
        $data['password'] = $this->getPassword();
        return $data;
    }

    public function toDb()
    {
        return $this->toArrayNoNull();
    }

    public function fromDb($data)
    {
        $this->fromArray($data);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $id = \Helpers\Strings::create()->clearId($id);

        if ($id) {
            $this->id = $id;
        }
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $userId = \Helpers\Strings::create()->clearId($userId);

        if ($userId) {
            $this->userId = $userId;
        }

        return $this;
    }

    public function getSite()
    {
        return $this->site;
    }

    public function setSite($site)
    {
        $site = (int)$site;
        if (in_array($site, \Consts\Table\LogOpenAcc\Site::$allParams)) {
            $this->site = $site;
        }
        return $this;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = \Helpers\Strings::create()->clear($login);
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = \Helpers\Strings::create()->clear($password);
        return $this;
    }


}
