<?php
/**
 * Date: 11/25/14
 * Time: 05:04 PM
 * @author namax
 */

namespace Entities;

use \Zend\Json\Json;

/**
 * Class BillingType
 * @package Entities
 *
 * id
 * name
 * debit_or_credit
 * price_params
 *
 *
 * id
 * name
 * debitOrCredit
 * priceParams
 */
class BillingType extends AbstractEntity
{

    protected $id = null;
    protected $name = null;
    protected $debitOrCredit = null;
    protected $priceParams = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['name'])) {
            $this->setName($data['name']);
        }
        if (isset($data['debit_or_credit'])) {
            $this->setDebitOrCredit($data['debit_or_credit']);
        }
        if (isset($data['price_params'])) {
            $this->setPriceParams($data['price_params']);
        }
    }

    public function toArray()
    {
        $data = [];

        $data['id'] = $this->getId();
        $data['name'] = $this->getName();
        $data['debit_or_credit'] = $this->getDebitOrCredit();
        $data['price_params'] = $this->getPriceParams();

        return $data;
    }

    public function toDb()
    {
        $data = $this->toArrayNoNull();

        if (isset($data['price_params'])) {
            $data['price_params'] = Json::encode($data['price_params']);
        }

        return $data;
    }

    public function fromDb($data)
    {
        if (isset($data['price_params'])) {
            $data['price_params'] = Json::decode($data['price_params'], Json::TYPE_ARRAY);
        }
        $this->fromArray($data);
        return $this;
    }

    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->debit_or_credit = null;
        $this->price_params = null;

        return $this;
    }

    public function getPriceByTariff($tariffId)
    {

        $priceParams = $this->getPriceParams();
        if (is_null($priceParams)) {
            throw new \Exception(__METHOD__ . " No prices were found ");
        }

        if (!isset($priceParams[$tariffId])) {
            throw new \Exception(__METHOD__ . " Not set price for tariff id " . $tariffId);
        }

        return $priceParams[$tariffId];
    }

    /**
     * @return null
     */
    public function getDebitOrCredit()
    {
        return $this->debitOrCredit;
    }

    /**
     * @param null $debitOrCredit
     */
    public function setDebitOrCredit($debitOrCredit)
    {
        $this->debitOrCredit = $debitOrCredit;
        return $this;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null
     */
    public function getPriceParams()
    {
        return $this->priceParams;
    }

    /**
     * @param null $priceParams
     */
    public function setPriceParams($priceParams)
    {
        $this->priceParams = $priceParams;
        return $this;
    }


}