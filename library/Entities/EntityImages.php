<?php
/**
 * Date: 2/22/15
 * Time: 7:39 AM
 * @author namax
 */

namespace Entities;

class EntityImages extends \Entities\AbstractEntity
{
    protected $id = null;
    protected $adId = null;
    protected $imageId = null;
    protected $urlHash = null;
    protected $order = null;
    protected $dateLastUpdate = null;
    protected $countUploads = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['ad_id'])) {
            $this->setAdId($data['ad_id']);
        }
        if (isset($data['image_id'])) {
            $this->setImageId($data['image_id']);
        }
        if (isset($data['url_hash'])) {
            $this->setUrlHash($data['url_hash']);
        }
        if (isset($data['order'])) {
            $this->setOrder($data['order']);
        }
        if (isset($data['date_last_update'])) {
            $this->setDateLastUpdate(\Helpers\DateTime::create()->getDateTimeObject($data['date_last_update']));
        }
        if (isset($data['count_uploads'])) {
            $this->setCountUploads($data['count_uploads']);
        }
    }

    public function clear()
    {
        $this->id = null;
        $this->adId = null;
        $this->imageId = null;
        $this->urlHash = null;
        $this->order = null;
        $this->dateLastUpdate = null;
        $this->countUploads = null;
        return $this;
    }

    public function toArray()
    {
        $data = [];
        $data['id'] = $this->getId();
        $data['ad_id'] = $this->getAdId();
        $data['image_id'] = $this->getImageId();
        $data['url_hash'] = $this->getUrlHash();
        $data['order'] = $this->getOrder();
        $data['date_last_update'] = $this->getDateLastUpdate();
        $data['count_uploads'] = $this->getCountUploads();
        return $data;
    }

    public function toDb()
    {
        $data = $this->toArrayNoNull();

        if (isset($data['date_last_update']) && $data['date_last_update'] instanceof \DateTime) {
            $data['date_last_update'] = \Helpers\DateTime::create()->printDate($data['date_last_update']);
        }

        return $data;
    }

    public function fromDb($data)
    {
        $this->fromArray($data);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $id = \Helpers\Strings::create()->clearId($id);

        if ($id) {
            $this->id = $id;
        }
        return $this;
    }

    public function getAdId()
    {
        return $this->adId;
    }

    public function setAdId($adId)
    {
        $this->adId = $adId;
        return $this;
    }

    public function getImageId()
    {
        return $this->imageId;
    }

    public function setImageId($imageId)
    {

        $imageId = \Helpers\Strings::create()->clearMd5Hash($imageId);

        if ($imageId) {
            $this->imageId = $imageId;
        }
        return $this;
    }

    public function getUrlHash()
    {
        return $this->urlHash;
    }

    public function setUrlHash($urlHash)
    {
        $urlHash = \Helpers\Strings::create()->clearMd5Hash($urlHash);

        if ($urlHash) {
            $this->urlHash = $urlHash;
        }
        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $order = \Helpers\Strings::create()->clearDigits($order);

        if ($order) {
            $this->order = $order;
        }
        return $this;

    }

    public function getDateLastUpdate()
    {
        return $this->dateLastUpdate;
    }

    public function setDateLastUpdate(\DateTime $dateLastUpdate)
    {
        $this->dateLastUpdate = $dateLastUpdate;
        return $this;
    }


    public function getCountUploads()
    {
        return $this->countUploads;
    }

    public function setCountUploads($countUploads)
    {
        $countUploads = \Helpers\Strings::create()->clearDigits($countUploads);

        if ($countUploads) {
            $this->countUploads = $countUploads;
        }
        return $this;
    }
}
