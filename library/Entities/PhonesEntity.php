<?php

namespace Entities;

class PhonesEntity extends \Entities\AbstractEntity
{
    protected $id = null;
    protected $userId = null;
    protected $status = null;
    protected $phone = null;
    protected $smsCode = null;
    protected $adPassword = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['user_id'])) {
            $this->setUserId($data['user_id']);
        }
        if (isset($data['status'])) {
            $this->setStatus($data['status']);
        }
        if (isset($data['phone'])) {
            $this->setPhone($data['phone']);
        }
        if (isset($data['sms_code'])) {
            $this->setSmsCode($data['sms_code']);
        }
        if (isset($data['ad_password'])) {
            $this->setAdPassword($data['ad_password']);
        }

    }


    public function isEmptyPhone()
    {
        return empty($this->getPhone());
    }

    public function clear()
    {
        $this->id = null;
        $this->userId = null;
        $this->status = null;
        $this->phone = null;
        $this->smsCode = null;
        $this->adPassword = null;
        return $this;
    }

    public function toArray()
    {
        $data = [];
        $data['id'] = $this->getId();
        $data['user_id'] = $this->getUserId();
        $data['status'] = $this->getStatus();
        $data['phone'] = $this->getPhone();
        $data['sms_code'] = $this->getSmsCode();
        $data['ad_password'] = $this->getAdPassword();
        return $data;
    }

    public function toDb()
    {
        $data = $this->toArrayNoNull();
        unset($data['id']);
        return $data;
    }

    public function fromDb($data)
    {
        $this->fromArray($data);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null
     */
    public function getSmsCode()
    {
        return $this->smsCode;
    }

    /**
     * @param null $smsCode
     */
    public function setSmsCode($sms_code)
    {
        $sms_code = \Helpers\Strings::create()->clearAlias($sms_code);

        if ($sms_code) {
            $this->smsCode = $sms_code;
        }

        return $this;
    }


    public function setId($id)
    {

        $id = \Helpers\Strings::create()->clearId($id);

        if ($id) {
            $this->id = $id;
        }

        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $userId = \Helpers\Strings::create()->clearDigits($userId);

        if ($userId) {
            $this->userId = $userId;
        }


        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $status - (int)(string)$status;
        if (in_array($status, \Consts\Table\UserPhones\Status::$allParams)) {
            $this->status = $status;
        }

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $phone = \Helpers\Strings::create()->clearDigits($phone);

        if ($phone) {
            $this->phone = $phone;
        }


        return $this;
    }

    /**
     * @return null
     */
    public function getAdPassword()
    {
        return $this->adPassword;
    }

    /**
     * @param null $adPassword
     */
    public function setAdPassword($adPassword)
    {
        $this->adPassword = \Helpers\Strings::create()->clear($adPassword);
        return $this;
    }

}
