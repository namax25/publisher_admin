<?php
/**
 * Date: 2/5/15
 * Time: 5:59 PM
 * @author namax
 */

namespace Entities;


class EntityAdsArenda extends \Entities\AbstractEntity
{
    protected $id = null;
    protected $idOld = null;
    protected $userId = null;
    protected $type = null;
    protected $category = null;
    protected $city = null;
    protected $address = null;
    protected $metro = null;
    protected $rooms = null;
    protected $price = null;
    protected $floor = null;
    protected $floors = null;
    protected $period = null;
    protected $hometype = null;
    protected $area = null;
    protected $createdAt = null;
    protected $publicationDate = null;
    protected $images = null;
    protected $description = null;
    protected $name = null;
    protected $email = null;
    protected $generatedUrlToImages = null;


    const BASE_IMAGE_URL = "/uploads/";

    public function getImagesUrls()
    {
        $result = [];
        if ($this->images) {
            foreach ($this->images as $image) {
                $result[$image] = self::BASE_IMAGE_URL . $image[0] . $image[1] . "/" .
                    $image[2] . $image[3] . "/" . $image . ".jpg";
            }
        }


        return $result;
    }

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['id_old'])) {
            $this->setIdOld($data['id_old']);
        }
        if (isset($data['user_id'])) {
            $this->setUserId($data['user_id']);
        }
        if (isset($data['type'])) {
            $this->setType($data['type']);
        }
        if (isset($data['category'])) {
            $this->setCategory($data['category']);
        }
        if (isset($data['city'])) {
            $this->setCity($data['city']);
        }
        if (isset($data['address'])) {
            $this->setAddress($data['address']);
        }
        if (isset($data['metro'])) {
            $this->setMetro($data['metro']);
        }
        if (isset($data['rooms'])) {
            $this->setRooms($data['rooms']);
        }
        if (isset($data['price'])) {
            $this->setPrice($data['price']);
        }
        if (isset($data['floor'])) {
            $this->setFloor($data['floor']);
        }
        if (isset($data['floors'])) {
            $this->setFloors($data['floors']);
        }
        if (isset($data['period'])) {
            $this->setPeriod($data['period']);
        }
        if (isset($data['hometype'])) {
            $this->setHometype($data['hometype']);
        }
        if (isset($data['area'])) {
            $this->setArea($data['area']);
        }

        if (isset($data['created_at'])) {
            $this->setCreatedAt(\Helpers\DateTime::create()->getDateTimeObject($data['created_at']));
        }

        if (isset($data['publication_date'])) {
            $this->setPublicationDate(\Helpers\DateTime::create()->getDateTimeObject($data['publication_date']));
        }


        if (isset($data['images'])) {
            $this->setImages($data['images']);
        }
        if (isset($data['description'])) {
            $this->setDescription($data['description']);
        }
        if (isset($data['name'])) {
            $this->setName($data['name']);
        }
        if (isset($data['email'])) {
            $this->setEmail($data['email']);
        }
    }

    public function clear()
    {
        $this->id = null;
        $this->idOld = null;
        $this->userId = null;
        $this->type = null;
        $this->category = null;
        $this->city = null;
        $this->address = null;
        $this->metro = null;
        $this->rooms = null;
        $this->price = null;
        $this->floor = null;
        $this->floors = null;
        $this->period = null;
        $this->hometype = null;
        $this->area = null;
        $this->createdAt = null;
        $this->publicationDate = null;
        $this->images = null;
        $this->description = null;
        $this->name = null;
        $this->email = null;
        return $this;
    }

    public function toArray()
    {
        $data = [];
        $data['id'] = $this->getId();
        $data['id_old'] = $this->getIdOld();
        $data['user_id'] = $this->getUserId();
        $data['type'] = $this->getType();
        $data['category'] = $this->getCategory();
        $data['city'] = $this->getCity();
        $data['address'] = $this->getAddress();
        $data['metro'] = $this->getMetro();
        $data['rooms'] = $this->getRooms();
        $data['price'] = $this->getPrice();
        $data['floor'] = $this->getFloor();
        $data['floors'] = $this->getFloors();
        $data['period'] = $this->getPeriod();
        $data['hometype'] = $this->getHometype();
        $data['area'] = $this->getArea();
        $data['created_at'] = $this->getCreatedAt();
        $data['publication_date'] = $this->getPublicationDate();
        $data['images'] = $this->getImages();
        $data['description'] = $this->getDescription();
        $data['name'] = $this->getName();
        $data['email'] = $this->getEmail();
        return $data;
    }

    public function toDb()
    {
        $data = $this->toArrayNoNull();

        if (isset($data['images'])) {
            if (is_array($data['images'])) {
                $data['images'] = \Zend\Json\Json::encode($data['images']);
            } else {
                unset($data['images']);
            }
        }

        if (isset($data['publication_date']) && $data['publication_date'] instanceof \DateTime) {
            $data['publication_date'] = \Helpers\DateTime::create()->printDate($data['publication_date']);
        }

        if (isset($data['created_at']) && $data['created_at'] instanceof \DateTime) {
            $data['created_at'] = \Helpers\DateTime::create()->printDate($data['created_at']);
        }

        return $data;
    }

    public function fromDb($data)
    {
        $this->fromArray($data);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $id = \Helpers\Strings::create()->clearId($id);

        if ($id) {
            $this->id = $id;
        }
        return $this;
    }

    public function getIdOld()
    {
        return $this->idOld;
    }

    public function setIdOld($idOld)
    {
        $idOld = \Helpers\Strings::create()->clearId($idOld);

        if ($idOld) {
            $this->idOld = $idOld;
        }
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $userId = \Helpers\Strings::create()->clearId($userId);

        if ($userId) {
            $this->userId = $userId;
        }

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $type = (int)$type;
        if (in_array($type, \Consts\Table\Arenda\AdType::$allParams)) {
            $this->type = $type;
        }
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $category = (int)$category;
        if (in_array($category, \Consts\Table\Arenda\Category::$allParams)) {
            $this->category = $category;
        }
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $city = (int)$city;
        if (in_array($city, \Consts\Regions::$allParams)) {
            $this->city = $city;
        }
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = \Helpers\Strings::create()->clear($address);
        return $this;
    }

    public function getMetro()
    {
        return $this->metro;
    }

    public function setMetro($metro)
    {
        $this->metro = \Helpers\Strings::create()->clearDigits($metro);
        return $this;
    }

    public function getRooms()
    {
        return $this->rooms;
    }

    public function setRooms($rooms)
    {
        $rooms = (int)$rooms;
        if (in_array($rooms, \Consts\Table\Arenda\Rooms::$allParams)) {
            $this->rooms = $rooms;
        }
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $price = (int)(string)$price;
        if ($price < 0) {
            $price = 0;
        }

        $this->price = $price;
        return $this;
    }

    public function getFloor()
    {
        return $this->floor;
    }

    public function setFloor($floor)
    {
        $floor = (int)(string)$floor;
        if ($floor < 0) {
            $floor = 0;
        }

        $this->floor = $floor;
        return $this;
    }

    public function getFloors()
    {
        return $this->floors;
    }

    public function setFloors($floors)
    {
        $floors = (int)(string)$floors;
        if ($floors < 0) {
            $floors = 0;
        }

        $this->floors = $floors;
        return $this;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function setPeriod($period)
    {
        $period = (int)$period;
        if (in_array($period, \Consts\Table\Arenda\Period::$allParams)) {
            $this->period = $period;
        }
        return $this;
    }

    public function getHometype()
    {
        return $this->hometype;
    }

    public function setHometype($hometype)
    {

        $hometype = (int)$hometype;
        if (in_array($hometype, \Consts\Table\Arenda\Hometype::$allParams)) {
            $this->hometype = $hometype;
        }
        return $this;
    }

    public function getArea()
    {
        return $this->area;
    }

    public function setArea($area)
    {
        $area = (int)(string)$area;
        if ($area < 0) {
            $area = 0;
        }

        $this->area = $area;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTime $publicationDate)
    {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setImages($images)
    {
        $images = \Helpers\Strings::create()->clearMd5Hash($images);

        if ($images) {

            if (!is_array($images)) {
                $images = [$images];
            }

            $this->images = $images;
        }
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = \Helpers\Strings::create()->clear($description);
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = \Helpers\Strings::create()->clear($name);
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = \Helpers\Strings::create()->clear($email);
        return $this;
    }


}
