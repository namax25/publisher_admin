<?php

namespace Entities;

class EntityAdsTasks extends \Entities\AbstractEntity
{
    protected $id = null;
    protected $userId = null;
    protected $phoneId = null;
    protected $adId = null;
    protected $createdAt = null;
    protected $completedAd = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['user_id'])) {
            $this->setUserId($data['user_id']);
        }
        if (isset($data['phone_id'])) {
            $this->setPhoneId($data['phone_id']);
        }
        if (isset($data['ad_id'])) {
            $this->setAdId($data['ad_id']);
        }
        if (isset($data['created_at'])) {
            $this->setCreatedAt(\Helpers\DateTime::create()->getDateTimeObject($data['created_at']));
        }
        if (isset($data['completed_ad'])) {
            $this->setCompletedAd(\Helpers\DateTime::create()->getDateTimeObject($data['completed_ad']));
        }
    }

    public function clear()
    {
        $this->id = null;
        $this->userId = null;
        $this->phoneId = null;
        $this->adId = null;
        $this->createdAt = null;
        $this->completedAd = null;
        return $this;
    }

    public function toArray()
    {
        $data = [];
        $data['id'] = $this->getId();
        $data['user_id'] = $this->getUserId();
        $data['phone_id'] = $this->getPhoneId();
        $data['ad_id'] = $this->getAdId();
        $data['created_at'] = $this->getCreatedAt();
        $data['completed_ad'] = $this->getCompletedAd();
        return $data;
    }

    public function toDb()
    {
        $data = $this->toArrayNoNull();


        if (isset($data['created_at']) && $data['created_at'] instanceof \DateTime) {
            $data['created_at'] = \Helpers\DateTime::create()->printDate($data['created_at']);
        }

        if (isset($data['completed_ad']) && $data['completed_ad'] instanceof \DateTime) {
            $data['completed_ad'] = \Helpers\DateTime::create()->printDate($data['completed_ad']);
        }

        return $data;
    }

    public function fromDb($data)
    {
        $this->fromArray($data);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $id = \Helpers\Strings::create()->clearId($id);

        if ($id) {
            $this->id = $id;
        }
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $userId = \Helpers\Strings::create()->clearId($userId);

        if ($userId) {
            $this->userId = $userId;
        }

        return $this;
    }

    public function getPhoneId()
    {
        return $this->phoneId;
    }

    public function setPhoneId($phoneId)
    {
        $phoneId = \Helpers\Strings::create()->clearId($phoneId);

        if ($phoneId) {
            $this->phoneId = $phoneId;
        }
        return $this;
    }

    public function getAdId()
    {
        return $this->adId;
    }

    public function setAdId($adId)
    {
        $adId = \Helpers\Strings::create()->clearId($adId);

        if ($adId) {
            $this->adId = $adId;
        }
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCompletedAd()
    {
        return $this->completedAd;
    }

    public function setCompletedAd(\DateTime $completedAd)
    {
        $this->completedAd = $completedAd;
        return $this;
    }


}
