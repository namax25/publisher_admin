<?php
/**
 * Date: 11/29/14
 * Time: 10:52 AM
 * @author namax
 */

namespace Datatypes\Redis;


class Key extends AbstractDatatype
{

    public function expire($key, $value, $timeout)
    {
        $this->getRedis()->multi();
        $this->getRedis()->set($key, $value);
        $this->getRedis()->expire($key, $timeout);
        $this->getRedis()->exec();
    }
} 