<?php
/**
 * Date: 12/27/14
 * Time: 5:33 PM
 * @author namax
 */

namespace Datatypes\Redis;


class ListAdsTasks extends AbstractList
{

    const MAX_URLS_IN_LIST = 10000;
    const MIN_URLS_IN_LIST = 5;

    protected $key = 'list_ads_tasks';


    public function  getMaxUrlsInList()
    {
        return self::MAX_URLS_IN_LIST;
    }

    public function  getMinUrlsInList()
    {
        return self::MIN_URLS_IN_LIST;
    }


}