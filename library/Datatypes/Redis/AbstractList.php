<?php
/**
 * Date: 11/14/14
 * Time: 06:52 PM
 * @author namax
 */

namespace Datatypes\Redis;


abstract class AbstractList extends AbstractDatatype
{


    public function count()
    {
        return $this->getRedis()->lLen($this->getKey());
    }

    public function prepend($value)
    {
        return $this->getRedis()->lPush($this->getKey(), $value);
    }

    public function append($value)
    {
        return $this->getRedis()->rPush($this->getKey(), $value);
    }

    public function lPop()
    {
        return $this->getRedis()->lPop($this->getKey());
    }

    public function rPop()
    {
        return $this->getRedis()->rPop($this->getKey());
    }

} 