<?php

/**
 * Date: 2/22/15
 * Time: 3:13 PM
 * @author namax
 */
class Stopwatch
{

    private static $start = 0;

    public static function start()
    {
        self::$start = microtime(true);
    }

    public static function end()
    {
        if (self::$start == 0) {
            throw new \Exception("Timing was not start");
        }
        return round((microtime(true) - self::$start), 4);
    }
}