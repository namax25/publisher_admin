<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Log\Writer;

class Db extends \Zend\Log\Writer\Db
{

    /**
     * Map event into column using the $columnMap array
     *
     * @param  array $event
     * @param  array $columnMap
     * @return array
     */
    protected function mapEventIntoColumn(array $event, array $columnMap = null)
    {
        $data = parent::mapEventIntoColumn($event, $columnMap);
        $data['file'] = $event['extra']['file'];
        $data['line'] = $event['extra']['line'];
        unset($event['extra']['file']);
        unset($event['extra']['line']);
        $data['context'] = json_encode($event['extra']);
        $data['backtrace'] = json_encode(debug_backtrace());
        return $data;
    }

}
