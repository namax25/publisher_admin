<?php

namespace Mail\GatewayProviders;

/**
 * Description of AmazonSes
 *
 * @author namax
 */
class AmazonSes implements \Mail\iMail
{

    protected $mailClient = null;

    /**
     *
     * @var \Aws\Common\Aws
     */
    protected $awsFactory = null;
    protected $awsFactoryParams = [];

    public function __construct(array $awsFactoryParams)
    {
        $this->awsFactoryParams = $awsFactoryParams;
    }

    public function getAwsFactory()
    {
        if (is_null($this->awsFactory)) {
            $this->awsFactory = \Aws\Common\Aws::factory($this->awsFactoryParams);
        }
        return $this->awsFactory;
    }

    public function send(\Mail\MailParams $mailParams)
    {
        return $this->getMailClient()->sendEmail($this->generateAppropriateArgs($mailParams));
    }

    /**
     *
     * @return \Aws\Ses\SesClient
     */
    public function getMailClient()
    {
        if (is_null($this->mailClient)) {
            $this->mailClient = $this->getAwsFactory()->get('Ses');
        }
        return $this->mailClient;
    }

    public function setMailClient(\Aws\Ses\SesClient $mailClient)
    {
        $this->mailClient = $mailClient;
    }

    public function generateAppropriateArgs(\Mail\MailParams $mailParams)
    {


        $result = [];
        $result['Source'] = $mailParams->getFromAddress();
        $result['Destination'] = [];
        $result['Destination']['ToAddresses'] = $mailParams->getToAddresses();

        if ($mailParams->getBccAddresses()) {
            $result['Destination']['BccAddresses'] = $mailParams->getBccAddresses();
        }

        if ($mailParams->getCcAddresses()) {
            $result['Destination']['CcAddresses'] = $mailParams->getCcAddresses();
        }


        $result['Message'] = [];
        $result['Message']['Subject'] = [];
        $result['Message']['Subject']['Data'] = $mailParams->getMessageSubject();
        $result['Message']['Subject']['Charset'] = $mailParams->getMessageSubjectCharset();

        if ($mailParams->getBodyText()) {
            $result['Message']['Body'] = [];
            $result['Message']['Body']['Text']['Data'] = $mailParams->getBodyText();
            $result['Message']['Body']['Text']['Charset'] = $mailParams->getBodyTextCharset();
        }
        if ($mailParams->getBodyHtml()) {
            $result['Message']['Body']['Html']['Data'] = $mailParams->getBodyHtml();
            $result['Message']['Body']['Html']['Charset'] = $mailParams->getBodyHtmlCharset();
        }

        $result['ReplyToAddresses'] = $mailParams->getReplyToAddresses();
        $result['ReturnPath'] = $mailParams->getReturnPath();

        return $result;
    }
}
