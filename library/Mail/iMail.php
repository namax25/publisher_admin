<?php

namespace Mail;

/**
 *
 * @author namax
 */
interface iMail
{

    public function send(\Mail\MailParams $mailParams);
}
