<?php

namespace Mail;

/**
 * Параметры, которые формируют сообщение
 * @author namax
 */
class MailParams
{

    protected $fromAddress = null;
    protected $toAddresses = [];
    protected $ccAddresses = [];
    protected $bccAddresses = [];
    protected $messageSubject = null;
    protected $messageSubjectCharset = 'utf8';
    protected $bodyText = null;
    protected $bodyTextCharset = 'utf8';
    protected $bodyHtml = null;
    protected $bodyHtmlCharset = 'utf8';
    protected $replyToAddresses = [];
    protected $returnPath = null;

    public function getFromAddress()
    {
        if (is_null($this->fromAddress)) {
            throw new \Exception(__METHOD__ . " The from address must be set");
        }
        return $this->fromAddress;
    }

    public function getToAddresses()
    {
        if (!$this->fromAddress) {
            throw new \Exception(__METHOD__ . " The to address must be set");
        }
        return $this->toAddresses;
    }

    public function getCcAddresses()
    {
        return $this->ccAddresses;
    }

    public function getBccAddresses()
    {
        return $this->bccAddresses;
    }

    public function getMessageSubject()
    {
        if (!$this->fromAddress) {
            throw new \Exception(__METHOD__ . " The message subject must be set");
        }
        return $this->messageSubject;
    }

    public function getMessageSubjectCharset()
    {
        return $this->messageSubjectCharset;
    }

    public function getBodyText()
    {
        return $this->bodyText;
    }

    public function getBodyTextCharset()
    {
        return $this->bodyTextCharset;
    }

    public function getBodyHtml()
    {
        return $this->bodyHtml;
    }

    public function getBodyHtmlCharset()
    {
        return $this->bodyHtmlCharset;
    }

    public function getReplyToAddresses()
    {
        return $this->replyToAddresses;
    }

    public function getReturnPath()
    {
        return $this->returnPath;
    }

    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;
        return $this;
    }

    public function setToAddresses(array $toAddresses)
    {
        $this->toAddresses = $toAddresses;
        return $this;
    }

    public function setCcAddresses(array $ccAddresses)
    {
        $this->ccAddresses = $ccAddresses;
        return $this;
    }

    public function setBccAddresses(array $bccAddresses)
    {
        $this->bccAddresses = $bccAddresses;
        return $this;
    }

    public function addToAddresses($toAddresses)
    {
        $this->toAddresses[] = $toAddresses;
    }

    public function addCcAddresses($ccAddresses)
    {
        $this->ccAddresses[] = $ccAddresses;
    }

    public function addBccAddresses($bccAddresses)
    {
        $this->bccAddresses[] = $bccAddresses;
    }

    public function setMessageSubject($messageSubject)
    {
        $this->messageSubject = $messageSubject;
        return $this;
    }

    public function setMessageSubjectCharset($messageSubjectCharset)
    {
        $this->messageSubjectCharset = $messageSubjectCharset;
        return $this;
    }

    public function setBodyText($bodyText)
    {
        $this->bodyText = $bodyText;
        return $this;
    }

    public function setBodyTextCharset($bodyTextCharset)
    {
        $this->bodyTextCharset = $bodyTextCharset;
        return $this;
    }

    public function setBodyHtml($bodyHtml)
    {
        $this->bodyHtml = $bodyHtml;
        return $this;
    }

    public function setBodyHtmlCharset($bodyHtmlCharset)
    {
        $this->bodyHtmlCharset = $bodyHtmlCharset;
        return $this;
    }

    public function setReplyToAddresses($replyToAddresses)
    {
        $this->replyToAddresses = $replyToAddresses;
        return $this;
    }

    public function setReturnPath($returnPath)
    {
        $this->returnPath = $returnPath;
        return $this;
    }
}
