<?php

namespace App\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Result as AuthenticationResult;

/**
 * DbAdapter
 *
 * @author namax
 *
 */
class DbAdapter extends AbstractAdapter implements AdapterInterface
{

    private $userDao = null;

    public function __construct(\Dao\Users $userDao)
    {
        $this->userDao = $userDao;
    }

    public function authenticate()
    {

        $userData = $this->userDao->getByLogin($this->getIdentity());
        $this->sesscontainer = new \Zend\Session\Container('sess_acl');

        $pass = $this->getCredential() . $userData->login . $userData->id;

        if (!empty($userData->password) && password_verify($pass, $userData->password)) {
            $this->sesscontainer->userData = $userData;
            return new AuthenticationResult(1, $userData);
        }
        return new AuthenticationResult(0, $userData, ['ACCESS DENIED']);
    }

    public function generateNewHash($userData, $password)
    {

        $options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $pass = $password . $userData->login . $userData->id;

        return password_hash($pass, PASSWORD_BCRYPT, $options);
    }
}
