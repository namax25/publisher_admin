<?php

namespace App\Auth\Storage;

use Zend\Authentication\Storage;

/**
 * Auth
 *
 * @author namax
 *
 */
class Session extends \Zend\Authentication\Storage\Session
{

    public function setRememberMe($rememberMe = 0, $time = 1209600)
    {
        if ($rememberMe == 1) {
            $this->session->getManager()->rememberMe($time);
        }
    }

    public function forgetMe()
    {
        $this->session->getManager()->forgetMe();
    }

}
 