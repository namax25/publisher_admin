<?php

namespace App\Controllers;

use ArrayIterator;
use Countable;
use IteratorAggregate;

/**
 *   Messenger - implement memory-based messages
 */
class Messenger implements IteratorAggregate, Countable
{

    private $debugMessages = [];
    private $infoMessages = [];
    private $warnMessages = [];
    private $errMessages = [];
    private $successMessages = [];

    /**
     * Messages from previous request
     * @var array
     */
    protected $messages = array();

    /**
     * Whether a message has been added during this request
     *
     * @var bool
     */
    protected $messageAdded = false;

    public function toArray()
    {
        return $this->getAllMessages();
    }

    public function getAllMessages()
    {
        $this->messages = [];
        if ($this->debugMessages) {
            $this->messages['debug'] = $this->debugMessages;
        }
        if ($this->infoMessages) {
            $this->messages['info'] = $this->infoMessages;
        }
        if ($this->warnMessages) {
            $this->messages['warn'] = $this->warnMessages;
        }
        if ($this->errMessages) {
            $this->messages['errors'] = $this->errMessages;
        }
        if ($this->successMessages) {
            $this->messages['success'] = $this->successMessages;
        }
        return $this->messages;
    }

    /**
     * Add a message
     *
     * @param  string $message
     * @return FlashMessenger Provides a fluent interface
     */
    public function addMessages(&$container, $messages)
    {
        if (is_array($messages)) {
            foreach ($messages as $msg) {
                $container[] = (string)$msg;
            }
        } else {
            $container[] = (string)$messages;
        }

        $this->messageAdded = true;

        return $this;
    }

    /**
     * Add a message with "info" type
     *
     * @param  string $message
     * @return FlashMessenger
     */
    public function addInfoMessage($message)
    {
        $this->addMessages($this->infoMessages, $message);
        return $this;
    }

    /**
     * Add a message with "success" type
     *
     * @param  string $message
     * @return FlashMessenger
     */
    public function addSuccessMessage($message)
    {
        $this->addMessages($this->successMessages, $message);
        return $this;
    }

    /**
     * Add a message with "warning" type
     *
     * @param string $message
     * @return FlashMessenger
     */
    public function addWarningMessage($message)
    {
        $this->addMessages($this->warnMessages, $message);

        return $this;
    }

    /**
     * Add a message with "error" type
     *
     * @param  string $message
     * @return FlashMessenger
     */
    public function addErrorMessage($message)
    {
        $this->addMessages($this->errMessages, $message);

        return $this;
    }

    /**
     * Whether a specific namespace has messages
     *
     * @return bool
     */
    public function hasMessages()
    {
        return !empty($this->getAllMessages());
    }

    /**
     * Whether "info" namespace has messages
     *
     * @return bool
     */
    public function hasInfoMessages()
    {
        return !empty($this->getInfoMessages());
    }

    /**
     * Whether "success" namespace has messages
     *
     * @return bool
     */
    public function hasSuccessMessages()
    {
        return !empty($this->getSuccessMessages());
    }

    /**
     * Whether "warning" namespace has messages
     *
     * @return bool
     */
    public function hasWarningMessages()
    {
        return !empty($this->getWarningMessages());
    }

    /**
     * Whether "error" namespace has messages
     *
     * @return bool
     */
    public function hasErrorMessages()
    {
        return !empty($this->getErrorMessages());
    }

    /**
     * Get messages from "info" namespace
     *
     * @return array
     */
    public function getInfoMessages()
    {
        return $this->infoMessages;
    }

    /**
     * Get messages from "success" namespace
     *
     * @return array
     */
    public function getSuccessMessages()
    {
        return $this->successMessages;
    }

    /**
     * Get messages from "warning" namespace
     *
     * @return array
     */
    public function getWarningMessages()
    {
        return $this->warnMessages;
    }

    /**
     * Get messages from "error" namespace
     *
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errMessages;
    }

    public function clearAllMessages()
    {
        $this->debugMessages = [];
        $this->infoMessages = [];
        $this->warnMessages = [];
        $this->errMessages = [];
        $this->successMessages = [];
        $this->messages = [];
        $this->messageAdded = false;
    }

    /**
     * Complete the IteratorAggregate interface, for iterating
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        if ($this->hasMessages()) {
            return new ArrayIterator($this->toArray());
        }

        return new ArrayIterator();
    }

    /**
     * Complete the countable interface
     *
     * @return int
     */
    public function count()
    {
        if ($this->hasMessages()) {
            return count($this->getErrorMessages()) + count($this->getWarningMessages()) + count($this->getInfoMessages()) + count($this->getSuccessMessages());
        }

        return 0;
    }

}
