<?php

namespace Jobs;

use \Consts\Table\CalendarEvents\HowOftenNotify;

/**
 * Description of EventNotifier
 *
 * @author namax
 */
class EventNotifier extends AbstractJob
{

    const STEP = 100;

    /**
     *
     * @var \Dao\CalendarEvents
     */
    protected $daoCalendarEvents = null;

    /**
     *
     * @var \Dao\Notifications
     */
    protected $daoNotifications = null;

    /**
     *
     * @var \Dao\Users
     */
    protected $daoUsers = null;

    /**
     *
     * @var \Mail\iMail
     */
    protected $mailService = null;

    /**
     *
     * @var \Sms\iSms
     */
    protected $smsService = null;

    protected function configure()
    {

        $this->daoCalendarEvents = new \Dao\CalendarEvents($this->getDb());
        $this->daoNotifications = new \Dao\Notifications($this->getDb());
        $this->daoUsers = new \Dao\Users($this->getDb());
    }

    protected function doAll()
    {

        $offset = 0;
        $amount = 200;

        while ($events = $this->getEvents($amount, $offset)) {

            foreach ($events as $event) {
                $this->getLogCli()->info("Processing event: " . $event->id);

                $this->updateDateForNextNotification($event);
            }
            $offset += $amount;
        }
    }

    /**
     * Получаем новую порцию событий
     * @return boolean
     */
    protected function getEvents($amount, $offset)
    {
        $events = $this->daoCalendarEvents->getEventForNotifyInHour($amount, $offset);
        if (!$events->count()) {
            $this->getLogCli()->notice("No events were found ");
            return false;
        }
        return $events;
    }

    protected function updateDateForNextNotification($event)
    {

        if (empty($event->notify_date) ||
            (empty($event->how_often_notify) && $event->how_often_notify == HowOftenNotify::DO_NOT_REMIND)
        ) {
            return false;
        }

        $intervalStr = $this->getInterval($event->how_often_notify);
        if ($intervalStr) {
            if ($event->how_often_notify != HowOftenNotify::ONSE) {
                $date = \Helpers\DateTime::create()->getDateTimeObject();
                $date->add(new \DateInterval($intervalStr));
                $this->daoCalendarEvents->update(
                    ['notify_date' => \Helpers\DateTime::create()->printDate($date)],
                    ['id' => $event->id]
                );
            } else {
                $this->daoCalendarEvents->update(['notify_date' => null], ['id' => $event->id]);
            }
            $this->daoNotifications->insertOnDuplicate($event->user_id, $event->id);

            $userData = $this->daoUsers->getById($event->user_id);

            if ($event->by_email) {
                $this->sendMailNotification($userData, $event);
            }

            if ($event->by_sms) {
                $this->sendSmsNotification($userData, $event);
            }

            $this->getLogCli()->log(\Log\Logger::OK, "Event set as new");
        }
        return true;
    }

    protected function getInterval($how_often_notify)
    {
        $interval = null;
        switch ($how_often_notify) {
            case HowOftenNotify::ONSE:
                $interval = "PT0S";
                break;
            case HowOftenNotify::DAYLY:
                $interval = "P1D";
                break;
            case HowOftenNotify::WEEKLY:
                $interval = "P7D";
                break;
            case HowOftenNotify::MONTHLY:
                $interval = "P1M";
                break;
            case HowOftenNotify::EVERY_3_MONTH:
                $interval = "P3M";
                break;
            case HowOftenNotify::EVERY_6_MONTH:
                $interval = "P6M";
                break;
            case HowOftenNotify::YEARLY:
                $interval = "P1Y";
                break;
        }
        return $interval;
    }

    /**
     *
     * @return \Sms\iSms
     */
    protected function getSmsSevice()
    {
        if (is_null($this->smsService)) {
            /*    $this->smsService = new \Sms\GatewayProviders\Twilio(
                    $this->getGlobalConfigs()->sms_twilio->account_id,
                    $this->getGlobalConfigs()->sms_twilio->auth_tokem
                );*/

            $this->smsService = new \Sms\GatewayProviders\Plivo(
                $this->getGlobalConfigs()->sms_plivo->account_id,
                $this->getGlobalConfigs()->sms_plivo->auth_tokem
            );
        }

        return $this->smsService;
    }

    protected function sendSmsNotification($userData, $event)
    {
        if (empty($userData->login)) {
            return false;
        }

        $startDate = new \DateTime($event->startDate);
        if (!empty($userData->timezone)) {
            $startDate->setTimezone(new \DateTimeZone($userData->timezone));
        }
        $startDateStr = $startDate->format("H:i, j/m");
        $this->getLogCli()->log(\Log\Logger::OK, "Send sms");

        return $this->getSmsSevice()->send(
            "+12032901389",
            "+" . $userData->login,
            "Напоминание: " . $startDateStr . " " . $event->title
        );
    }

    protected function getMailSevice()
    {

        if (is_null($this->mailService)) {
            $this->mailService = new \Mail\GatewayProviders\AmazonSes(array(
                'key' => $this->getGlobalConfigs()->amazon_ses->aws_access_key_id,
                'secret' => $this->getGlobalConfigs()->amazon_ses->aws_secret_access_key,
                'aws_access_key_id' => $this->getGlobalConfigs()->amazon_ses->aws_access_key_id,
                'aws_secret_access_key' => $this->getGlobalConfigs()->amazon_ses->aws_secret_access_key,
                'region' => $this->getGlobalConfigs()->amazon_ses->region,
            ));
        }

        return $this->mailService;
    }

    protected function sendMailNotification($userData, $event)
    {

        if (empty($userData->email)) {
            return false;
        }


        $startDate = new \DateTime($event->startDate);
        if (!empty($userData->timezone)) {
            $startDate->setTimezone(new \DateTimeZone($userData->timezone));
        }
        $startDateStr = $startDate->format("H:i, d F");

        $mailParams = new \Mail\MailParams();
        $mailParams
            ->setFromAddress("reminder@citoopus.com")
            ->setToAddresses([$userData->email])
            ->setMessageSubject("Напоминание о событии")
            ->setBodyHtml("<strong>" . $startDateStr . "</strong> " . $event->title)
            ->setReplyToAddresses(['twh11mail@gmail.com'])
            ->setReturnPath('twh11mail@gmail.com');

        $this->getLogCli()->log(\Log\Logger::OK, "Sent email");

        return $this->getMailSevice()->send($mailParams);
    }
}
