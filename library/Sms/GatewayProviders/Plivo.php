<?php

namespace Sms\GatewayProviders;

/**
 * Description of Plivo
 *
 * @author namax
 */
class Plivo implements \Sms\iSms
{

    private $accountId = null;
    private $authToken = null;

    /**
     *
     * @var \Net\Curl
     */
    private $curl = null;
    private $debug = true;
    private $client = true;

    const URL = "https://api.plivo.com";
    const VERSION = "v1";

    public function __construct($accountId, $authToken)
    {
        $this->accountId = $accountId;
        $this->authToken = $authToken;
        $this->client = new \RestAPI($accountId, $authToken);
    }

    public function send($fromPhoneNumber, $toPhoneNumber, $text)
    {

        $params = array(
            'src' => $fromPhoneNumber,
            'dst' => $toPhoneNumber,
            'text' => $text,
            'type' => 'sms',
        );
        return $this->client->send_message($params);


//        $postFields = [];
//        $postFields['src'] = $fromPhoneNumber;
//        $postFields['dst'] = $toPhoneNumber;
//        $postFields['text'] = $text;
//        $postFields['type'] = 'sms';
//        echo $this->generateUrlForSendAMessage() . PHP_EOL;
//        echo strlen(json_encode($postFields)) . PHP_EOL;
//
//        $ch = curl_init();
//        $cookiePath = ROOT_PATH .
//            DIRECTORY_SEPARATOR . 'tmp' .
//            DIRECTORY_SEPARATOR . 'cookies' .
//            DIRECTORY_SEPARATOR . 'plivo';
//        $options = [];
////        $options[CURLOPT_URL] = 'localhost:85';
//        $options[CURLOPT_URL] = trim($this->generateUrlForSendAMessage());
//        $options[CURLOPT_HEADER] = false;
//        $options[CURLOPT_FOLLOWLOCATION] = true;
//        $options[CURLOPT_POST] = true;
//        $options[CURLOPT_POSTFIELDS] = json_encode($postFields);
//        $options[CURLOPT_RETURNTRANSFER] = true;
//        $options[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
//
//        $options[CURLOPT_USERPWD] = $this->accountId . ":" . $this->authToken;
//        $options[CURLOPT_SSL_VERIFYPEER] = false;
//        $options[CURLOPT_SSL_VERIFYHOST] = 2;
//        $options[CURLOPT_USERAGENT] = 'PHPPlivo';
//        $options[CURLOPT_TIMEOUT] = 30;
//        $options[CURLOPT_CONNECTTIMEOUT] = 10;
//        $options[CURLOPT_COOKIEFILE] = $cookiePath . DIRECTORY_SEPARATOR . '1.txt';
//        $options[CURLOPT_COOKIEJAR] = $cookiePath . DIRECTORY_SEPARATOR . '1.txt';
//        $options[CURLOPT_HTTPHEADER] = [
//            'Content-Type: application/json',
//            'Connection: close',
//            'Content-Length: ' . strlen(json_encode($postFields))
//        ];
//        curl_setopt_array($ch, $options);
////        var_dump($ch);
//        $response = curl_exec($ch);
//        $error = curl_error($ch);
//        var_dump($response, $error);
//        $info = curl_getinfo($ch);
//
//        curl_close($ch);
//        $this->getCurl()->post('localhost:8085', $postFields, function($info, $output, $error = null) {
//            return $this->responseHandlerCallback($info, $output, $error);
//        });
//        $this->getCurl()->post($this->generateUrlForSendAMessage(), $postFields, function($info, $output, $error = null) {
//            return $this->responseHandlerCallback($info, $output, $error);
//        });
    }

    protected function generateUrlForSendAMessage()
    {

        return self::URL . "/" . self::VERSION . "/Account/" . $this->accountId . '/Message';
    }

    public function getDebug()
    {
        return $this->debug;
    }

    public function setDebug($debug)
    {
        $this->debug = (bool)$debug;
    }
}
