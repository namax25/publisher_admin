<?php

namespace Sms\GatewayProviders;

/**
 * Description of Twilio
 *
 * @author namax
 */
class Twilio implements \Sms\iSms
{

    /**
     *
     * @var Services_Twilio
     */
    protected $client = null;

    public function __construct($accountId, $authToken)
    {
        $this->client = new \Services_Twilio($accountId, $authToken);
    }

    public function send($fromPhoneNumber, $toPhoneNumber, $text)
    {
        return $this->client->account->messages->sendMessage($fromPhoneNumber, $toPhoneNumber, $text);
    }
}
