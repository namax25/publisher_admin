<?php

namespace Dao;

/**
 * Description of LocalDaoFabrica
 *
 * @author namax
 */
class DaoFactory implements \Zend\ServiceManager\FactoryInterface
{

    /**
     *
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $db = null;

    /**
     * DAO
     * @var type
     */
    protected $daoUsers = null;
    protected $daoUserPhones = null;
    protected $daoNotifications = null;
    protected $daoArenda = null;
    protected $daoAdsTasks = null;
    protected $daoProxy = null;
    protected $daoLogOpenAcc = null;
    protected $daoImages = null;
    protected $daoBillingHistory = null;
    protected $daoBillingType = null;


    /**
     *
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator = null;

    public function __construct()
    {

    }

    protected function getDaoClass(&$variable, $className, $newDao = false)
    {
        if (is_null($variable)) {
            $variable = new $className($this->getDb());
        }
        return $newDao ? new $className($this->getDb()) : $variable;
    }

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->setDb($this->serviceLocator->get('db'));
        return $this;
    }

    /**
     *
     * @return \Zend\Db\Adapter\Adapter
     */
    public function getDb()
    {
        return $this->db;
    }

    public function setDb(\Zend\Db\Adapter\Adapter $db)
    {
        $this->db = $db;
        return $this;
    }


    /**
     * @param bool $newDao
     * @return \Dao\Arenda
     */
    public function getDaoArenda($newDao = false)
    {
        return $this->getDaoClass($this->daoArenda, '\Dao\Arenda', $newDao);
    }


    /**
     * @param bool $newDao
     * @return \Dao\Metro
     */
    public function getDaoMetro($newDao = false)
    {
        return $this->getDaoClass($this->daoMetro, '\Dao\Metro', $newDao);
    }

    /**
     * @param bool $newDao
     * @return \Dao\BillingHistory
     */
    public function getDaoBillingHistory($newDao = false)
    {
        return $this->getDaoClass($this->daoBillingHistory, '\Dao\BillingHistory', $newDao);
    }

    /**
     * @param bool $newDao
     * @return \Dao\BillingType
     */
    public function getDaoBillingType($newDao = false)
    {
        return $this->getDaoClass($this->daoBillingType, '\Dao\BillingType', $newDao);
    }

    /**
     * @param bool $newDao
     * @return \Dao\Images
     */
    public function getDaoImages($newDao = false)
    {
        return $this->getDaoClass($this->daoImages, '\Dao\Images', $newDao);
    }


    /**
     * @param bool $newDao
     * @return \Dao\AdsTasks
     */
    public function getDaoAdsTasks($newDao = false)
    {
        return $this->getDaoClass($this->daoAdsTasks, '\Dao\AdsTasks', $newDao);
    }

    /**
     * @param bool $newDao
     * @return \Dao\Proxy
     */
    public function getDaoProxy($newDao = false)
    {
        return $this->getDaoClass($this->daoProxy, '\Dao\Proxy', $newDao);
    }

    /**
     * @param bool $newDao
     * @return \Dao\LogOpenAcc
     */
    public function getDaoLogOpenAcc($newDao = false)
    {
        return $this->getDaoClass($this->daoLogOpenAcc, '\Dao\LogOpenAcc', $newDao);
    }


    /**
     *
     * @param boll $newDao
     * @return \Dao\Users
     */
    public function getDaoUsers($newDao = false)
    {
        if ($this->daoUsers === null) {
            $this->daoUsers = new \Dao\Users($this->getDb());
            return $this->daoUsers;
        }
        return $newDao ? new \Dao\Users($this->getDb()) : $this->daoUsers;
    }


    public function getDaoUserPhones($newDao = false)
    {
        if ($this->daoUserPhones === null) {
            $this->daoUserPhones = new \Dao\UserPhones($this->getDb());
            return $this->daoUserPhones;
        }
        return $newDao ? new \Dao\UserPhones($this->getDb()) : $this->daoUserPhones;
    }

    /**
     *
     * @param boll $newDao
     * @return \Dao\Notifications
     */
    public function getDaoNotifications($newDao = false)
    {
        if ($this->daoNotifications === null) {
            $this->daoNotifications = new \Dao\Notifications($this->getDb());
            return $this->daoNotifications;
        }
        return $newDao ? new \Dao\Notifications($this->getDb()) : $this->daoNotifications;
    }


}
