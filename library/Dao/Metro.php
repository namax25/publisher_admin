<?php
/**
 * Date: 12/16/14
 * Time: 7:14 PM
 * @author namax
 */

namespace Dao;


class Metro extends \Dao\AbstractDao
{

    protected $table = "metro";


    public function getAllSortedByName()
    {

        return $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) {
                $select->order("name asc");
            }
        );

    }
}