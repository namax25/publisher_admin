<?php

namespace Dao;

/**
 * Description of AbstractDao
 *
 * @author namax
 */
class AbstractDao
{

    /**
     * Db adapter
     * @var \Zend\Db\Adapter\Adapter
     */
    public $db = null;
    protected $table = null;
    protected $tableGateway = null;

    public function __construct(\Zend\Db\Adapter\Adapter $db)
    {
        $this->db = $db;
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway($this->table, $db);
    }

    public function getById($id)
    {
        $id = \Helpers\Strings::create()->clearDigits($id);
        $result = $this->getDb()->query('SELECT * FROM ' . $this->table . ' WHERE `id` = ? ', [$id]);
        return $result->current();
    }

    public function getAll()
    {
        return $this->getTableGateway()->select();
    }

    /**
     *
     * @param type $amount
     * @param type $offset
     * @return \Zend\Db\ResultSet\ResultSet;
     */
    public function getByRange($amount = 0, $offset = 0)
    {

        $offset = (int)(string)$offset;
        $amount = (int)(string)$amount;

        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($amount, $offset) {
            $select->offset($offset)->limit($amount);
//            echo $select->getSqlString();
        });

        return $result;
    }

    public function getOne($order = 'id DESC')
    {
        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($order) {
            $select->order($order)->limit(1);
        });
        return $result->current();
    }

    public function insert($data)
    {
        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->getLastInsertValue();
        }
        return false;
    }

    public function update($data, $where)
    {
        return $this->tableGateway->update($data, $where);
    }

    public function delete($where)
    {
        return $this->tableGateway->delete($where);
    }

    /**
     *
     * @return \Zend\Db\Adapter\Adapter
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     *
     * @return  \Zend\Db\TableGateway\AbstractTableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    public function setDb(\Zend\Db\Adapter\Adapter $db)
    {
        $this->db = $db;
    }

    public function setTableGateway($tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

}
