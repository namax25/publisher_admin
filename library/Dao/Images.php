<?php

namespace Dao;

/**
 * Параметры
 */
class Images extends \Dao\AbstractDao
{

    protected $table = "images";


    public function saveNewImage($imageId)
    {


    }

    public function updateImageDate($hash)
    {
        $hash = \Helpers\Strings::create()->clearMd5Hash($hash);
        return $this->update(
            ["date_last_update" => \Helpers\DateTime::create()->now()],
            function (\Zend\Db\Sql\Where $where) use ($hash) {
                $where->
                equalTo('image_id', $hash)->
                isNull('ad_id');
            }
        );
    }


    public function getByHash($hash)
    {
        $hash = \Helpers\Strings::create()->clearMd5Hash($hash);
        $result = $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) use ($hash) {
                $select->where->
                equalTo('image_id', $hash);
                $select->limit(1);
            }
        );

        return $result->current();
    }

    public function getAllImagesByAdId($adId)
    {
        $adId = \Helpers\Strings::create()->clearDigits($adId);

        return $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) use ($adId) {
                $select->where->
                equalTo('ad_id', $adId);
            }
        );

    }

    public function unbindAllImagesFromAd($adId)
    {
        return $this->update(
            ["ad_id" => null],
            function (\Zend\Db\Sql\Where $where) use ($adId) {
                $where->
                equalTo('ad_id', $adId);
            }
        );

    }


    public function unbindImagesFromAd($adId, array $hashes)
    {
        $hashes = \Helpers\Strings::create()->clearMd5Hash($hashes);
        if (!$hashes) {
            return false;
        }

        return $this->update(
            ["ad_id" => null],
            function (\Zend\Db\Sql\Where $where) use ($hashes, $adId) {
                $where->
                equalTo('ad_id', $adId)->
                notIn('image_id', $hashes);
            }
        );

    }


}