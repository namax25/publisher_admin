<?php
/**
 * Date: 12/16/14
 * Time: 7:14 PM
 * @author namax
 */

namespace Dao;


class Arenda extends \Dao\AbstractDao
{

    protected $table = "arenda";


    public function getByIdForUser($userId, $id)
    {

        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($userId, $id) {
            $select->where->
            equalTo('user_id', $userId)->
            equalTo('id', $id);
//            echo $select->getSqlString();
        });

        return $result->current();
    }

}
