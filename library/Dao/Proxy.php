<?php
/**
 * Date: 1/22/15
 * Time: 5:21 PM
 * @author namax
 */

namespace Dao;

class Proxy extends \Dao\AbstractDao implements \Proxy\iProxyTable
{

    protected $table = "proxy";

    /**
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getAllActiveProxy()
    {
        return $this->getTableGateway()->select(
            function (Select $select) {
                $select->where(['active' => 1]);
                $select->order('last_used ASC');
            }
        );
    }

    /**
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getOneActiveProxy($order = 'last_used ASC')
    {
        $result = $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) use ($order) {
                $select->where(['active' => 1]);
                $select->order($order)->limit(1);
            }
        );
        return $result->current();
    }

    public function getOneProxyByDate($order = 'last_used ASC')
    {
        $result = $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) use ($order) {
                $select->where->lessThan('last_used', \Helpers\DateTime::create()->now())->where(['active' => 1]);
                $select->order($order)->limit(1);
                echo $select;
            }
        );
        return $result->current();
    }


    public function updateProxyUsageDate($id, \DateTime $usageDate)
    {

        $id = \Helpers\Strings::create()->clearId($id);

        if (!$id) {
            return false;
        }

        return $this->update(
            ["last_used" => \Helpers\DateTime::create()->printDate($usageDate)],
            function (\Zend\Db\Sql\Where $where) use ($id) {
                $where->
                equalTo('id', $id);
            }
        );

    }
}