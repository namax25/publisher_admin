<?php

namespace Dao;

/**
 *
 * @author namax
 */
class UserPhones extends \Dao\AbstractDao
{

    protected $table = "user_phones";


    public function getByIdForUser($userId, $id)
    {

        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($userId, $id) {
            $select->where->
            equalTo('user_id', $userId)->
            equalTo('id', $id);
//            echo $select->getSqlString();
        });

        return $result->current();
    }


}
