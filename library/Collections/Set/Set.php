<?php

namespace Collections\Set;

/**
 * Description of AbstractSet
 *
 * @author namax
 */
class Set implements \Collections\CollectionsInterface, \IteratorAggregate
{

    protected $set = [];

    public function add($val)
    {
        if (!in_array($val, $this->set)) {
            $this->set[] = $val;
            return true;
        }
        return false;
    }

    public function clear()
    {
        $this->set = [];
    }

    public function contains($val)
    {
        return in_array($val, $this->set);
    }

    public function hashCode()
    {
        return spl_object_hash($this);
    }

    public function isEmpty()
    {
        return $this->set ? false : true;
    }

    public function remove($val)
    {
        $key = array_search($val, $this->set);
        if ($key === false) {
            return false;
        }
        unset($this->set[$key]);
        return true;
    }

    public function removeAll($collection)
    {

    }

    public function retainAll($collection)
    {

    }

    public function size()
    {
        return count($this->set);
    }

    public function toArray()
    {
        return $this->set;
    }

    public function getIterator()
    {
        if ($this->set) {
            return new \ArrayIterator($this->set);
        }

        return new \ArrayIterator();
    }

}
