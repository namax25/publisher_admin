<?php

namespace Collections\Set;

/**
 * Description of HashSet
 *
 * @author namax
 */
class HashSet extends \Collections\Set\Set
{

    public function add($obj)
    {
        $key = $this->getHashCode($obj);
        if (array_key_exists($key, $this->set)) {
            return false;
        }
        $this->set[$key] = $obj;
        return true;
    }

    public function contains($obj)
    {
        return array_key_exists($this->getHashCode($obj), $this->set);
    }

    public function remove($obj)
    {
        $key = $this->getHashCode($obj);
        if ($this->contains($obj)) {
            unset($this->set[$key]);
            return true;
        }
        return false;
    }

    public function removeAll($collection)
    {

    }

    public function retainAll($collection)
    {

    }

    /**
     * @codeCoverageIgnore
     */
    private function getHashCode($obj)
    {
        if ($obj instanceof \Collections\ObjectInterface) {
            return $obj->hashCode();
        }
        if (is_object($obj)) {
            return spl_object_hash($obj);
        }
        return $obj;
    }

}
