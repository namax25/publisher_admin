<?php

namespace Collections\Proxy;

/**
 * Description of ProxySet
 *
 * @author namax
 */
class ProxySet extends \SplObjectStorage
{

    public function getHash($object)
    {
        if (!($object instanceof \Collections\ObjectInterface)) {
            throw new \Exception("Object must implements  \Collections\ObjectInterface");
        }
        return $object->hashCode();
    }

}
