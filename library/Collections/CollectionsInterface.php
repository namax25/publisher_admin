<?php

namespace Collections;

/**
 *
 * @author namax
 */
interface CollectionsInterface
{

    public function add($obj);

    public function clear();

    public function contains($obj);

    public function isEmpty();

    public function remove($obj);

    public function size();

    public function toArray();

    public function hashCode();

    public function removeAll($collection);

    public function retainAll($collection);
}
