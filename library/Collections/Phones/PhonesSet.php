<?php

namespace Collections\Phones;

/**
 * Description of Phone
 * Работа с коллекцией телефонов - очистка, хранение , выдача, уникальность.
 *
 * @author namax
 */
class PhonesSet extends \Collections\Set\Set
{

    const DEVIDER = " ";

    /**
     * Получаем первый телефон
     * @return type Returns the last value of array.
     * If array is empty (or is not an array), NULL will be returned.
     */
    public function getFirstPhone()
    {
        $arr = $this->toArray();
        return array_shift($arr);
    }

    /**
     * Получаем последний телефон
     * @return type Returns the shifted value, or NULL if array is empty or
     * is not an array.
     */
    public function getLastPhone()
    {
        $arr = $this->toArray();
        return array_pop($arr);
    }

    /**
     * Добавление телефона. Можно добавлять целый массив или строку
     * @param array\string $phone
     */
    public function add($phone)
    {
        if (is_array($phone)) {
            $this->addPhoneArray($phone);
        } else {
            $this->clearOnePhone($phone);
        }
    }

    /**
     * Добавление строки содержащей несколько телефонов, разделенных пробелом
     * @param type $phone
     */
    public function addFromPreparedString($phone)
    {
        $phonesArr = explode(self::DEVIDER, $phone);
        if (is_array($phonesArr)) {
            $this->addPhoneArray($phonesArr);
        }
    }

    /**
     * Получение телефонных номеров из объявления
     * Шаблоны телефонов
     * +79895897002,        79895897003,    7 9815897004,        7 982 5897005,
     * 7 983 589 7006,      5897025,        589-7026,        589-70-27
     * 7 984 589 70 07,     7 (985) 5897008,    7 (986) 589 7009,    7 (987) 589 70 10,
     * 7 (988) 5897011,     7 (989) 589-7012,    7 (910) 589-70-13,    7-9095897014,
     * 7-911-5897015,       7-912-589-7016,    7-913-589-70-17,    7 914 5897018,
     * 7 915 589-7019,      7 916 589-70-20,    7(985)589-7121,        7 (985)589-7122,
     * 7(985)589-71-23,     7 (985)589-71-24,    Т:915-309-3025        Т:(915) 309-3026
     * Т:(915) 309-30-27    9815897028        :9815897029        тел:89653923830
     * 8-909/-694/-94-81    8-/903/622/38/76
     *
     * @param type $text - текст
     * @return type массив с номерами
     */
    public function addFromText($text)
    {
        if (!$text) {
            return false;
        }

        $text = preg_replace('/(\s?\()+|(\)\s?)+/ui', '', $text);

        $this->clearPrices($text);
        $this->clearDates($text);
        $this->commonClean($text);


        $matches = [];
        $pattern = '/((\d|\(|\-|\:)?\d\d{9,10})|((\d|\(|\-|\:)[\s\(\-\/]{0,2}\d{3}[\s\-\)\/]{0,2}\s?'
            . '[\s\(\-\/]{0,2}\d{3}[\s\-\)\/]{0,2}\d{2}[\s\-\/]?\d{2})|(\(?\d{3}[\s\-\)]{0,2}\d{2}[\s\-]?\d{2})/';
        preg_match_all($pattern, $text, $matches);

        if ($matches && isset($matches[0])) {
            $this->addPhoneArray($matches[0]);
            return true;
        }
        return false;
    }

    /**
     * Перевод массива в строку
     * @return type
     */
    public function __toString()
    {
        return implode(self::DEVIDER, $this->toArray());
    }

    private function clearPrices(&$text)
    {
        $matches = [];

        preg_match_all('/(цена)?\s?\d+\s?руб(\s|\.)?/ui', $text, $matches);
        if ($matches && $matches[0]) {
            foreach ($matches[0] as $str) {
                $text = preg_replace('/' . $str . '/ui', '', $text);
            }
        }
    }

    private function clearDates(&$text)
    {
        $text = preg_replace('/\d{1,2}+\.\d{1,2}+\.\d{2,4}+/ui', '', $text);
    }

    private function commonClean(&$text)
    {
        $text = preg_replace('/(Тел\.?)+|[т\*\_\-\#\@\!\(\)\^\:\;\.\/]|([0-9]{0,3}\%)+/ui', '', $text);
    }

    /**
     * Очистка массива телефонов
     * @param type $phonesArray
     */
    private function addPhoneArray(array $phonesArray)
    {
        foreach ($phonesArray as $phone) {
            $phoneCleared = $this->clearPhone($phone);
            if ($phoneCleared) {
                parent::add($phoneCleared);
            }
        }
    }

    /**
     * Очистка телефона
     * @param type $phone
     */
    private function clearOnePhone($phone)
    {
        $phoneCleared = $this->clearPhone($phone);
        if ($phoneCleared) {
            parent::add($phoneCleared);
        }
    }

    /**
     * Очистка строки от всего кроме цифр и добваление 7 в начало номера.
     * В том числе с заменой 8
     * @param string $textNumber - строка с номером телефона
     * @return string|boolean
     */
    private function clearPhone($phone)
    {

        $digits = preg_replace('/\D/', '', $phone);
        $digitsCount = strlen($digits);

        if ($digitsCount < 7) {
            return false;
        }

//        if ($digitsCount == 7) {
//            return '7495' . $digits;
//        }

        if ($digitsCount == 10) {
            $digits = '7' . $digits;
        }

        if ($digitsCount > 10) {
            $digits = '7' . substr($digits, -10);
        }

        if (strlen($digits) < 10) {
            return false;
        }

        return $digits;
    }
}
