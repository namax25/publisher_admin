<?php
/**
 * Date: 12/16/14
 * Time: 7:52 PM
 * @author namax
 */

namespace Helpers;


class RearrangeArray extends \AbstractHelper

{
    /**
     * Сортирует вложенный массив по ключу, если есть такой элемент
     * то старый стирается
     * @param array $items
     * @param $key
     * @return array
     */
    public function byKey(array $items, $key)
    {
        $result = array();
        foreach ($items as $item) {
            if (isset($item[$key])) {
                $result[$item[$key]] = $item;
            }
        }
        return $result;
    }


    /**
     * Сортирует вложенный массив по двум ключам. Первый ключ, его значения
     * станут ключами в новом массиве, а значения второго ключа будут значениями
     * в новом массиве.
     * Если ключ дублируется, то значене сохраняется
     *
     * @param array $items - вложенный массив для сортировки.
     * @param  $key1 - ключ для сортировки
     * @param  $keyAsValue - ключ массива. Этот элемент будет использоваться
     * как значение будущего массива
     * @return array
     */
    public function byKeys(array $items, $keyAsKey, $keyAsValue)
    {
        $result = array();
        foreach ($items as $item) {
            if (isset($item[$keyAsKey]) && isset($item[$keyAsValue])) {
                $result[$item[$keyAsKey]] = $item[$keyAsValue];
            }
        }
        return $result;
    }
}