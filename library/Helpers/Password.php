<?php

namespace Helpers;

/**
 * Description of Password
 *
 * @author namax
 */
class Password extends \AbstractHelper
{

    public function generate($salt, $id, $login, $password)
    {
        return password_hash($id . $salt . $password . $login . $salt, PASSWORD_DEFAULT);
    }
}
