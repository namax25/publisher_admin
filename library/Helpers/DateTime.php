<?php

namespace Helpers;

/**
 * Description of DateTime
 *
 * @author namax
 */
class DateTime extends \AbstractHelper
{

    public function now()
    {
        return date("Y-m-d H:i:s");
    }

    public function printDate(\DateTime $date, $format = "Y-m-d H:i:s")
    {
        return $date->format($format);
    }

    public function check($dateStr, $timezone = null)
    {
        return $this->getDateTimeObject($dateStr, $timezone)->format("Y-m-d H:i:s");
    }

    public function getTimeStamp($dateStr, $timezone = null)
    {
        return $this->getDateTimeObject($dateStr, $timezone)->getTimestamp();
    }

    public function getDateTimeObject($dateStr = 'now', $timezone = null)
    {
        $dateTimeZone = null;
        if ($timezone) {
            $dateTimeZone = new \DateTimeZone($timezone);
        }
        $date = null;
        if ($dateStr === 'now') {
            $date = new \DateTime('now', $dateTimeZone);
            if ($dateTimeZone) {
                $date->setTimezone($dateTimeZone);
            }
            return $date;
        }
        if ($dateStr === null) {
            return $date;
        }

        if (preg_match('/[^\d]/', $dateStr)) {
            $date = new \DateTime($dateStr);
        } else {
            //если строка unix  Timestamp
            $date = new \DateTime();
            $date->setTimestamp($dateStr);
        }

        if ($dateTimeZone) {
            $date->setTimezone($dateTimeZone);
        }

        return $date;
    }

}
