<?php

class Process
{

    public static function check($name, $limit = 1)
    {
        $process = array();
        exec("ps xo args", $process);
        $ps = 0;
        $name = preg_quote($name, '/');
        foreach ($process as $p) {
            if (strpos($p, 'bin/sh') > 0) {
                continue;
            }
            $p = preg_quote($p, '/');

            if (\Helpers\Strings::create()->isStrContain($name, $p, false)) {
                $ps++;
            }
        }
        return $ps <= $limit;
    }

}
