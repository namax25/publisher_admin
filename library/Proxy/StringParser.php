<?php

namespace Proxy;

/**
 * Description of Parser
 *
 * @author namax
 */
class StringParser
{

    public static function create()
    {
        return new static();
    }

    public function parse($proxyStr)
    {
        $parts = explode('@', $proxyStr);
        $userParts = [];
        $proxyParts = [];
        if (count($parts) == 2) {
            $userParts = explode(':', $parts[0]);
            $proxyParts = explode(':', $parts[1]);
        } else {
            $proxyParts = explode(':', $parts[0]);
        }

        $url = (empty($proxyParts[0])) ? null : trim($proxyParts[0]);
        $port = (empty($proxyParts[1])) ? null : trim($proxyParts[1]);
        if (!$url || !$port) {
            return [];
        }

        return array(
            'url' => $url,
            'port' => $port,
            'login' => (empty($userParts[0])) ? null : trim($userParts[0]),
            'password' => (empty($userParts[1])) ? null : trim($userParts[1])
        );
    }

}
