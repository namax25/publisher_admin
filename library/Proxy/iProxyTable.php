<?php

namespace Proxy;

/**
 *
 * @author namax
 */
interface iProxyTable
{

    public function getAllActiveProxy();

    public function getOneActiveProxy();
}
