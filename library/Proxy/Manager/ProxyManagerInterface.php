<?php

namespace Proxy\Manager;

/**
 *
 * @author namax
 */
interface ProxyManagerInterface
{

    public function getProxy();

    public function setLastError(\Entity\Proxy $proxy, $code, $msg);

    public function count();

    public function getLastReturnProxy();
}
