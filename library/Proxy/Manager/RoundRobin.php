<?php

namespace Proxy\Manager;

/**
 * Description of RoundRobin
 *
 * @author namax
 */
class RoundRobin implements ProxyManagerInterface
{

    /**
     *
     * @var \Collections\Proxy\ProxySet
     */
    private $proxyList = null;

    /**
     * Репозитарий прокси
     * @var \Proxy\Repository\ProxyRepositoryInterface
     */
    private $proxyRepository = null;

    /**
     * Интервал на который откладываем прокси
     * @var  \DateInterval
     */
    private $usingInterval = null;

    /**
     * Лимит прокси , если меньше, то обновляем прокси
     * @var type
     */
    private $proxyMinLimit = 2;

    /**
     * Максимальное кол-во ошибок, которое может совершить прокси
     * @var type
     */
    private $maxErrors = 2;

    /**
     * Последний возвращенный прокси
     * @var type
     */
    private $lastReturnProxy = null;

    public function __construct(\Proxy\Repository\ProxyRepositoryInterface $proxyRepository)
    {
        $this->setProxyRepository($proxyRepository);
    }

    /**
     *
     * @return \Entity\Proxy
     */
    public function getProxy()
    {
        if ($this->isLowProxyLimit()) {
            throw new \Exception(__METHOD__ . " Proxy below min limit ");
        }

        if (!$this->getProxyList()->valid()) {
            $this->getProxyList()->rewind();
        }

        $proxy = null;

        do {

            $proxyTemp = $this->getNextProxy();
            if ($this->isWork($proxyTemp)) {
                if ($this->getUsingInterval()) {
                    $proxyTemp->lastUsedDate()->add($this->getUsingInterval());
                }
                $proxy = $proxyTemp;
                break;
            }
        } while ($this->getProxyList()->valid());

        $this->lastReturnProxy = $proxy ? (string)$proxy : null;
        return $proxy;
    }

    public function getLastReturnProxy()
    {
        return $this->lastReturnProxy;
    }

    public function count()
    {
        return $this->getProxyList()->count();
    }

    public function setLastError(\Entity\Proxy $proxy, $code, $msg = null)
    {
        if ($code == 0 || $code != 404 && $code >= 400) {
            $proxy->incError();
        }
        $this->getProxyRepository()->update($proxy, $code, $msg);
    }

    protected function isWork(\Entity\Proxy $proxy)
    {

        if ($proxy->getBlock()) {
            $this->getProxyList()->detach($proxy);
            return false;
        }

        if ($this->getUsingInterval()) {
            $date = new \DateTime();
            if ($proxy->lastUsedDate() > $date) {
                return false;
            }
        }

        return true;
    }

    /**
     *
     * @return \Entity\Proxy
     */
    protected function getNextProxy()
    {
        $proxy = $this->getProxyList()->current();
        $this->getProxyList()->next();
        return $proxy;
    }

    /**
     *
     * @return \Collections\Proxy\ProxySet
     */
    protected function getProxyList()
    {
        if ($this->proxyList == null) {
            $this->proxyList = new \Collections\Proxy\ProxySet();
            $this->setProxyList($this->getProxyRepository()->getAll());
            $this->proxyList->rewind();
            if ($this->proxyList->count() == 0) {
                throw new \Exception(__METHOD__ . " Proxy list has not recieved proxy from Repo");
            }
        }
        return $this->proxyList;
    }

    protected function isLowProxyLimit()
    {
        if ($this->getProxyMinLimit() > 0 && $this->getProxyList()->count() < $this->getProxyMinLimit()) {
            return true;
        }
        return false;
    }

    protected function setProxyList($proxyList)
    {
        if (is_array($proxyList)) {
            foreach ($proxyList as $item) {
                if ($item instanceof \Entity\Proxy) {
                    $this->proxyList->attach($item);
                }
            }
        }
    }

// <editor-fold defaultstate="collapsed" desc="Getters\Setters">
    /**
     *
     * @return \Proxy\Repository\ProxyRepositoryInterface
     */
    public function getProxyRepository()
    {
        return $this->proxyRepository;
    }

    protected function setProxyRepository(\Proxy\Repository\ProxyRepositoryInterface $proxyRepository)
    {
        $this->proxyRepository = $proxyRepository;
    }

    public function getMaxErrors()
    {
        return $this->maxErrors;
    }

    public function getUsingInterval()
    {
        return $this->usingInterval;
    }

    public function setMaxErrors($maxErrors)
    {
        $this->maxErrors = $maxErrors;
    }

    public function setUsingInterval(\DateInterval $usingInterval)
    {
        $this->usingInterval = $usingInterval;
    }

    public function getProxyMinLimit()
    {
        return $this->proxyMinLimit;
    }

    public function setProxyMinLimit($proxyMinLimit)
    {
        $this->proxyMinLimit = (int)$proxyMinLimit;
    }

// </editor-fold>
}
