<?php

namespace Proxy\Repository;

/**
 *
 * @author namax
 */
interface ProxyRepositoryInterface
{

    public function getOne();

    public function getAll();

    public function update(\Entity\Proxy $proxy, $code, $msg = null);

    public function isRenewable();
}
