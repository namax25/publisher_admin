<?php

namespace Proxy\Repository;

/**
 * Description of MySql
 *
 * @author namax
 */
class MySql implements ProxyRepositoryInterface
{

    /**
     *
     * @var \Proxy\iProxyTable
     */
    protected $dao = null;
    protected $renewable = null;
    protected $columnMapper = null;

    public function __construct($dao, \DbMappers\iDbMapper $columnMapper, $renewable = false)
    {
        $this->setDao($dao);
        $this->setColumnMapper($columnMapper);
        $this->renewable = (bool)$renewable;
    }

    /**
     *
     * @return \DbMappers\iDbMapper
     */
    public function getColumnMapper()
    {
        return $this->columnMapper;
    }

    public function setColumnMapper(\DbMappers\iDbMapper $columnMapper)
    {
        $this->columnMapper = $columnMapper;
    }

    public function isRenewable()
    {
        return $this->renewable;
    }

    /**
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getAll()
    {
        $result = $this->getDao()->getAllActiveProxy();
        $proxyList = [];

        if ($result) {
            $itemsArr = $result->toArray();
            foreach ($itemsArr as $item) {
                $proxyList[] = $this->getColumnMapper()->mapFromDb($item);
            }
        }

        return $proxyList;
    }

    /**
     *
     * @return \Entity\Proxy
     */
    public function getOne()
    {
        $result = $this->getDao()->getOneActiveProxy();
        if ($result) {
            return $this->getColumnMapper()->mapFromDb($result->getArrayCopy());
        }
        return false;
    }

    public function update(\Entity\Proxy $proxy, $httpCode = null, $msg = null)
    {
        $data = [];
        $data = $proxy->toArray();
        if ($this->getColumnMapper()) {
            $data = $this->getColumnMapper()->mapToDb($proxy);
        }
        return $this->getDao()->update($data, ['url' => $proxy->getUrl(), 'port' => $proxy->getPort()]);
    }

    /**
     * \Proxy\iProxyTable
     * @return type
     */
    protected function getDao()
    {
        return $this->dao;
    }

    protected function setDao(\Proxy\iProxyTable $dao)
    {
        if (!$dao instanceof \Dao\AbstractDao) {
            throw new \Exception(__METHOD__ . " Dao must extend  \Dao\AbstractDao and impliments \Proxy\iProxyTable");
        }
        $this->dao = $dao;
    }

}
