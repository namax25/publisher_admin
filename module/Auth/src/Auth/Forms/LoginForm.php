<?php

namespace Auth\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * LoginForm
 *
 * @author namax
 *
 */
class LoginForm extends Form
{

    public function prepareElements()
    {

        $this->setAttribute('method', 'post');

        $this->add($this->createElement(new Element\Text('login'), [
                'class' => 'form-control',
                'id' => 'inputLogin1',
                'placeholder' => 'введите Ваш e-mail'
            ], ['label' => 'Email'], ['class' => 'col-lg-2 control-label']
        ));

        $this->add($this->createElement(new Element\Password('password'), [
                'class' => 'form-control',
                'id' => 'inputEmail1',
                'placeholder' => 'введите Ваш пароль'], ['label' => 'пароль'], ['class' => 'col-lg-2 control-label']
        ));

        $this->add($this->createElement(new Element\ Submit('submit'), [
                'type' => 'submit',
                'value' => 'Войти',
                'class' => 'btn btn-default']
        ));
    }

    protected function createElement(Element $element, array $attributes, array $options = [], array $labelAttributes = [])
    {
        $element->setAttributes($attributes);
        if ($options) {
            $element->setOptions($options);
        }
        if ($labelAttributes) {
            $element->setLabelAttributes($labelAttributes);
        }
        return $element;
    }
}
