<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Auth\Forms;
use Auth\InputFilters;

class IndexController extends AbstractActionController
{

    private $modelAuth = null;
    private $authService = null;
    private $storage = null;
    private $formLogin = null;

    const MAX_LOGIN_ATTEMPS = 10;

    /**
     *
     * @var \App\Controllers\Messenge
     */
    protected $messages = null;

    public function loginAction()
    {
        $request = $this->getRequest();
        /*     $salt = 'EpJC8+eLNR79BDr0$6FQXSn3UJXBp2KuOUE0zSOf703d82d16ddztZMCqxHaJQS+eLNR79BDrSH+wJcn';;
     //                var_dump(password_hash("1EpJC8+eLNR79BDr0$6FQXSn3UJXBp2KuOUE0zSOf703d82d16ddztZMCqxHaJQS+eLNR79BDrSH+wJcn35750179namax25@gmail.comEpJC8+eLNR79BDr0$6FQXSn3UJXBp2KuOUE0zSOf703d82d16ddztZMCqxHaJQS+eLNR79BDrSH+wJcn", PASSWORD_DEFAULT));
             $options = [
                 'cost' => 11,
                 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
             ];
             echo password_hash("35750179", PASSWORD_BCRYPT, $options) . "\n";
             echo password_hash("123123", PASSWORD_BCRYPT, $options) . "\n";

             var_dump(password_verify("123123", '$2y$11$Yd0SbsxIpU2JAtb1i2BjmOYjRWtmk/WH4pMACYjYbTofOIsXnBBQy'));
             $userData = $this->getServiceLocator()->get('DaoFactory')->getDaoUsers()->getByLogin('namax25@gmail.com');
             var_dump($this->getAuthService()->getAdapter()->generateNewHash($userData, '35750179'));
             var_dump(password_verify("135750179" . $salt . "namax25@gmail.com" . $salt,
                     '$2y$10$zcXAzUoV/mHHisQ307Elu.oFGcLscjkEqLhGwtOM/Nr1Zj3Etsbny'));
     */

        if ($request->isPost()) {
            $this->getLoginForm()->setData($request->getPost());
            $this->getMessages()->clearAllMessages();
            if ($this->getLoginForm()->isValid()) {


                $userData = $this->getModelAuth()->getByLogin($request->getPost('login'));
                if ($userData) {

                    if ($userData->login_attempts < self::MAX_LOGIN_ATTEMPS) {
                        $this->getModelAuth()->updateLoginAttempts($request->getPost('login'),
                            ++$userData->login_attempts);

                        $this->getAuthService()->getAdapter()
                            ->setIdentity($request->getPost('login'))
                            ->setCredential($request->getPost('password'));
                        $result = $this->getAuthService()->authenticate();
                        foreach ($result->getMessages() as $message) {
                            $this->flashmessenger()->addErrorMessage($message);
                        }
                        if ($result->isValid()) {
//                    $this->flashmessenger()->addSuccessMessage("");
                            $this->getModelAuth()->updateLoginAttempts($request->getPost('login'), 0);
                            setcookie("timezone", $result->getIdentity()->timezone, 0, "/");
                            $this->getAuthService()->setStorage($this->getSessionStorage());

                            $this->getAuthService()->getStorage()->write($result->getIdentity());
                            return $this->redirect()->toRoute('app/default',
                                ['controller' => 'index', 'action' => 'dashboard']);
                        }

                        $this->getMessages()->addErrorMessage("Логин или пароль введены неверно");
                    } else {
                        $this->getMessages()->addErrorMessage("Количество допустимых попыток превышено. Требуется восстановить аккаунт");
                    }
                } else {
                    $this->getMessages()->addErrorMessage("Логин или пароль введены неверно");
                }
            }
        } else {
            if ($this->getServiceLocator()->get('AuthenticationService')->hasIdentity()) {
                return $this->redirect()->toRoute('app/default', ['controller' => 'index', 'action' => 'dashboard']);
            }
        }
//        $this->layout('layout/login');

        $viewModel = new ViewModel(['form' => $this->getLoginForm(), 'messages' => $this->getMessages()]);

        return $viewModel;
    }

    public function logoutAction()
    {
        $sesscontainer = new \Zend\Session\Container('sess_acl');
        $sesscontainer->getManager()->getStorage()->clear('sess_acl');
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        return $this->redirect()->toRoute('app');
    }

//<editor-fold defaultstate="collapsed" desc="Get\Set">
    public function getLoginForm()
    {
        if (!$this->formLogin) {
            $loginForm = new Forms\LoginForm();
            $loginForm->prepareElements();
            $loginFilters = new InputFilters\Login();
            $loginForm->setInputFilter($loginFilters->getInputFilter());
            $this->formLogin = $loginForm;
        }

        return $this->formLogin;
    }

    public function getAuthService()
    {
        if (!$this->authService) {
            $this->authService = $this->getServiceLocator()->get('AuthenticationService');
        }
        return $this->authService;
    }

    /**
     *
     * @return \Auth\Models\Auth
     */
    public function getModelAuth()
    {
        if (!$this->modelAuth) {
            $this->modelAuth = new \Auth\Models\Auth($this->getServiceLocator()->get('DaoFactory'));
        }

        return $this->modelAuth;
    }

    public function getSessionStorage()
    {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()->get('Common\Storage\Auth');
        }
        return $this->storage;
    }

    /**
     *
     * @return \App\Controllers\Messenger
     */
    public function getMessages()
    {
        if ($this->messages === null) {
            $this->messages = new \App\Controllers\Messenger();
        }

        return $this->messages;
    }
    // </editor-fold>
}
