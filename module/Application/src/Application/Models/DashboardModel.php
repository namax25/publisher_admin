<?php

namespace Application\Models;

/**
 * Description of DashboardModel
 *
 * @author namax
 */
class DashboardModel
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function countAllContacts($userId)
    {
        $count = $this->getDaoFactory()->getDaoUserContacts()->countAll($userId);
        return $count ? $count['allContacts'] : 0;
    }

    public function countAllCalendarEvents($userId)
    {
        $count = $this->getDaoFactory()->getDaoCalendarEvents()->countAll($userId);
        return $count ? $count['countAll'] : 0;
    }

    public function countNewEvents($userId)
    {
        $count = $this->getDaoFactory()->getDaoNotifications()->countAll($userId);
        return $count ? $count['countAll'] : 0;
    }

}
