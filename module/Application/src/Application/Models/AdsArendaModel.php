<?php

namespace Application\Models;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;

/**
 * Description of ContactsModel
 *
 * @author namax
 */
class AdsArendaModel
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function getAd($userId, $id)
    {
        $userId = \Helpers\Strings::create()->clearDigits($userId);

        if ($userId <= 0) {
            throw new \Exception(__METHOD__ . " Invalid user id " . $userId);
        }

        return $this->getDaoFactory()->getDaoArenda()->getByIdForUser($userId, $id);
    }


    public function getAdForSendToPublisher($userId, $id)
    {
        $ad = $this->getAd($userId, $id);


        if (!$ad) {
            return false;
        }


        return $this->getDaoFactory()->getDaoArenda()->getByIdForUser($userId, $id);
    }

    public function save(\Entities\EntityAdsArenda $ad)
    {
        return $this->getDaoFactory()->getDaoArenda()->insert($ad->toDb());
    }


    public function update($data, $id)
    {
        return $this->getDaoFactory()->getDaoArenda()->update($data, ['id' => $id]);
    }

    public function deleteAd($userId, $id)
    {
        return $this->getDaoFactory()->getDaoArenda()->delete(['id' => $id, "user_id" => $userId]);
    }

    /**
     * @param $userId
     * @return Paginator
     */
    public function getList($userId)
    {

        $userId = \Helpers\Strings::create()->clearDigits($userId);

        $select = new Select('arenda');
        $select->where(['user_id' => $userId]);
        $resultSetPrototype = new ResultSet();
        $paginatorAdapter = new DbSelect(
            $select,
            $this->getDaoFactory()->getDaoArenda()->getTableGateway()->getAdapter(),
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);

        return $paginator;
    }


    /**
     * Save image info to db
     *
     * @param \Entities\EntityImages $image
     * @return bool|int
     * @throws \Exception
     */
    public function saveImage(\Entities\EntityImages $image)
    {
        if (empty($image->getImageId())) {
            throw new \Exception(__METHOD__ . " Image id is invalid");
        }
        return $this->getDaoFactory()->getDaoImages()->insert($image->toDb());
    }


    public function getImageByHash($hash)
    {
        $hash = \Helpers\Strings::create()->clearMd5Hash($hash);
        return $this->getDaoFactory()->getDaoImages()->getByHash($hash);
    }

    public function updateImageDate($hash)
    {
        return $this->getDaoFactory()->getDaoImages()->updateImageDate($hash);
    }

    public function processNewImage($hash)
    {
        $imageRecord = $this->getImageByHash($hash);

        if ($imageRecord === false) {
            $imageEntity = new \Entities\EntityImages();
            $imageEntity->setImageId($hash);
            $this->saveImage($imageEntity);
        } else {
            $this->updateImageDate($hash);
        }

    }

    public function processImagesFromEditForm($adId, $photos)
    {

        if (empty($photos)) {
            $this->unbindAllImagesFromAd($adId);
            return;
        }

        $this->unbindImagesFromAd($adId, $photos);

        $allImages = $this->getAllAdImages($adId);
        $allImagesArr = $allImages->toArray();
        $imageIds = array_column($allImagesArr, "image_id");

        //TODO: всегда создает новую запись. Можно брать уже существующую , если записей больше 1
        //для выборки хэшей у которых есть уже 2 и более записей select *   from images where ad_id is null and  image_id in ('f4183ef1b428ad3878af7df24408f515', 'd84f7eb25d5ae9346149ad8d669c9cc9') Group by image_id having count(*) > 1
        //TODO: Надо обновлять даты существующем (Сейчас пропускается). Это надо для сборщика мусора
        //TODO: Написать сборщик мусора

        foreach ($photos as $photo) {
            if (in_array($photo, $imageIds)) {
                continue;
            }
            $imageEntity = new \Entities\EntityImages();
            $imageEntity->setImageId($photo);
            $imageEntity->setAdId($adId);
            $imageEntity->setDateLastUpdate(new \DateTime());
            $this->saveImage($imageEntity);
        }
    }

    public function unbindImagesFromAd($adId, array $hashes)
    {
        return $this->getDaoFactory()->getDaoImages()->unbindImagesFromAd($adId, $hashes);
    }

    public function unbindAllImagesFromAd($adId)
    {
        return $this->getDaoFactory()->getDaoImages()->unbindAllImagesFromAd($adId);
    }


    public function getAllAdImages($adId)
    {
        return $this->getDaoFactory()->getDaoImages()->getAllImagesByAdId($adId);
    }


}
