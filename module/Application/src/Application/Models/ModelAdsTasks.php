<?php
/**
 * Date: 12/27/14
 * Time: 7:59 PM
 * @author namax
 */

namespace Application\Models;


class ModelAdsTasks extends ModelAbstract
{


    public function save(\Entities\EntityAdsTasks $entityAdsTasks)
    {
        return $this->getDaoFactory()->getDaoAdsTasks()->insert($entityAdsTasks->toDb());
    }


}