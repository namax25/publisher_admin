<?php
/**
 * Date: 12/27/14
 * Time: 8:00 PM
 * @author namax
 */

namespace Application\Models;


abstract class ModelAbstract
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }


}