<?php

namespace Application\Models;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;

/**
 * Description of ContactsModel
 *
 * @author namax
 */
class PhonesModel
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function getPhone($userId, $id)
    {
        $userId = \Helpers\Strings::create()->clearDigits($userId);

        if ($userId <= 0) {
            throw new \Exception(__METHOD__ . " Invalid user id " . $userId);
        }

        return $this->getDaoFactory()->getDaoUserPhones()->getByIdForUser($userId, $id);
    }

    public function update(\Entities\PhonesEntity $phone, $id)
    {

        if ($phone->isEmpty()) {
            return false;
        }

        return $this->getDaoFactory()->getDaoUserPhones()->update(
            $phone->toDb(),
            ['id' => $id]
        );
    }

    public function updateStatus(\Entities\PhonesEntity $phone, $id)
    {

        if (is_null($phone->getStatus())) {
            return false;
        }

        return $this->getDaoFactory()->getDaoUserPhones()->update(
            ['status' => $phone->getStatus()],
            ['id' => $id]
        );

    }

    public function updateStatusByPhone(\Entities\PhonesEntity $phone)
    {

        if (is_null($phone->getStatus())) {
            return false;
        }

        return $this->getDaoFactory()->getDaoUserPhones()->update(
            ['status' => $phone->getStatus()],
            ['phone' => $phone->getPhone()]
        );

    }

    public function updateSmsCode($smsCode, $id)
    {
        return $this->getDaoFactory()->getDaoUserPhones()->update(
            ['sms_code' => $smsCode, 'status' => \Consts\Table\UserPhones\Status::USED],
            ['id' => $id]
        );

    }

    public function updatePhoneStatusAndAdPassword(\Entities\PhonesEntity $phone)
    {

        if ($phone->isEmptyPhone()) {
            throw new \Exception(__METHOD__ . " Status of empty phone can not be updated ");
        }

        if (!$phone->getStatus()) {
            throw new \Exception(__METHOD__ . " Status is empty ");
        }


        return $this->getDaoFactory()->getDaoUserPhones()->update(
            ['status' => $phone->getStatus(), 'ad_password' => $phone->getAdPassword()],
            ['user_id' => $phone->getUserId(), 'phone' => $phone->getPhone()]
        );

    }

    public function saveNewAcc(\Entities\EntityLogOpenAcc $newAcc)
    {
        if ($newAcc->hasAnyNulls(["id"])) {
            throw new \Exception(__METHOD__ . " Entity not filled correct");
        }

        return $this->getDaoFactory()->getDaoLogOpenAcc()->insert($newAcc->toDb());
    }


    public function save(\Entities\PhonesEntity $phone)
    {
        if (empty($phone->getUserId()) || empty($phone->getPhone())) {
            throw new \Exception(__METHOD__ . " Phone can't be empty");
        }
        return $this->getDaoFactory()->getDaoUserPhones()->insert($phone->toDb());
    }

    public function deletePhone($userId, $id)
    {
        return $this->getDaoFactory()->getDaoUserPhones()->delete(['id' => $id, "user_id" => $userId]);
    }


    public function getList($userId)
    {

        $userId = \Helpers\Strings::create()->clearDigits($userId);

        $select = new Select('user_phones');
        $select->where(['user_id' => $userId]);
        $select->order(['status asc']);
        $resultSetPrototype = new ResultSet();
        $paginatorAdapter = new DbSelect(
            $select,
            $this->getDaoFactory()->getDaoUserPhones()->getTableGateway()->getAdapter(),
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);

        return $paginator;
    }




}
