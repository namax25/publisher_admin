<?php

namespace Application\Models;

/**
 * Description of ContactsModel
 *
 * @author namax
 */
class ProfileModel
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function getUserProfileByLogin($login)
    {
        return $this->getDaoFactory()->getDaoUsers()->getByLogin($login);
    }

    public function update($userData, $userId)
    {
        $dataForUpdate = [];

        if (isset($userData['name'])) {
            $dataForUpdate['name'] = $userData['name'];
        }

        if (isset($userData['email'])) {
            $dataForUpdate['email'] = $userData['email'];
        }
        if (isset($userData['timezone'])) {

            $timezone = preg_replace("/[^a-zA-Z0-9\_\/]+/iu", '', $userData['timezone']);
            if ($timezone) {
                $dataForUpdate['timezone'] = $timezone;
            }
        }

        if (!$dataForUpdate) {
            return false;
        }

        return $this->getDaoFactory()->getDaoUsers()->update($dataForUpdate, ['id' => $userId]);
    }

}
