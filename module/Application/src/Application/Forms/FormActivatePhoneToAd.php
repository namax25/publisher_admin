<?php

namespace Application\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * LoginForm
 *
 * @author namax
 *
 */
class FormActivatePhoneToAd extends Form
{

    public function prepareElements()
    {

        $this->setAttribute('method', 'post');


        $this->add(
            array(
                'name' => 'phone_id',
                'type' => 'Hidden',
            )
        );


        $this->add($this->createElement(new Element\Text('ad_id'), [
            'class' => 'form-control',
            'id' => 'ad_id',
            'placeholder' => 'Введите id(номер) объявления'
        ], ['label' => 'Номер объявления'], ['class' => 'control-label']
        ));


        $this->add($this->createElement(new Element\ Submit('submit'), [
                'type' => 'submit',
                'value' => 'Сохранить',
                'class' => 'btn btn-default'
            ]
        ));
    }

    protected function createElement(
        Element $element,
        array $attributes,
        array $options = [],
        array $labelAttributes = []
    ) {
        $element->setAttributes($attributes);
        if ($options) {
            $element->setOptions($options);
        }
        if ($labelAttributes) {
            $element->setLabelAttributes($labelAttributes);
        }
        return $element;
    }

//    public function getInputFilterSpecification() {
//        return array(
//            'phone' => array(
//                'required' => true,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name' => 'Digits',
//                    ),
//                ),
//            )
//        );
//    }
}
