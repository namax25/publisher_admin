<?php

namespace Application\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * LoginForm
 *
 * @author namax
 *
 */
class AdsArendaForm extends Form
{

    public function prepareElements(\Dao\DaoFactory $daoFactory)
    {


        $this->setAttribute('method', 'post');


        $this->add(
            array(
                'name' => 'id',
                'type' => 'Hidden',
            )
        );


        $this->add(
            $this->createElement(
                new Element\Text('id_old'),
                [
                    'class' => 'form-control',
                    'id' => 'id_old',
                    'placeholder' => 'id оригинального объявления, если есть'
                ],
                ['label' => 'id оригинального объявления'],
                ['class' => 'control-label']
            )
        );


        $this->add(
            $this->createElement(
                new Element\Select('type'),
                [
                    'class' => 'form-control',
                    'id' => 'type',
                ],
                [
                    'label' => 'Тип объявления',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => \Consts\Table\Arenda\AdType::$names,
                ],
                ['class' => 'control-label']
            )
        );


        $this->add(
            $this->createElement(
                new Element\Select('category'),
                [
                    'class' => 'form-control',
                    'id' => 'category',
                ],
                [
                    'label' => 'Категория',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => \Consts\Table\Arenda\Category::$names,
                ],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Select('city'),
                [
                    'class' => 'form-control',
                    'id' => 'city',
                ],
                [
                    'label' => 'Регион',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => \Consts\Regions::$names,
                ],
                ['class' => 'control-label']
            )
        );


        $this->add(
            $this->createElement(
                new Element\Text('address'),
                [
                    'class' => 'form-control',
                    'id' => 'address',
                    'placeholder' => 'Адрес'
                ],
                ['label' => 'Адрес'],
                ['class' => 'control-label']
            )
        );


        $allMetro = $daoFactory->getDaoMetro()->getAllSortedByName();

        $metroArr = [];
        $metroMSKArr = [];
        $metroSPBArr = [];

        foreach ($allMetro as $metro) {

            if ($metro->region_id = \Consts\Regions::MSK) {
                $metroMSKArr[$metro->id] = $metro->name;

            }

            if ($metro->region_id = \Consts\Regions::SPB) {
                $metroSPBArr[$metro->id] = $metro->name;


            }


        }

        $metroArr = array(
            array(
                'label' => \Consts\Regions::$names[\Consts\Regions::MSK],
                'options' => $metroMSKArr
            ),
            array(
                'label' => \Consts\Regions::$names[\Consts\Regions::SPB],
                'options' => $metroSPBArr
            )
        );


        $this->add(
            $this->createElement(
                new Element\Select('metro'),
                [
                    'class' => 'form-control',
                    'id' => 'metro',
                ],
                [
                    'label' => 'Метро',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => $metroArr,
                ],
                ['class' => 'control-label']
            )
        );


        $rooms = [
            \Consts\Table\Arenda\Rooms::ROOMS_21 => 'Студия',
            \Consts\Table\Arenda\Rooms::ROOMS_1 => '1',
            \Consts\Table\Arenda\Rooms::ROOMS_2 => '2',
            \Consts\Table\Arenda\Rooms::ROOMS_3 => '3',
            \Consts\Table\Arenda\Rooms::ROOMS_4 => '4',
            \Consts\Table\Arenda\Rooms::ROOMS_5 => '5',
            \Consts\Table\Arenda\Rooms::ROOMS_6 => '6',
            \Consts\Table\Arenda\Rooms::ROOMS_7 => '7',
            \Consts\Table\Arenda\Rooms::ROOMS_8 => '8',
            \Consts\Table\Arenda\Rooms::ROOMS_9 => '9',
        ];
        $this->add(
            $this->createElement(
                new Element\Select('rooms'),
                [
                    'class' => 'form-control',
                    'id' => 'rooms',
                ],
                [
                    'label' => 'Кол-во комнат',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => $rooms,
                ],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Text('price'),
                [
                    'class' => 'form-control',
                    'id' => 'price',
                    'placeholder' => 'цена'
                ],
                ['label' => 'Цена'],
                ['class' => 'control-label']
            )
        );


        $floor = range(1, 99);

        $this->add(
            $this->createElement(
                new Element\Select('floor'),
                [
                    'class' => 'form-control',
                    'id' => 'floor',
                ],
                [
                    'label' => 'Этаж',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => $floor,
                ],
                ['class' => 'control-label']
            )
        );


        $this->add(
            $this->createElement(
                new Element\Select('floors'),
                [
                    'class' => 'form-control',
                    'id' => 'floors',
                ],
                [
                    'label' => 'Этажность',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => $floor,
                ],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Select('period'),
                [
                    'class' => 'form-control',
                    'id' => 'period',
                ],
                [
                    'label' => 'Период',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => \Consts\Table\Arenda\Period::$names,
                ],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Select('hometype'),
                [
                    'class' => 'form-control',
                    'id' => 'hometype',
                ],
                [
                    'label' => 'Тип дома',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => \Consts\Table\Arenda\Hometype::$names,
                ],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Text('area'),
                [
                    'class' => 'form-control',
                    'id' => 'area',
                    'placeholder' => 'Площадь'
                ],
                ['label' => 'Площадь'],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Textarea('description'),
                [
                    'class' => 'form-control',
                    'id' => 'description',
                    'placeholder' => 'Описание'
                ],
                ['label' => 'Описание'],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Text('name'),
                [
                    'class' => 'form-control',
                    'id' => 'name',
                    'placeholder' => 'Имя'
                ],
                ['label' => 'Имя'],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Text('email'),
                [
                    'class' => 'form-control',
                    'id' => 'email',
                    'placeholder' => 'Электронный адрес'
                ],
                ['label' => 'E-mail'],
                ['class' => 'control-label']
            )
        );


        $this->add(
            $this->createElement(
                new Element\ Submit('submit'),
                [
                    'type' => 'submit',
                    'value' => 'Сохранить',
                    'class' => 'btn btn-default'
                ]
            )
        );
    }


    protected function createElement(
        Element $element,
        array $attributes,
        array $options = [],
        array $labelAttributes = []
    ) {
        $element->setAttributes($attributes);
        if ($options) {
            $element->setOptions($options);
        }
        if ($labelAttributes) {
            $element->setLabelAttributes($labelAttributes);
        }
        return $element;
    }

}
