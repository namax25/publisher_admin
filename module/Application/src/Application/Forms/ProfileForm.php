<?php

namespace Application\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * LoginForm
 *
 * @author namax
 *
 */
class ProfileForm extends Form
{

    public function prepareElements()
    {

        $this->setAttribute('method', 'post');


//        $this->add(array(
//            'name' => 'id',
//            'type' => 'Hidden',
//        ));
//        $this->add($this->createElement(new Element\Hidden('id'), []));


        $this->add($this->createElement(new Element\Text('name'), [
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => 'Имя'
            ], ['label' => 'Имя'], ['class' => 'control-label']
        ));

        $this->add($this->createElement(new Element\Email('email'), [
                'class' => 'form-control',
                'id' => 'email',
                'placeholder' => 'email'
            ], ['label' => 'email'], ['class' => 'control-label']
        ));

        $listAbbreviations = \DateTimeZone::listAbbreviations();
        $listTimezones = [];

//        foreach ($listAbbreviations as $abbreviations) {
//            foreach ($abbreviations as $item) {
//                $hour = (float) round($item['offset'] / 60 / 60, 1);
//                $listTimezones[$item['timezone_id']] = str_replace("_", " ", $item['timezone_id']) . " ( " . ($hour < 0 ? $hour : "+" . $hour) . " )";
//            }
//        }
        $listIdentifiers = \DateTimeZone::listIdentifiers();

        foreach ($listIdentifiers as $identifier) {
            $listTimezones[$identifier] = str_replace("_", " ", $identifier);
        }
        $this->add($this->createElement(new Element\Select('timezone'), [
                'class' => 'form-control',
                'id' => 'datepickerprofile',
                'placeholder' => 'Часовой пояс',
            ], [
                'label' => 'Часовой пояс',
                'empty_option' => '-- Не выбрано --',
                'value_options' => $listTimezones,
            ], ['class' => 'control-label']
        ));


        $this->add($this->createElement(new Element\ Submit('submit'), [
                'type' => 'submit',
                'value' => 'Сохранить',
                'class' => 'btn btn-default'
            ]
        ));
    }

    protected function createElement(
        Element $element,
        array $attributes,
        array $options = [],
        array $labelAttributes = []
    ) {
        $element->setAttributes($attributes);
        if ($options) {
            $element->setOptions($options);
        }
        if ($labelAttributes) {
            $element->setLabelAttributes($labelAttributes);
        }
        return $element;
    }

//    public function getInputFilterSpecification() {
//        return array(
//            'phone' => array(
//                'required' => true,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name' => 'Digits',
//                    ),
//                ),
//            )
//        );
//    }
}
