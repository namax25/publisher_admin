<?php

namespace Application\InputFilters;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use \Consts\Table\Arenda;

/**
 * Login
 *
 * @author namax
 *
 */
class AdsArendaFormFilters implements InputFilterAwareInterface
{

    protected $inputFilter;
    protected $inputFactory;

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    /**
     * @return mixed
     */
    public function getInputFactory()
    {
        if (is_null($this->inputFactory)) {
            $this->inputFactory = new InputFactory();

        }
        return $this->inputFactory;
    }


    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add($this->createValidatorsForDigit('id_old'));
            $inputFilter->add($this->createValidatorsForConsts('type', Arenda\AdType::$allParams));
            $inputFilter->add($this->createValidatorsForConsts('category', Arenda\Category::$allParams));
            $inputFilter->add($this->createValidatorsForConsts('city', \Consts\Regions::$allParams));
            $inputFilter->add($this->createValidatorsForText('address', 1, 250));
            $inputFilter->add($this->createValidatorsForConsts('rooms', Arenda\Rooms::$allParams));
            $inputFilter->add($this->createValidatorsForDigit('price'));
            $inputFilter->add($this->createValidatorsForDigit('floor'));
            $inputFilter->add($this->createValidatorsForDigit('floors'));
            $inputFilter->add($this->createValidatorsForConsts('period', Arenda\Period::$allParams));
            $inputFilter->add($this->createValidatorsForConsts('hometype', Arenda\Hometype::$allParams));
            $inputFilter->add($this->createValidatorsForDigit('area'));
            $inputFilter->add($this->createValidatorsForText('description', 1, 5000));
            $inputFilter->add($this->createValidatorsForText('name', 1, 100));
            $inputFilter->add(
                $this->getInputFactory()->createInput(
                    array(
                        'name' => 'email',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'EmailAddress',
                            ),
                        ),
                    )
                )
            );


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }


    protected function createValidatorsForConsts($name, $params)
    {

        return $this->getInputFactory()->createInput(
            array(
                'name' => $name,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^0-9]+/iu',
                            'replacement' => '',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => $params
                        ),
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'haystack' => 'UTF-8',
                            'min' => 1,
                            'max' => 11,
                        ),
                    ),
                ),
            )
        );
    }

    protected function createValidatorsForDigit($name)
    {
        return $this->getInputFactory()->createInput(
            array(
                'name' => $name,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^0-9]+/iu',
                            'replacement' => '',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'haystack' => 'UTF-8',
                            'min' => 1,
                            'max' => 11,
                        ),
                    ),
                ),
            )
        );
    }

    protected function createValidatorsForText($name, $minLen, $maxLen)
    {
        return $this->getInputFactory()->createInput(
            array(
                'name' => $name,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' =>
                                '/[^a-zA-Z0-9а-яА-ЯёЁ\_\=\ \{\}\[\]\(\)\,\.\!\?\@\*\+\-\;\:\|\/\#]+/iu',
                            'replacement' => '',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => $minLen,
                            'max' => $maxLen,
                        ),
                    ),
                ),
            )
        );
    }

}
