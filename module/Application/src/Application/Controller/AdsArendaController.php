<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use \Helpers\Strings;

class AdsArendaController extends AbstractController
{

    protected $modelAdsArends = null;
    protected $formAdsArenda = null;
    const ITEMS_PER_PAGE = 20;

    protected $imagesResize = null;
    protected $imagesUtils = null;

    /**
     * @return \Images\Resize
     */
    public function getImagesResize()
    {
        if ($this->imagesResize === null) {
            $this->imagesResize = new \Images\Resize();
        }
        return $this->imagesResize;
    }

    public function getImagesUtils()
    {
        if ($this->imagesUtils === null) {
            $this->imagesUtils = new \Images\Utils();
        }
        return $this->imagesUtils;
    }

    /**
     *
     * @return \Application\Forms\AdsArendaForm()
     */
    public function getFormAdsArenda()
    {
        if (!$this->formAdsArenda) {
            $form = new \Application\Forms\AdsArendaForm();

            $form->prepareElements($this->getDaoFactory());
            $filters = new \Application\InputFilters\AdsArendaFormFilters();
            $form->setInputFilter($filters->getInputFilter());
            $this->formAdsArenda = $form;
        }

        return $this->formAdsArenda;
    }

    /**
     *
     * @return \Application\Models\AdsArendaModel
     */
    public function getModelAdsArenda()
    {
        if ($this->modelAdsArends === null) {
            $this->modelAdsArends = new \Application\Models\AdsArendaModel(
                $this->getServiceLocator()->get('DaoFactory')
            );
        }
        return $this->modelAdsArends;
    }


    public function addAction()
    {

        $request = $this->getRequest();
        $this->getMessages()->clearAllMessages();

        $userData = $this->getAuthService()->getIdentity();

        $photos = [];

        $adsArendaEntity = new \Entities\EntityAdsArenda();


        if ($request->isPost()) {
            $photos = $this->params()->fromPost('photos', []);

            $this->getFormAdsArenda()->setData($request->getPost());

            if ($this->getFormAdsArenda()->isValid()) {

                $adsArendaEntity->setUserId($userData->id)->setImages($photos);
                $adsArendaEntity->fromArray($this->getFormAdsArenda()->getData());

                $this->getModelAdsArenda()->save($adsArendaEntity);
                $this->getMessages()->addSuccessMessage("Успешно обновлено");
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }

        $this->getFormAdsArenda()->setData($request->getPost());
        $adsArendaEntity->setImages($photos);

        $viewModel = new ViewModel(
            [
                'form' => $this->getFormAdsArenda(),
                'id' => $userData->id,
                'messages' => $this->getMessages(),
                'photos' => $adsArendaEntity->getImagesUrls(),
                'photosHashes' => $adsArendaEntity->getImages()
            ]
        );
        return $viewModel;
    }


    public function listAction()
    {

        $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();

        $list = $this->getModelAdsArenda()->getList($idenity->id)
            ->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1))
            ->setItemCountPerPage(self::ITEMS_PER_PAGE);


        return (new ViewModel())->setVariable('itemsList', $list);
    }

    public function editAction()
    {
        $this->getMessages()->clearAllMessages();

        $id = (int)$this->params()->fromRoute('id', 0);

        $request = $this->getRequest();

        if (!$id) {
            return $this->redirect()->toRoute(
                'app/default',
                array(
                    'controller' => 'ads-arenda',
                    'action' => 'add'
                )
            );
        }


        $userData = $this->getAuthService()->getIdentity();

        $adsArendaEntity = new \Entities\EntityAdsArenda();

        if ($request->isPost()) {

            $photos = $this->params()->fromPost('photos', []);


            $this->getFormAdsArenda()->setData($request->getPost());

            if ($this->getFormAdsArenda()->isValid()) {

                $adsArendaEntity->fromArray($this->getFormAdsArenda()->getData());
                $adsArendaEntity->setUserId($userData->id)->setImages($photos);
                $data = $adsArendaEntity->toDb();

                if (!$photos) {
                    $data['images'] = null;
                }

                $this->getModelAdsArenda()->update($data, $adsArendaEntity->getId());
                $this->getModelAdsArenda()->processImagesFromEditForm($adsArendaEntity->getId(), $photos);


                $this->getMessages()->addSuccessMessage("Успешно обновлено");
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        } else {

        }

        $data = $this->getModelAdsArenda()->getAd($userData->id, $id);
        $adsArendaEntity->setImages(\Zend\Json\Json::decode($data->images));

        if (empty($data)) {
            return $this->redirect()->toRoute(
                'app/default',
                array(
                    'controller' => 'ads-arenda',
                    'action' => 'add'
                )
            );
        }

        $this->getFormAdsArenda()->setData($data);

        $viewModel = new ViewModel(
            [
                'form' => $this->getFormAdsArenda(),
                'id' => $id,
                'messages' => $this->getMessages(),
                'photos' => $adsArendaEntity->getImagesUrls(),
                'photosHashes' => $adsArendaEntity->getImages()
            ]
        );

        return $viewModel;
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);

        $userData = $this->getAuthService()->getIdentity();


        if ($id && $userData->id) {
            $this->getModelAdsArenda()->deleteAd($userData->id, $id);
            $this->getModelAdsArenda()->unbindAllImagesFromAd($id);
        }

        return $this->redirect()->toRoute(
            'app/default',
            [
                'controller' => 'ads-arenda',
                'action' => 'list'
            ]
        );
    }


    public function uploadImagesAjaxAction()
    {
        try {

            $this->getJsonResponse()->clearAll();

            $request = $this->getRequest();
            if ($request->isPost()) {


                $post = $request->getPost()->toArray();


                if (empty($post['img'])) {
                    return;

                }

                $base64 = preg_replace('/^data:image\/(png|jpg|jpeg);base64,/', "", $post['img']);

                $matches = [];
                preg_match("/(png|jpg|jpeg)/", $post['img'], $matches);

                $extension = $this->getImagesUtils()->getExtensionByType(!empty($matches[1]) ? $matches[1] : null);

                if ($extension === false) {
                    $this->getJsonResponse()->addErr('Недопустимый файл. Разрешены картинки png, jpeg');
                    return new JsonModel($this->getJsonResponse());
                }

                $image = base64_decode($base64);

                //Берем хэш от base64 строки, чтобы совпадал с js хешем
                $hash = md5($base64);
                // $hash = md5($image);

                if ($this->getImagesUtils()->isImageToLarge($image)) {
                    $this->getJsonResponse()->addErr('Изображение слишком большое. Максимум 2 MB');
                    return new JsonModel($this->getJsonResponse());
                }

                $imageFolder = $this->createFolderForImage($hash);

                if (!is_writable($imageFolder)) {
                    throw new \Exception(__METHOD__ . " Не удалось создать директорию. ");
                }

                $pathToImage = $imageFolder . DIRECTORY_SEPARATOR . $hash . "." . $extension;

                $isSuccess = file_exists($pathToImage) ?: file_put_contents($pathToImage, $image);

                $this->getImagesResize()->createStandartSizePhotos($imageFolder, $hash . "." . $extension, $hash);

                //TODO: send resize image request to Rabbit   ;


                if ($isSuccess) {
                    $this->getJsonResponse()->addSuccess('Файл успешно сохранен');
                    $this->getModelAdsArenda()->processNewImage($hash);
                } else {
                    $this->getJsonResponse()->addErr('Ошибка сохранения файла');

                }

            }

        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $this->getJsonResponse()->addErr("Ошибка обновления данных");
        }


        return new JsonModel($this->getJsonResponse());
    }

    public function uploadImagesFormAction()
    {
        return new ViewModel();
    }

    private function createFolderForImage($fileMd5Hash)
    {
        $mainFolderLevel = $fileMd5Hash[0] . $fileMd5Hash[1];
        $oneFolderLevel = $fileMd5Hash[2] . $fileMd5Hash[3];

        $pathToFile = "www" . DIRECTORY_SEPARATOR . $this->getConfig('pathes')['file_uploads'] . DIRECTORY_SEPARATOR .
            $mainFolderLevel . DIRECTORY_SEPARATOR . $oneFolderLevel;

        if (!is_dir($pathToFile)) {
            mkdir($pathToFile, 0755, true);
        }
        chmod($pathToFile, 0755);

        if (!is_writable($pathToFile)) {
            throw new \Exception(__METHOD__ . " Не удалось создать директорию. " . $pathToFile);
        }


        return $pathToFile;
    }

    public function imageExistsAction()
    {

        try {

            $this->getJsonResponse()->clearAll();

            $request = $this->getRequest();
//            $this->getResponse()->getHeaders()
//                ->addHeaderLine('X-RateLimit-UserLimit', '60');

            if ($request->isPost()) {


                $hash = $this->params()->fromPost('hash', 0);
                $type = $this->params()->fromPost('type', 'jpg');

                $extension = $this->getImagesUtils()->getExtensionByType($type);
                $imageFolder = $this->createFolderForImage($hash);
                $pathToImage = $imageFolder . DIRECTORY_SEPARATOR . $hash . "." . $extension;


                if (file_exists($pathToImage)) {
                    $this->getJsonResponse()->addSuccess('Файл существует');

                    $this->getImagesResize()->createStandartSizePhotos($imageFolder, $hash . "." . $extension, $hash);

                } else {
                    $this->getJsonResponse()->addErr('Файл не найден');
                }


            }

        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $this->getJsonResponse()->addErr("Ошибка обновления данных");
        }


        return new JsonModel($this->getJsonResponse());
    }

    /**
     * Не используется
     * @return void|JsonModel
     * @throws \Exception
     */
    public function uploadImagesAction()
    {
        $this->getJsonResponse()->clearAll();
        return new JsonModel($this->getJsonResponse());

        $request = $this->getRequest();
        if ($request->isPost()) {


            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );


            if (empty($post['imagefile'])) {
                return;

            }
            $pathinfo = pathinfo($post['imagefile']['name']);
            $hash = md5_file($post['imagefile']['tmp_name']);

            $pathToFile = $this->createFolderForImage($hash);

            if (!is_writable($pathToFile)) {
                throw new \Exception(__METHOD__ . " Не удалось создать директорию. ");
            }

            if (!file_exists($pathToFile . DIRECTORY_SEPARATOR . $hash . "." . $pathinfo['extension'])) {

                //проверяем загрузили или нет изображение
                if (!move_uploaded_file(
                    $post['imagefile']['tmp_name'],
                    $pathToFile . DIRECTORY_SEPARATOR . $hash . "." . $pathinfo['extension']
                )
                ) {
                    $this->errors[] = 'Ошибка копирования';
                } else {
                    $this->getJsonResponse()->addSuccess('Файл успешно сохранен');
                }
            }
        }

        return new JsonModel($this->getJsonResponse());
    }


}
