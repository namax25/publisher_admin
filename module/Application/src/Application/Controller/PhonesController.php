<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class PhonesController extends AbstractController
{

    protected $modelPhones = null;
    protected $formPhones = null;
    const ITEMS_PER_PAGE = 20;


    public function addAction()
    {

        $request = $this->getRequest();
        $this->getMessages()->clearAllMessages();

        $userData = $this->getAuthService()->getIdentity();

        if ($request->isPost()) {
            $this->getPhonesForm()->setData($request->getPost());

            if ($this->getPhonesForm()->isValid()) {
                $phone = new \Entities\PhonesEntity();
                $phone->setUserId($userData->id);
                $phone->fromArray($this->getPhonesForm()->getData());
                $this->getModelPhones()->save($phone);
                $this->getMessages()->addSuccessMessage("Успешно обновлено");
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }

        $this->getPhonesForm()->setData($request->getPost());
        $viewModel = new ViewModel(
            [
                'form' => $this->getPhonesForm(),
                'id' => $userData->id,
                'messages' => $this->getMessages()
            ]
        );
        return $viewModel;
    }


    /**
     *
     * @return \Application\Forms\Ph()
     */
    public function getPhonesForm()
    {
        if (!$this->formPhones) {
            $form = new \Application\Forms\PhonesForm();
            $form->prepareElements();
            $filters = new \Application\InputFilters\PhonesFormFilters();
            $form->setInputFilter($filters->getInputFilter());
            $this->formPhones = $form;
        }

        return $this->formPhones;
    }

    /**
     *
     * @return \Application\Models\PhonesModel
     */
    public function getModelPhones()
    {
        if ($this->modelPhones === null) {
            $this->modelPhones = new \Application\Models\PhonesModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelPhones;
    }


    public function listAction()
    {

        $page = 0;
        $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();

        $list = $this->getModelPhones()->getList($idenity->id);
        $list->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
        $list->setItemCountPerPage(self::ITEMS_PER_PAGE);
        $viewModel = new ViewModel();
        $viewModel->setVariable('itemsList', $list);
        return $viewModel;
    }

    public function editAction()
    {
        $this->getMessages()->clearAllMessages();

        $id = (int)$this->params()->fromRoute('id', 0);

        $request = $this->getRequest();

        if (!$id) {
            return $this->redirect()->toRoute('app/default', array(
                'controller' => 'phones',
                'action' => 'add'
            ));
        }
        $userData = $this->getAuthService()->getIdentity();

        $phoneEntity = new \Entities\PhonesEntity();

        if ($request->isPost()) {
            $this->getPhonesForm()->setData($request->getPost());

            if ($this->getPhonesForm()->isValid()) {

                $phoneEntity->fromArray($this->getPhonesForm()->getData());
                $phoneEntity->setUserId($userData->id);
                $phoneEntity->setStatus(\Consts\Table\UserPhones\Status::NEWONE);

                $this->getModelPhones()->update($phoneEntity, $phoneEntity->getId());

                $this->getMessages()->addSuccessMessage("Успешно обновлено");
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }

        $data = $this->getModelPhones()->getPhone($userData->id, $id);

        if (empty($data)) {
            return $this->redirect()->toRoute('app/default', array(
                'controller' => 'phone',
                'action' => 'add'
            ));
        }

        $this->getPhonesForm()->setData($phoneEntity->clear()->fromDb($data)->toArray());


        $viewModel = new ViewModel(
            ['form' => $this->getPhonesForm(), 'id' => $id, 'messages' => $this->getMessages()]
        );

        return $viewModel;
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);

        $userData = $this->getAuthService()->getIdentity();


        if ($id && $userData->id) {
            $this->getModelPhones()->deletePhone($userData->id, $id);
        }
        return $this->redirect()->toRoute('app/default', array(
            'controller' => 'phones',
            'action' => 'list'
        ));
    }


}
