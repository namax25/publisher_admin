<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 28/10/14
 * Time: 03:50 PM
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class AbstractController extends AbstractActionController
{


    /**
     * @var \Zend\Log\LoggerInterface
     */
    protected $logDb = null;

    /**
     * @var \Zend\Authentication\AuthenticationServic
     */
    protected $authService = null;

    /**
     * @var \App\Controllers\Messenger
     */
    protected $messages = null;

    /**
     * @var \Dao\DaoFactory
     */
    protected $daoFactory = null;


    /**
     * @var \App\Controllers\Responses\JsonResponse
     */
    protected $jsonResponse = null;


    /**
     * @var \Redis
     */
    protected $redis;


    protected $config;

    /**
     * @return mixed
     */
    public function getConfig($name)
    {
        return $this->getServiceLocator()->get('config')[$name];
    }


    /**
     * @return \Dao\DaoFactory
     */
    public function getDaoFactory()
    {
        if ($this->daoFactory === null) {
            $this->daoFactory = $this->getServiceLocator()->get('DaoFactory');
        }

        return $this->daoFactory;
    }

    /**
     *
     * @return \App\Controllers\Messenger
     */
    public function getMessages()
    {
        if ($this->messages === null) {
            $this->messages = new \App\Controllers\Messenger();
        }

        return $this->messages;
    }


    /**
     * @return \App\Controllers\Responses\JsonResponse
     */
    public function getJsonResponse()
    {
        if ($this->jsonResponse === null) {
            $this->jsonResponse = new \App\Controllers\Responses\JsonResponse();
        }

        return $this->jsonResponse;
    }


    /**
     *
     * @return \Zend\Log\LoggerInterface
     */
    protected function getLogDb()
    {
        if ($this->logDb === null) {
            return $this->getServiceLocator()->get('log_db');
        }
        return $this->logDb;
    }

    /**
     *
     * @return  \Zend\Authentication\AuthenticationService
     */
    public function getAuthService()
    {
        if (!$this->authService) {
            $this->authService = $this->getServiceLocator()->get('AuthenticationService');
        }
        return $this->authService;
    }

    /**
     * @return \Mail\iMail
     */
    protected function getMailSevice()
    {
        return $this->getServiceLocator()->get('MailService');
    }


    /**
     * @return \Redis
     */
    protected function getRedis()
    {
        if ($this->redis === null) {
            $this->redis = $this->getServiceLocator()->get('Redis');
        }
        return $this->redis;
    }

} 