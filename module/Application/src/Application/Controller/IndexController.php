<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Forms;
use Application\InputFilters;
use Zend\Session\Container;


class IndexController extends AbstractController
{

    /**
     *
     * @var \Application\Models\DashboardModel
     */
    protected $modelDashboard = null;

    public function indexAction()
    {

//        $session = new Container('base');
//
//        $counter = 1;
//
//        if ($session->offsetExists('counter')) {
//            $counter += $session->offsetGet('counter');
//        }
//
//        $session->offsetSet('counter', $counter);
//
//        var_dump($session->offsetGet('counter'));

        //$key = new \Datatypes\Redis\Key($this->getRedis());

        //$key->expire("test", "4", 15);


        // var_dump($key->getRedis()->get("test"));

//        $data = $this->getDaoFactory()->getDaoArenda()->getById(1);
//        $converter = new \Converters\Ads\AdToAvito($this->getDaoFactory());

//        var_dump($converter->convert($data->getArrayCopy()));


        /*
                $userData = $this->getServiceLocator()->get('DaoFactory')->getDaoUsers()->getByLogin('79052168170');
                var_dump($this->getAuthService()->getAdapter()->generateNewHash($userData, '11554225'));

                $userData = $this->getServiceLocator()->get('DaoFactory')->getDaoUsers()->getByLogin('79112408040');
                var_dump($this->getAuthService()->getAdapter()->generateNewHash($userData, '72039722'));*/

//            $XHPROF_ROOT = "/usr/share/php";
//        include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_lib.php";
//        include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_runs.php";
//        xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
//        if (!$this->getServiceLocator()->get('AuthenticationService')->hasIdentity()) {
//            return $this->redirect()->toRoute('auth', ['controller' => 'index', 'action' => 'login']);
//        }
//        var_dump($this->getServiceLocator()->get('DaoFactory')->getDaoUsers()->getByLogin('79255086415'));
        $viewModel = new ViewModel(['userData' => $this->getAuthService()->getStorage()->read()]);


//        $profiler_namespace = strtolower(str_replace(["/", "-"], "_", $_SERVER['REQUEST_URI'])); // namespace for your application
//        $xhprof_data = xhprof_disable();
//        $xhprof_runs = new \XHProfRuns_Default();
//        $run_id = $xhprof_runs->save_run($xhprof_data, $profiler_namespace);
        return $viewModel;
//        return new ViewModel();
    }

    public function dashboardAction()
    {


        $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();


        $viewModel = new ViewModel(
            [
            ]);

        return $viewModel;
    }

    /**
     *
     * @return \Application\Models\DashboardModel
     */
    public function getModelDashboard()
    {
        if ($this->modelDashboard === null) {
            $this->modelDashboard = new \Application\Models\DashboardModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelDashboard;
    }
}
