<?php
/**
 * Date: 12/23/14
 * Time: 6:09 PM
 * @author namax
 */

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ImportAdsController extends AbstractController
{

    private $allowedImports = [
        'smartagent'
    ];

    private $rabbitConnection = null;


    public function __destruct()
    {

        if ($this->rabbitConnection) {
            $this->rabbitConnection->close();

        }
    }

    public function indexAction()
    {


        return new ViewModel();
    }

    public function requestAction()
    {
        $jsonResponse = new \App\Controllers\Responses\JsonResponse();

        try {

            if ($this->getRequest()->isPost()) {

                $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();


                $type = $this->params()->fromPost('type', 0);

                $typeKey = array_search($type, $this->allowedImports);

                if ($typeKey !== false) {
                    $typeClear = $this->allowedImports[$typeKey];

                    $this->rabbitConnection = $this->getServiceLocator()->get('RabbitMQ');
                    $channel = $this->rabbitConnection->channel();
                    $channel->queue_declare('importads_'.$typeClear, false, false, false, false);
                    $msg = new AMQPMessage(json_encode(
                        [
                            'type' => $typeClear,
                            'user_id' => $idenity->id,
                            'old_id' => $idenity->old_id,
                        ]
                    ));
                    $channel->basic_publish($msg, '', 'importads_'.$typeClear);

                    $jsonResponse->addSuccess("Запрос принят");
                    $channel->close();

                } else {
                    $jsonResponse->addErr("Данный импорт не поддерживается");
                }


            } else {
                $jsonResponse->addErr("Ошибка получения данных");
            }
        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $jsonResponse->addErr("Ошибка импорта. Повторите попытку позже");
        }
        return new JsonModel($jsonResponse);

    }

}