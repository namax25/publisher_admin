<?php
/**
 * Date: 12/14/14
 * Time: 11:25 AM
 * @author namax
 */

namespace Application\Controller;

use Zend\View\Model\JsonModel;
use Helpers\Strings;
use Zend\View\Model\ViewModel;
use Zend\Json\Json;

class SmsConfirmController extends AbstractController
{

    protected $modelPhones = null;
    protected $formSmsCode = null;
    protected $formActivatePhoneToAd = null;
    protected $listAdsTasks = null;
    protected $modelAdsArenda = null;
    protected $modelAdsTasks = null;


    /**
     *
     * @return \Application\Models\AdsArendaModel
     */
    public function getModelAdsArenda()
    {
        if ($this->modelAdsArenda === null) {
            $this->modelAdsArenda = new \Application\Models\AdsArendaModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelAdsArenda;
    }


    /**
     *
     * @return \Application\Models\PhonesModel
     */
    public function getModelPhones()
    {
        if ($this->modelPhones === null) {
            $this->modelPhones = new \Application\Models\PhonesModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelPhones;
    }

    /**
     *
     * @return \Application\Models\ModelAdsTasks
     */
    public function getModelAdsTasks()
    {
        if ($this->modelAdsTasks === null) {
            $this->modelAdsTasks = new \Application\Models\ModelAdsTasks(
                $this->getServiceLocator()->get('DaoFactory')
            );
        }
        return $this->modelAdsTasks;
    }

    /**
     * @return \Datatypes\Redis\ListAdsTasks
     */
    public function getListAdsTasks()
    {

        if ($this->listAdsTasks === null) {
            $this->listAdsTasks = new \Datatypes\Redis\ListAdsTasks($this->getRedis());
        }
        return $this->listAdsTasks;
    }

    /**
     *
     * @return \Application\Forms\SmsCodeForm()
     */
    public function getFormSmsCode()
    {
        if (!$this->formSmsCode) {
            $form = new \Application\Forms\SmsCodeForm();
            $form->prepareElements();
            $filters = new \Application\InputFilters\SmsCodeFilter();
            $form->setInputFilter($filters->getInputFilter());
            $this->formSmsCode = $form;
        }

        return $this->formSmsCode;
    }

    /**
     *
     * @return \Application\Forms\FormActivatePhoneToAd()
     */
    public function getFormActivatePhoneToAd()
    {
        if (!$this->formActivatePhoneToAd) {
            $form = new \Application\Forms\FormActivatePhoneToAd();
            $form->prepareElements();
            $filters = new \Application\InputFilters\FiltersActivatePhoneToAd();
            $form->setInputFilter($filters->getInputFilter());
            $this->formActivatePhoneToAd = $form;
        }

        return $this->formActivatePhoneToAd;
    }


    /**
     *
     * Ввод смс кода пользователем
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function enterCodeAction()
    {

        $this->getMessages()->clearAllMessages();

        $id = (int)$this->params()->fromRoute('id', 0);

        $request = $this->getRequest();

        $userData = $this->getAuthService()->getIdentity();


        if (!$id || !$userData) {
            return $this->redirect()->toRoute('app/default', array(
                'controller' => 'phones',
                'action' => 'list'
            ));
        }

        $phoneEntity = new \Entities\PhonesEntity();

        $updatedData = null;

        if ($request->isPost()) {
            $this->getFormSmsCode()->setData($request->getPost());

            if ($this->getFormSmsCode()->isValid()) {

                $data = $this->getFormSmsCode()->getData();


                if ($this->getModelPhones()->updateSmsCode($data['sms_code'], $id)) {

                    $updatedData = $this->getModelPhones()->getPhone($userData->id, $id);
                    $this->getRedis()->set(
                        $userData->id . ":" . $updatedData->phone,
                        $data['sms_code']
                    );


                    $this->getMessages()->addSuccessMessage("Смс код сохранен");
                } else {
                    $this->getMessages()->addWarningMessage("Вы уже сохранили смс код на этот номер");
                }

            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }

        if (empty($updatedData)) {
            $updatedData = $this->getModelPhones()->getPhone($userData->id, $id);
        }


        if (empty($updatedData)) {
            return $this->redirect()->toRoute('app/default', array(
                'controller' => 'phone',
                'action' => 'list'
            ));
        }

        $this->getFormSmsCode()->setData($phoneEntity->clear()->fromDb($updatedData)->toArray());


        $viewModel = new ViewModel(
            ['form' => $this->getFormSmsCode(), 'id' => $id, 'messages' => $this->getMessages()]
        );

        return $viewModel;

    }


    /**
     *
     * Активируем телефон и привязываем его к конкретному объявлению через форму.
     * А также помещаем это объявление в очередь редиса
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function activateAction()
    {

        $this->getMessages()->clearAllMessages();
        $phone_id = (int)$this->params()->fromRoute('id', 0);
        $request = $this->getRequest();

        if (!$phone_id && !$request->isPost()) {
            return $this->redirect()->toRoute('app/default', array(
                'controller' => 'phones',
                'action' => 'list'
            ));
        }
        $userData = $this->getAuthService()->getIdentity();

        $phoneEntity = new \Entities\PhonesEntity();
        $phoneEntity->setStatus(\Consts\Table\UserPhones\Status::ACTIVE);

        $entityAdsTasks = new \Entities\EntityAdsTasks();

        if ($request->isPost()) {
            $this->getFormActivatePhoneToAd()->setData($request->getPost());

            if ($this->getFormActivatePhoneToAd()->isValid()) {

                $data = $this->getFormActivatePhoneToAd()->getData();
                $entityAdsTasks->fromArray($data);

                $adId = $this->getModelAdsArenda()->getAd($userData->id, $entityAdsTasks->getAdId());
                $phone = $this->getModelPhones()->getPhone($userData->id, $entityAdsTasks->getPhoneId());
                $phoneEntity->fromDb($phone);

                if ($phone === false || $phoneEntity->getStatus() != \Consts\Table\UserPhones\Status::NEWONE) {
                    $this->getMessages()->addErrorMessage("Не новый телефон нельзя активировать или его нет");
                }

                if (!$adId) {
                    $this->getMessages()->addErrorMessage("Объявления с таким id не существует");
                }

                if (!$this->getMessages()->hasErrorMessages()) {


                    $phoneEntity->setStatus(\Consts\Table\UserPhones\Status::ACTIVE);

                    $converter = new \Converters\Ads\AdToAvito($this->getDaoFactory());
                    $adConverted = $converter->convert((array)$adId);

                    if ($this->getModelPhones()->updateStatus($phoneEntity, $entityAdsTasks->getPhoneId())) {
                        $this->getMessages()->addSuccessMessage("Телефон успешно активирован");


                        $entityAdsTasks->setCreatedAt(new \DateTime())
                            ->setUserId($userData->id)
                            ->setPhoneId($phone_id);

                        $this->getModelAdsTasks()->save($entityAdsTasks);

                        $task = [];
                        $adConverted['phone'] = $phone->phone;
                        $task['ad'] = $adConverted;

                       // $proxy = $this->getDaoFactory()->getDaoProxy()->getOneProxyByDate();
                        $proxy = $this->getDaoFactory()->getDaoProxy()->getById(6);

                        if ($proxy) {
                            $proxyArr = $proxy->getArrayCopy();

                            $date = new \DateTime();
                            $date->add(new \DateInterval("P1D"));

                            $this->getDaoFactory()->getDaoProxy()->updateProxyUsageDate($proxyArr['id'], $date);
                            $proxy = [
                                "url" => $proxyArr['url'],
                                "port" => $proxyArr['port'],
                                "login" => $proxyArr['login'],
                                "password" => $proxyArr['password'],
                            ];

                            $task['proxy'] = $proxy;

                            if ($task) {
                                $this->getListAdsTasks()->prepend(Json::encode($task));
                            }

                        } else {
                            $this->getMessages()->addErrorMessage(
                                "Не осталось ни одного прокси для публикации"
                            );
                        }


                    } else {

                        $this->getMessages()->addWarningMessage(
                            "У вас нет такого телефона или вы уже его активировали"
                        );

                    }
                }

            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }

            $this->getFormActivatePhoneToAd()->setData($data);
        } else {
            $entityAdsTasks->setPhoneId($phone_id);
            $this->getFormActivatePhoneToAd()->setData($entityAdsTasks->toArray());
        }


        $viewModel = new ViewModel(
            ['form' => $this->getFormActivatePhoneToAd(), 'messages' => $this->getMessages()]
        );

        return $viewModel;


    }

    protected function canWeActivatePhone()
    {


    }

    /**
     *
     * Для получения сведений от публикаторов, что им нужен (ожидают) sms код
     * @return JsonModel
     */
    public function waitAction()
    {
        $this->getJsonResponse()->clearAll();

        $this->getMessages()->clearAllMessages();


        try {

            if ($this->getRequest()->isPost()) {


                $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();

                $phone = Strings::create()->clearDigits($this->params()->fromPost('phone', ''));
                $user_id = Strings::create()->clearDigits($this->params()->fromPost('id', 0));
                $adPassword = Strings::create()->clear($this->params()->fromPost('ad_password', ''));
                $site = Strings::create()->clear($this->params()->fromPost('site', 0));
                $login = Strings::create()->clear($this->params()->fromPost('ad_login', 0));


                $phoneEntity = new \Entities\PhonesEntity();
                $phoneEntity->setPhone($phone)->setUserId($user_id)->setAdPassword($adPassword)
                    ->setStatus(\Consts\Table\UserPhones\Status::WAIT_CONFIRMATION_CODE);

                $lastInsertedId = $this->getModelPhones()->updatePhoneStatusAndAdPassword($phoneEntity);

                if ($lastInsertedId) {

                    $logOpenAcc = new \Entities\EntityLogOpenAcc();
                    $logOpenAcc->setUserId($user_id)->setSite($site)
                        ->setLogin($login)->setPassword($adPassword);
                    $this->getModelPhones()->saveNewAcc($logOpenAcc);

                    $this->getJsonResponse()->addSuccess("Статус успешно обновлен");
                } else {
                    $this->getJsonResponse()->addErr(
                        "Ошибка обновления.  Нет такого телефона у пользователя. Телефон " . $phone
                    );
                }
            } else {
                $this->getJsonResponse()->addErr("Ошибка получения данных");
            }
        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $this->getJsonResponse()->addErr("Ошибка обновления данных");
        }


        return new JsonModel($this->getJsonResponse());
    }

    public function checkCodeAction()
    {
        $this->getJsonResponse()->clearAll();

        $this->getMessages()->clearAllMessages();


        try {

            if ($this->getRequest()->isPost()) {


                $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();

                $phone = Strings::create()->clearDigits($this->params()->fromPost('phone', ''));
                $user_id = Strings::create()->clearDigits($this->params()->fromPost('id', 0));

                $smsCode = $this->getRedis()->get($user_id . ":" . $phone);

                if ($smsCode) {
                    $this->getJsonResponse()->setPayload(["sms_code" => $smsCode]);
                    $this->getRedis()->del($user_id . ":" . $phone);
                } else {
                    $this->getJsonResponse()->addErr("Sms code not found");
                }

            } else {
                $this->getJsonResponse()->addErr("Ошибка получения данных");
            }
        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $this->getJsonResponse()->addErr("Произошла ошибка. Повторите действие.");
        }

        return new JsonModel($this->getJsonResponse());

    }


}