<?php
/**
 * Date: 1/9/15
 * Time: 1:35 PM
 * @author namax
 */

namespace Application\Controller;


use Zend\View\Model\JsonModel;
use Helpers\Strings;
use Zend\View\Model\ViewModel;
use Zend\Json\Json;
use \Consts\Table\BillingType\BillingType;

class AdsArendaAutoApiController extends AbstractController
{


    protected $listAdsTasks = null;
    protected $modelAdsArenda = null;
    protected $modelAdsTasks = null;
    protected $modelPhones = null;


    /**
     *
     * @return \Application\Models\PhonesModel
     */
    public function getModelPhones()
    {
        if ($this->modelPhones === null) {
            $this->modelPhones = new \Application\Models\PhonesModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelPhones;
    }

    /**
     *
     * @return \Application\Models\AdsArendaModel
     */
    public function getModelAdsArenda()
    {
        if ($this->modelAdsArenda === null) {
            $this->modelAdsArenda =
                new \Application\Models\AdsArendaModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelAdsArenda;
    }


    /**
     *
     * @return \Application\Models\ModelAdsTasks
     */
    public function getModelAdsTasks()
    {
        if ($this->modelAdsTasks === null) {
            $this->modelAdsTasks = new \Application\Models\ModelAdsTasks(
                $this->getServiceLocator()->get('DaoFactory')
            );
        }
        return $this->modelAdsTasks;
    }

    /**
     * @return \Datatypes\Redis\ListAdsTasks
     */
    public function getListAdsTasks()
    {

        if ($this->listAdsTasks === null) {
            $this->listAdsTasks = new \Datatypes\Redis\ListAdsTasks($this->getRedis());
        }
        return $this->listAdsTasks;
    }


    public function getArendaAdAction()
    {

        $this->getJsonResponse()->clearAll();

        $this->getMessages()->clearAllMessages();

        try {

            $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();

            //$payload = $this->getPayloadForTest();
            $payload =$this->getListAdsTasks()->rPop();
            if ($payload) {
                $this->getJsonResponse()->setPayload(JSON::decode($payload));

            } else {
                $this->getJsonResponse()->addErr("Queue is empty");
            }

        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $this->getJsonResponse()->addErr("Ошибка получения данных");
        }


        return new JsonModel($this->getJsonResponse());

    }

    private function getPayloadForTest()
    {

        $adId = $this->getModelAdsArenda()->getAd(1, 30);

        if (!$adId) {
            $this->getMessages()->addErrorMessage("Объявления с таким id не существует");
        }

        $converter = new \Converters\Ads\AdToAvito($this->getDaoFactory());
        $adConverted = $converter->convert((array)$adId);

        $adConverted['phone'] = '79636227318';
        $task['ad'] = $adConverted;
        $proxyRaw = $this->getDaoFactory()->getDaoProxy()->getById(5);
        $proxy = [
            "url" => $proxyRaw['url'],
            "port" => $proxyRaw['port'],
            "login" => $proxyRaw['login'],
            "password" => $proxyRaw['password'],
        ];
        $task['proxy'] = $proxy;

        return Json::encode($task);
    }

    public function successPublishAction()
    {
        $this->getJsonResponse()->clearAll();

        try {

            if ($this->getRequest()->isPost()) {


                $user_id = Strings::create()->clearDigits($this->params()->fromPost('id', 0));
                $phone = Strings::create()->clearDigits($this->params()->fromPost('phone', ''));


                $phoneEntity = new \Entities\PhonesEntity();
                $phoneEntity->setPhone($phone)->setUserId($user_id)
                    ->setStatus(\Consts\Table\UserPhones\Status::SUCCESS);

                $lastInsertedId = $this->getModelPhones()->updateStatusByPhone($phoneEntity);

                if ($lastInsertedId) {

                    $this->getJsonResponse()->addSuccess("Статус успешно обновлен");

                    $billingTypes = $this->getDaoFactory()->getDaoBillingType()->getAll();
                    $billingTypes = \Helpers\RearrangeArray::create()->byKey($billingTypes->toArray(), 'id');

                    $billingTypeEntity = new \Entities\BillingType();
                    $billingTypeEntity->fromDb($billingTypes[BillingType::PAY_FOR_PUBLISHED_AD]);

                    //$this->getDaoFactory()->getDaoUsers()->addToBalance($amountToPay, $user_id);

                    $userData = $this->getDaoFactory()->getDaoUsers()->getById($user_id);
                    $amountToPay = $billingTypeEntity->getPriceByTariff($userData->tariff);

                    $billingHistory = new \Entities\BillingHistory();
                    $billingHistory->setCredit($amountToPay)
                        ->setUserId($user_id)
                        ->setDate(\Helpers\DateTime::create()->now())
                        ->setBillingTypeId(BillingType::PAY_FOR_PUBLISHED_AD)
                        ->setDescription($billingTypeEntity->getName() . " на тел " . $phone)
                        ->setBalance($userData->balance);
                    $this->getDaoFactory()->getDaoBillingHistory()->insert($billingHistory->toDb());


                } else {
                    $this->getJsonResponse()->addErr(
                        "Ошибка обновления.  Нет такого телефона у пользователя. Телефон " . $phone
                    );
                }
            } else {
                $this->getJsonResponse()->addErr("Ошибка получения данных");
            }
        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $this->getJsonResponse()->addErr("Ошибка обновления данных");
        }


        return new JsonModel($this->getJsonResponse());
    }

    public function faildPublishAction()
    {

        $this->getJsonResponse()->clearAll();

        try {
            if ($this->getRequest()->isPost()) {


                $user_id = Strings::create()->clearDigits($this->params()->fromPost('id', 0));
                $phone = Strings::create()->clearDigits($this->params()->fromPost('phone', ''));
                $this->getLogDb()->info(__METHOD__." ".$user_id." ".$phone);

                $phoneEntity = new \Entities\PhonesEntity();
                $phoneEntity->setPhone($phone)->setUserId($user_id)
                    ->setStatus(\Consts\Table\UserPhones\Status::ERROR);

                $lastInsertedId = $this->getModelPhones()->updateStatusByPhone($phoneEntity);

                if ($lastInsertedId) {

                    $this->getJsonResponse()->addSuccess("Статус успешно обновлен");
                } else {
                    $this->getJsonResponse()->addErr(
                        "Ошибка обновления.  Нет такого телефона у пользователя. Телефон " . $phone
                    );
                }
            } else {
                $this->getJsonResponse()->addErr("Ошибка получения данных");
            }
        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $this->getJsonResponse()->addErr("Ошибка обновления данных");
        }


        return new JsonModel($this->getJsonResponse());
    }


}