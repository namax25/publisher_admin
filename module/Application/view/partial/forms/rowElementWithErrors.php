<div class="row">
    <div class="col-md-3 col-sm-4 col-md-offset-1 col-sm-offset-1">
        <div class="form-group <?= ($this->formElementErrors($form->get($this->elementName))) ? 'has-error' : '' ?>">
            <?= $this->formLabel($form->get($this->elementName)); ?>
            <?= $this->formElement($form->get($this->elementName)); ?>
        </div>
        <?php $errors = $form->get($this->elementName)->getMessages();
        ?>
        <?php if ($errors) : ?>
            <div class="form-group">
                <?php foreach ($errors as $error) : ?>
                    <div class="text-danger"><?= $this->translate($error) ?></div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>